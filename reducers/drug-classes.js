export default (
  state = {
    data: [],
    error: false,
    fetching: false,
  },
  action
) => {

  newState = Object.assign({}, state);

  switch (action.type) {

    case 'DRUG_CLASSES_POPULATE':
      newState.data = action.classes.slice(0);
      newState.fetching = false;
      return newState;

    case 'DRUG_CLASSES_INVALIDATE':
      newState.error = false;
      newState.fetching = true;
      return newState;

    case 'DRUG_CLASSES_SET_ERROR':
      newState.error = true;
      newState.fetching = false;
      return newState;

    default: return state;
  }

}