export default (
  state = {
    access: '',
    refresh: '',
  },
  action
) => {

  newState = Object.assign({}, state);

  switch(action.type) {

    case 'AUTH_SET_TOKENS':
      newState.access = action.access;
      newState.refresh = action.refresh;
      return newState;

    default: return state;
  }

}