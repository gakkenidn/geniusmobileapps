import auth from './auth';
import catalog from './catalog';
import articles from './articles';
import journalCategories from './journal-categories'
import drugClasses from './drug-classes'
import profile from './profile'
import { combineReducers } from 'redux';

export default combineReducers({
  auth, articles, catalog, drugClasses, journalCategories, profile
});