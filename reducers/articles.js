export default (
  state = {
    page: 1,
    data: [],
    error: false,
    fetching: false,
    search: [],
  },
  action
) => {

  newState = Object.assign({}, state);

  switch (action.type) {

    case 'ARTICLES_POPULATE':
      if (state.page > 1)
        action.articles.map(article => newState.data.push(article));
      else newState.data = action.articles.slice(0);

      newState.page = state.page + 1;
      newState.fetching = false;
      return newState;

    case 'ARTICLES_POPULATE_SEARCHED':
      newState.search = action.articles.slice(0);
      return newState;

    case 'ARTICLES_INVALIDATE':
      newState.page = 0;
      newState.fetching = true;
      newState.error = false;
      return newState;

    case 'ARTICLES_SET_ERROR':
      newState.fetching = false;
      newState.error = true;
      return newState;

    default: return state;
  }

}