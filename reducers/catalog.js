export default (
  state = {
    data: [],
    error: false,
    fetching: false,
  },
  action
) => {

  newState = Object.assign({}, state);

  switch (action.type) {

    case 'CATALOG_POPULATE':
      newState.data = action.catalog.slice(0);
      newState.fetching = false;
      return newState;

    case 'CATALOG_INVALIDATE':
      newState.error = false;
      newState.fetching = true;
      return newState;

    case 'CATALOG_SET_ERROR':
      newState.error = true;
      newState.fetching = false;
      return newState;

    default: return state;
  }

}