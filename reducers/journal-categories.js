export default (
  state = {
    data: [],
    error: false,
    fetching: false,
  },
  action
) => {

  newState = Object.assign({}, state);

  switch (action.type) {

    case 'JOURNAL_CATEGORIES_POPULATE':
      newState.data = action.categories.slice(0);
      newState.fetching = false;
      return newState;

    case 'JOURNAL_CATEGORIES_INVALIDATE':
      newState.error = false;
      newState.fetching = true;
      return newState;

    case 'JOURNAL_CATEGORIES_SET_ERROR':
      newState.error = true;
      newState.fetching = false;
      return newState;

    default: return state;
  }

}