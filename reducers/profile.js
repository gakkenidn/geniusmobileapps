export default (
  state = {
    data: {},
    error: false,
    fetching: false,
  },
  action
) => {

  newState = Object.assign({}, state);

  switch (action.type) {

    case 'PROFILE_POPULATE':
      newState.data = Object.assign({}, action.profile);
      newState.fetching = false;
      return newState;

    case 'PROFILE_INVALIDATE':
      newState.fetching = true;
      newState.error = false;
      return newState;

    case 'PROFILE_SET_ERROR':
      newState.fetching = false;
      newState.error = true;
      return newState;

    default: return state;
  }

}