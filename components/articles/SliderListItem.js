import React from 'react';
import { ImageBackground, Platform, Text, View } from 'react-native';
import { Link } from 'react-router-native';
import Touchable from 'react-native-platform-touchable';

import BaseStyles from '../../styles/base';
import * as StyleConstants from '../../styles/constants';

const SliderListItem = props => {
  const featuredImageUri =
    props.featuredImage.medium_large
    || props.featuredImage.medium
    || props.featuredImage.thumbnail;
  return (
    <View key={ props.slug } style={{ width: 300, marginRight: StyleConstants.SPACING.X4, marginBottom: StyleConstants.SPACING.X2 }}>
      <Link to={"/articles/" + props.slug } component={ Touchable }>
        <View style={{
          borderRadius: StyleConstants.SPACING.X2,
          overflow: 'hidden',
          width: 300,
          height: 200,
          elevation: 2,
          borderColor: StyleConstants.COLOR_GRAY.LIGHTEST,
          borderWidth: Platform.OS === 'ios' ? 1 : 0,
          marginBottom: StyleConstants.SPACING.X3,
        }}>
          <ImageBackground
          source={{ uri: featuredImageUri }}
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: StyleConstants.COLOR_GRAY.LIGHTEST
          }} />
        </View>
      </Link>
      <Text style={ BaseStyles.h4 }>{ props.title }</Text>
      <Text style={ BaseStyles.text } numberOfLines={ 2 }>{ props.subheading }</Text>
    </View>
  )
};

export default SliderListItem;