import React, { Fragment } from 'react';
import { Image, Text, TouchableWithoutFeedback, View } from 'react-native';
import { Link } from 'react-router-native';

import * as StyleConstants from '../../styles/constants';
import BaseStyles from '../../styles/base';
import ListStyles from '../../styles/lists';

const ListItem = props => {

  const textBlock = (
    <Fragment>
      <Text style={ props.index === 0 && props.withFeatured ? BaseStyles.h3 : BaseStyles.h4 } numberOfLines={ 2 }>{ props.item.title }</Text>
      <Text style={ BaseStyles.p } numberOfLines={ 3 }>{ props.item.subheading }</Text>
      <Text style={ BaseStyles.muted } numberOfLines={ 1 }>{ props.item.publishedAt } oleh { props.item.author }</Text>
    </Fragment>
  );
  const featuredImageUri =
    props.item.featuredImage.medium_large
    || props.item.featuredImage.medium
    || props.item.featuredImage.thumbnail;

  return (
    <Link to={ '/articles/' + props.item.slug + (props.linkQuery ? "?" + props.linkQuery : '') } component={ TouchableWithoutFeedback }>
      {
        props.index === 0 && props.withFeatured ?
        (
          <View style={{ paddingBottom: StyleConstants.SPACING.X3, paddingTop: StyleConstants.SPACING.X5 }}>
            <View style={{ paddingHorizontal: StyleConstants.SPACING.X4 }}><Text style={ BaseStyles.p }>Artikel Terbaru</Text></View>
            <Image source={{ uri: featuredImageUri }} style={{ width: '100%', height: 200 }} />
            <View style={{ paddingHorizontal: StyleConstants.SPACING.X4, paddingTop: StyleConstants.SPACING.X3 }}>
              { textBlock }
            </View>
          </View>
        ) : (
          <View style={ ListStyles.itemShadowed }>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
              <View style={{ flex: 1 }}>
                { textBlock }
              </View>
              <Image source={{ uri: featuredImageUri }} style={{ height: 100, width: 100, marginLeft: 24, borderRadius: 4 }} />
            </View>
          </View>
        )
      }
    </Link>
  );

}

export default ListItem;