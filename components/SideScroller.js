import React from 'react';
import { Image, ScrollView, Text, TouchableWithoutFeedback , View } from 'react-native';
import { Link } from 'react-router-native';
import * as StyleConstants from '../styles/constants';
import BaseStyles from '../styles/base';

const SideScroller = props => (
  <ScrollView horizontal={ true } showsHorizontalScrollIndicator={ false } style={ props.style || {} }>
    {
      props.items.map((item, index) => (
        <Link to={ item.link } component={ TouchableWithoutFeedback } key={ index }>
          <View
            style={{
              width: props.imageWidth || 150,
              alignItems: 'stretch',
              marginLeft: index === 0 ? StyleConstants.SPACING.X4 : 0,
              marginRight: index === props.items.length - 1 ? StyleConstants.SPACING.X4 : StyleConstants.SPACING.X3,
              justifyContent: 'flex-start'
            }}>
            { item.image && <Image source={{ uri: item.image }} style={{ borderRadius: StyleConstants.BORDER_RADIUS_BASE, marginBottom: StyleConstants.SPACING.X2, width: '100%', height: props.imageHeight || 100 }} /> }
            <Text style={ BaseStyles.h6 } numberOfLines={ props.numberOfLines || 3 }>{ item.title }</Text>
          </View>
        </Link>
      ))
    }
  </ScrollView>
);

export default SideScroller;