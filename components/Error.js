import React from 'react';
import { Image, Text, View } from 'react-native';
import Touchable from 'react-native-platform-touchable';

import BaseStyles from '../styles/base';
import * as StyleConstants from '../styles/constants';

const Error = props => (
  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: StyleConstants.SPACING.X4 }}>
    <Text style={ BaseStyles.p }>{ props.message || 'Ups, kami gagal mengambil data dari server.' }</Text>
    <Touchable onPress={ () => props.onRetry() }>
      <View>
        <Image source={ require('../resources/images/Reload-Dark.png') } style={{ width: 30, height: 30 }} />
      </View>
    </Touchable>
  </View>
);

export default Error;