import React, {Component, Fragment} from 'react';
import {Image, Text, TouchableWithoutFeedback, View} from 'react-native';
import {Link} from 'react-router-native';

import * as StyleConstants from '../../styles/constants';
import BaseStyles from '../../styles/base';
import ListStyles from '../../styles/lists';

const ListItem = props => {
    return(
        <TouchableWithoutFeedback>
            <View style={[ListStyles.itemShadowed, {flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}]}>
                <View>
                    <Image source={ require('../../resources/images/BebasTerbatas.png') } style={{ height: 20, width: 20 }} />
                </View>
                <View style={{
                    flex: 1,
                    paddingLeft: StyleConstants.SPACING_BASE,
                    paddingRight: StyleConstants.SPACING.X4,
                    flexDirection: 'column',
                    marginLeft: 15
                }}>
                    <Text style={BaseStyles.h5} numberOfLines={ 1 }>{props.item.title}</Text>
                    <Text style={BaseStyles.p} numberOfLines={ 1 }>{props.item.desc}</Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

export default ListItem;