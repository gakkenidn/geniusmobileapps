import React from 'react';
import { Image, Text, View } from 'react-native';
import { Link } from 'react-router-native';
import Touchable from 'react-native-platform-touchable';
import { getStatusBarHeight } from 'react-native-status-bar-height';

import * as StyleConstants from '../styles/constants';

const Header = props => {

  const backBtn = props.back && (
    <View style={{
      width: 30 + StyleConstants.SPACING.X4 + StyleConstants.SPACING.X3,
      paddingLeft: StyleConstants.SPACING.X4,
      paddingRight: StyleConstants.SPACING.X3,
      }}>
      {
        props.bgStyle === 'light' ?
          <Image source={ require('../resources/images/ArrowLeft-Dark.png') } style={{ height: 20, width: 24 }} /> :
          <Image source={ require('../resources/images/ArrowLeft-Light.png') } style={{ height: 20, width: 24 }} />
      }
    </View>
  );

    function logOut() {
        // console.log(props);
        props.signOut();
        props.gakkenIndo();
        props.route.history.push('/');
    }

  return (
    <View style={{
      height: 70 + getStatusBarHeight(true),
      elevation: 3,
      shadowColor: StyleConstants.COLOR.BLACK,
      shadowOpacity: 0.2,
      shadowOffset: { height: 3 },
      zIndex: 999,
      alignItems: 'center',
      flexDirection: 'row',
      justifyContent: 'space-between',
      backgroundColor: props.bgStyle === 'light' ? StyleConstants.COLOR_GRAY.LIGHTEST : StyleConstants.COLOR_GRAY.DARKEST,
      paddingTop: getStatusBarHeight(true),
      }}>
      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
        {
          backBtn && (
            typeof props.back === 'function' ?
            (
              <Touchable onPress={ () => props.back() } hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}>
                { backBtn }
              </Touchable>
            ) :
            (
              <Link to={ props.back } component={ Touchable } hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}>
                { backBtn }
              </Link>
            )
          )
        }
        <View style={{
          flex: 1,
          paddingLeft: props.back ? StyleConstants.SPACING_BASE : StyleConstants.SPACING.X4,
          paddingRight: StyleConstants.SPACING.X4,
          }}>
          {
            props.titleComponent ||
            (
              <Text style={{
                color: StyleConstants.COLOR_GRAY.LIGHTEST,
                fontSize: StyleConstants.FONT_SIZE.H3,
                fontFamily: StyleConstants.FONT_FAMILY_HEADINGS,
                }}
                numberOfLines={ 1 }>
                { props.title }
              </Text>
            )
          }
        </View>
      </View>
      <View>
          {
              props.signOut &&
              (
                  <Touchable hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }} onPress={ () => logOut() }>
                      <View style={{
                          width: 20 + StyleConstants.SPACING.X4 + StyleConstants.SPACING.X3,
                          paddingLeft: StyleConstants.SPACING.X3,
                          paddingRight: StyleConstants.SPACING.X4,
                      }}>
                          <Image source={ require('../resources/images/Sign-Out.png') } style={{ height: 40, width: 40 }} />
                      </View>
                  </Touchable>
              )
          }
      </View>
      <View>
        {
          props.searchRoute &&
          (
            <Link to={ props.searchRoute } component={ Touchable } hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}>
              <View style={{
                width: 20 + StyleConstants.SPACING.X4 + StyleConstants.SPACING.X3,
                paddingLeft: StyleConstants.SPACING.X3,
                paddingRight: StyleConstants.SPACING.X4,
                }}>
                <Image source={ require('../resources/images/Search-Light.png') } style={{ height: 20, width: 20 }} />
              </View>
            </Link>
          )
        }
        {
          props.editRoute &&
          (
            <Link to={ props.editRoute } component={ Touchable } hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}>
              <View style={{
                width: 20 + StyleConstants.SPACING.X4 + StyleConstants.SPACING.X3,
                paddingLeft: StyleConstants.SPACING.X3,
                paddingRight: StyleConstants.SPACING.X4,
                }}>
                <Image source={ require('../resources/images/EditPencil-Light.png') } style={{ height: 20, width: 20 }} />
              </View>
            </Link>
          )
        }
      </View>
    </View>
  );
}

export default Header;