import React from 'react';
import {View, Text} from 'react-native';

import * as Cons from '../styles/constants';
import Styles from '../styles/base';

export default class TopicBox extends React.Component{

		state = {
				expiredIn: '3 bulan',
				expiredAt: 'Sun, 20 Oct 2019, 01:02'
		};

		render(){
				return(
						<View style={{margin: 10, borderColor: Cons.COLOR_GRAY.LIGHT, borderStyle: 'solid', borderWidth: 1, borderRadius: 5}}>
								<View style={{backgroundColor: 'black', height: 30, flexDirection: 'row', borderTopLeftRadius: 5, borderTopRightRadius: 5, padding: 5}}>
										<Text style={{color: 'white'}}>Pengerjaan ditutup </Text>
										<Text style={{color: 'orange'}}>dalam {this.state.expiredIn} (</Text>
										<Text style={{color: 'orange'}}>{this.state.expiredAt})</Text>
								</View>
								{this.props.component}
						</View>
				)
		}
}