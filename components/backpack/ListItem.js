import React, {Component, Fragment} from 'react';
import {Image, Text, TouchableWithoutFeedback, View, TouchableOpacity} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import * as StyleConstants from '../../styles/constants';
import ListStyles from '../../styles/lists';

const ListItem = props => {
    return(
        <TouchableWithoutFeedback>
            <View style={[ListStyles.itemShadowed, {alignItems: 'center', flexDirection:'row', justifyContent: 'space-between', flex: 1}]}>
                <View>
                    {
                        props.item.type === 'pdf'
                            ? <FontAwesome name="file-pdf-o" size={25} color={StyleConstants.COLOR_GRAY.DARK}/>
                            : props.item.type === 'presentation'
                                ? <FontAwesome name="file-powerpoint-o" size={25} color={StyleConstants.COLOR_GRAY.DARK}/>
                                : props.item.type === 'word'
                                    ? <FontAwesome name="file-word-o" size={25} color={StyleConstants.COLOR_GRAY.DARK}/>
                                    : <FontAwesome name="file" size={25} color={StyleConstants.COLOR_GRAY.DARK}/>
                    }
                </View>
                <View
                    style={{flex: 1, flexDirection: 'column', alignItems: 'flex-start', justifyContent: "space-between", paddingLeft: 20, paddingRight: 10}}>
                    <Text style={{textAlign: 'left', flex: 1, color: StyleConstants.COLOR.BLACK, fontSize: 18}} numberOfLines={ 1 }>{props.item.nama}</Text>
                    <Text style={{textAlign: 'left', flex: 1}} numberOfLines={ 1 }>{props.item.ukuran + " kb"}</Text>
                </View>
                <View>
                    <TouchableOpacity>
                        <FontAwesome name="download" size={25} color={StyleConstants.COLOR.BLUE}/>
                    </TouchableOpacity>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );x
};

export default ListItem;