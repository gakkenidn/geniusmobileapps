import React, { Component, Fragment } from 'react';
import { Image, Text, TextInput, View } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import Touchable from 'react-native-platform-touchable';
import Validator from 'validator';

import LoginStyles from '../../styles/login';
import * as StyleConstants from '../../styles/constants';
import ProfileApi from '../../api/profile';
import DropdownAlert from "react-native-dropdownalert";

class LoginForm extends Component {
  state = {
    authenticating: false,
    email: '',
    password: '',
    errors: {
      general: '',
      email: false,
      password: false,
    },
  }

  authenticate() {
    if (this.state.authenticating)
      return false;

    this.setState({ errors: Object.assign(this.state.errors, { general: '', email: false, password: false })});
    if (!Validator.isEmail(this.state.email))
      this.setState({ errors: Object.assign(this.state.errors, { email: 'Email ini tidak valid' }) });
    if (Validator.isEmpty(this.state.password))
      this.setState({ errors: Object.assign(this.state.errors, { password: 'Masukkan kata sandi' }) });

    if (this.state.errors.email || this.state.errors.password)
      return false;

    this.setState({ authenticating: true });
    this.props.onSubmit();

    ProfileApi.authenticate({
      email: this.state.email,
      password: this.state.password,
      cb: ({ access, refresh }) => {
        this.props.onSuccess({ refresh, access });
      },
      err: e => {
        // console.warn(JSON.stringify(e.response.data));
        this.setState({
          errors: Object.assign(this.state.errors, { general: e.response.data.message }),
          authenticating: false,
        });
        this.dropDownAlertRef.alertWithType('error', 'Error', e.response.data.message);
        this.props.onFinish();
      }
    });
  }


  render() {
    return (
      <AnimatableView animation="fadeInLeft" duration={ 300 } easing="ease-in-out-circ" style={ LoginStyles.formContainer }>
        <View style={{ marginBottom: StyleConstants.SPACING.X3, width: '100%' }}>
          <View style={ LoginStyles.inputContainer }>
            <Image source={ require('../../resources/images/Envelope-Muted.png') } style={{ position: 'absolute', top: 13, left: 15, width: 20, height: 18 }} />
            <TextInput
              style={ LoginStyles.input }
              placeholder="Alamat email"
              keyboardType="email-address"
              autoCapitalize="none"
              underlineColorAndroid="rgba(0,0,0,0)"
              onFocus={ () => this.setState({ errors: Object.assign(this.state.errors, { email: false })})}
              onChange={ e => this.setState({ email: e.nativeEvent.text }) } />
            { this.state.errors.email && <AnimatableView animation="fadeInRight" duration={ 300 } style={ LoginStyles.inputErrorContainer }><Text style={ LoginStyles.inputErrorText }>{ this.state.errors.email }</Text></AnimatableView> }
          </View>
          <View style={ LoginStyles.inputContainer }>
            <Image source={ require('../../resources/images/Lock-Muted.png') } style={{ position: 'absolute', top: 10, left: 15, width: 20, height: 22 }} />
            <TextInput
              style={ LoginStyles.input }
              placeholder="Kata sandi"
              autoCapitalize="none"
              secureTextEntry={ true }
              underlineColorAndroid="rgba(0,0,0,0)"
              onFocus={ () => this.setState({ errors: Object.assign(this.state.errors, { password: false })})}
              onChange={ e => this.setState({ password: e.nativeEvent.text }) } />
            { this.state.errors.password && <AnimatableView animation="fadeInRight" duration={ 300 } style={ LoginStyles.inputErrorContainer }><Text style={ LoginStyles.inputErrorText }>{ this.state.errors.password }</Text></AnimatableView> }
          </View>
        </View>
        <Touchable onPress={ () => this.authenticate() } style={ this.state.authenticating ? LoginStyles.submitBtnDisabled : LoginStyles.submitBtn }>
          <Text style={ LoginStyles.submitBtnText }>
            { this.state.authenticating ? 'Mohon tunggu..' : 'Masuk' }
          </Text>
        </Touchable>
        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
      </AnimatableView>
    );
  }
}

export default LoginForm;