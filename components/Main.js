import React, { Component, Fragment } from 'react';
import { View } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NativeRouter, Redirect, Route, Switch } from 'react-router-native';
import AsyncStorage from '@react-native-community/async-storage';

import * as Actions from '../actions/index'
import Toolbar from './Toolbar';
import ToolbarLMS from './ToolbarLMS';

import HomeGakken from '../pages/gakken/HomeGakken';
import Splash from '../pages/Splash';
import Learn from '../pages/gakken/learn/Index';
import Profile from '../pages/profile/Index';
import Articles from '../pages/gakken/articles/Index';

import Class from '../pages/lms/class/Index';
import Backpack from '../pages/lms/Backpack';
import Notification from '../pages/lms/Notification';

class Main extends Component {
  state = {
    ready: false,
    splashDone: false,
    typeView: 1,
    isFull: false,
  };

  async componentDidMount() {
    try {
      let access = await AsyncStorage.getItem('@AppStore:accessToken');
      let refresh = await AsyncStorage.getItem('@AppStore:refreshToken');

      if (access == null) {
        access = '';
        refresh = '';
        await AsyncStorage.setItem('@AppStore:accessToken', '');
        await AsyncStorage.setItem('@AppStore:refreshToken', '');
      }

      this.props.setAuthenticationTokens({ access, refresh });
      this.setState({ ready: true });
    } catch (err) {

    }
  }

  render() {
    if (!this.state.splashDone)
      return <Splash onDone={ () => this.setState({ splashDone: true })} ready={ this.state.ready } />;


    if (this.state.typeView === 1) {
      return (
          <NativeRouter>
            <View style={{height: '100%'}}>
              <Fragment>
                <Switch>
                  <Route path="/articles" render={() => <Articles {...this.props} />}/>
                  <Route path="/learn" render={route => <Learn {...this.props} {...route} />}/>
                  <Route path="/profile" render={route => <Profile {...this.props} typeView={this.state.typeView} {...route}
                                                                onGakken={() => this.setState({typeView: 1})}
                                                                onLMS={() => this.setState({typeView: 2})}/>}/>
                  <Route exact path="/" render={() => <HomeGakken {...this.props} />}/>
                  <Redirect to="/"/>
                </Switch>
                <Toolbar/>
              </Fragment>
            </View>
          </NativeRouter>
      );
    }
    else if(this.state.typeView === 2){
      return (
          <NativeRouter>
            <View style={{height: '100%'}}>
              <Fragment>
                <Switch>
                  <Route path="/class" render={() => <Class {...this.props} isFull={tf => this.setState({isFull: tf})}/>}/>
                  <Route path="/backpack" render={() => <Backpack {...this.props}/>}/>
                  <Route path="/notification" render={() => <Notification {...this.props}/>}/>
                  <Route path="/profile" render={route => <Profile {...this.props} typeView={this.state.typeView} onGakken={() => this.setState({typeView: 1})}
                                                                onLMS={() => this.setState({typeView: 2})} {...route} />}/>
                  <Redirect to="/class"/>
                </Switch>
                <ToolbarLMS hide={this.state.isFull}/>
              </Fragment>
            </View>
          </NativeRouter>
      )
    }
  }
}

export default connect(
  (state, props) => {
    return {
      auth: state.auth,
      articles: state.articles,
      catalog: state.catalog,
      drugClasses: state.drugClasses,
      journalCategories: state.journalCategories,
      profile: state.profile,
      classes: state.classes,
    };
  },
  dispatch => bindActionCreators(Actions, dispatch)
)(Main);