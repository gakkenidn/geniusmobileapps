import React, { Component, Fragment } from 'react';
import { ScrollView, Text, TouchableWithoutFeedback, View } from 'react-native';
import { Link } from 'react-router-native';
import Spinner from '../Spinner';

import BaseStyles from '../../styles/base';
import * as StyleConstants from '../../styles/constants';
import ListItem from './SliderListItem';
import Error from '../Error';

class HomepageSlider extends Component {
  state = {  }

  componentDidMount() {
    this.props.getCatalog();
  }

  render() {
    return (
      <Fragment>
        <View style={{ height: StyleConstants.SPACING.X4 }} />
        <View style={ BaseStyles.content }>
          <Text style={ BaseStyles.h4 }>..pelajari topik P2KB</Text>
        </View>

        <ScrollView horizontal={ true } showsHorizontalScrollIndicator={ false }>
          <View style={{ width: StyleConstants.SPACING.X4 }} />
            { this.props.catalog.data.map((topic, index) => index > 9 ? null : <ListItem key={ topic.slug } { ...topic } index={ index } topicsCount={ this.props.catalog.data.length } /> ) }
          <View style={{ width: StyleConstants.SPACING.X4 }} />
        </ScrollView>

        { this.props.catalog.fetching && !this.props.catalog.data.length && <Spinner /> }
        { this.props.catalog.error && <Error onRetry={ () => this.props.getCatalog() } /> }

        {
          this.props.catalog.data.length > 0 &&
          (
            <Link to="/learn?section=p2kb" component={ TouchableWithoutFeedback }>
              <View style={ BaseStyles.content }>
                <Text style={ BaseStyles.textLink }>Katalog P2KB</Text>
              </View>
            </Link>
          )
        }
      </Fragment>
    );
  }
}

export default HomepageSlider;