import React, {Component} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';

import * as StyleConstants from "../../../styles/constants";

const QuizFillIn = props => (
    <View style={{ flexDirection: 'column', justifyContent: 'flex-start', margin: 10}}>
        <TextInput style = {styles.input}
        underlineColorAndroid = "transparent"
        autoCapitalize = "none"
        value={props.answer}
        onChange={e => props.onChange(e.nativeEvent.text)}/>
    </View>
);

const styles = StyleSheet.create({
    input: {
        margin: 15,
        height: 40,
        borderColor: StyleConstants.COLOR_GRAY.LIGHT,
        borderWidth: 1
    },
});

export default QuizFillIn;