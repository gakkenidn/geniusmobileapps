import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import BaseStyles from '../../../styles/base';
import * as StyleConstants from '../../../styles/constants';

const RadioButton = props => (
    <TouchableOpacity>
        <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginRight: StyleConstants.SPACING.X3, paddingVertical: StyleConstants.SPACING.X2}}>
            {
                props.selected
                    ? <Icon name="check-circle-o" size={25} color={StyleConstants.COLOR.PRIMARY}/>
                    : <Icon name="circle-o" size={25} color={StyleConstants.COLOR_GRAY.DARK}/>
            }
            <Text style={{fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, fontSize: StyleConstants.FONT_SIZE.H5, color: 'black', marginLeft: StyleConstants.SPACING.X2}}>{props.label}</Text>
        </View>
    </TouchableOpacity>
);

export default RadioButton;