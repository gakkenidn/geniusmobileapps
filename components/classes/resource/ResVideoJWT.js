import React from 'react';
import { WebView } from 'react-native-webview';
import {View, Text} from 'react-native';

import {API_BASE_URL} from "../../../env";

const ResVideoJWT = props => (
    <View style={{ height: '100%', width: '100%', backgroundColor: 'black' }}>
        <WebView source={{
            uri: API_BASE_URL + '/video/' + props.url,
            headers: {
                'Authorization': 'Bearer ' + props.auth.access
            }
        }} />
    </View>
);

export default ResVideoJWT;