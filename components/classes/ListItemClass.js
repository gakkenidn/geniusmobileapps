import React, {Component, Fragment} from 'react';
import {Text, TouchableWithoutFeedback, View}from 'react-native';
import {Link} from 'react-router-native';

import * as StyleConstants from '../../styles/constants';
import BaseStyles from '../../styles/base';
import ListStyles from '../../styles/lists';

const ListItem = props => {
    const textBLock = (
        <Fragment>
            <Text style={BaseStyles.topic} numberOfLines={1}>{props.item.categories.data[0].name}</Text>
            <Text style={BaseStyles.h4} numberOfLines={1}>{props.item.title}</Text>
            <Text style={BaseStyles.p} numberOfLines={2}>{props.item.description}</Text>
        </Fragment>
    );

    return(
        <Link to={'/class/' + props.item.slug + '/detail' } component={TouchableWithoutFeedback}>
            <View style={ListStyles.itemShadowed}>
                <View style={{flex: 1, flexDirection: 'column', alignItems: 'flex-start'}}>
                    {textBLock}
                </View>
                <View>
                    <Text style={{color: StyleConstants.COLOR.BLUE, textAlign: 'right'}}>Pelajari ></Text>
                </View>
            </View>
        </Link>
    );
};

export default ListItem;