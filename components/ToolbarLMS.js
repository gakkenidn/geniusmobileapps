import React from 'react';
import {Image, Text, View} from "react-native";
import {Link, Route, withRouter} from 'react-router-native';
import Touchable from 'react-native-platform-touchable';
import _ from 'lodash';

import BaseStyles from '../styles/base';

const ToolbarLMS = props => {

    if (props.hide){
        return null;
    }

    return(
        <View style={BaseStyles.toolbar}>
            <Link to="/class" component={Touchable} style={BaseStyles.toolbarTouchable}>
                <View style={BaseStyles.toolbarItem}>
                    {
                        _.startsWith(props.location.pathname, '/class') ?
                            <Image source={require('../resources/images/HomeActive.png')} style={{height: 30, width: 30}} /> :
                            <Image source={require('../resources/images/HomeIdle.png')} style={{height: 30, width: 30}} />
                    }
                </View>
            </Link>
            <Link to="/backpack" component={Touchable} style={BaseStyles.toolbarTouchable}>
                <View style={BaseStyles.toolbarItem}>
                    {
                        props.location.pathname === '/backpack' ?
                            <Image source={require('../resources/images/BackpackActive.png')} style={{height: 30, width: 30}} /> :
                            <Image source={require('../resources/images/BackpackIdle.png')} style={{height: 30, width: 30}} />
                    }
                </View>
            </Link>
            <Link to="/notification" component={Touchable} style={BaseStyles.toolbarTouchable}>
                <View style={BaseStyles.toolbarItem}>
                    {
                        props.location.pathname === '/notification' ?
                            <Image source={require('../resources/images/NotificationActive.png')} style={{height: 30, width: 30}} /> :
                            <Image source={require('../resources/images/NotificationIdle.png')} style={{height: 30, width: 30}} />
                    }
                </View>
            </Link>
            <Link to="/profile" component={Touchable} style={BaseStyles.toolbarTouchable}>
                <View style={BaseStyles.toolbarItem}>
                    {
                        _.startsWith(props.location.pathname, '/profile') ?
                            <Image source={require('../resources/images/ProfileActive.png')} style={{height: 30, width: 30}} /> :
                            <Image source={require('../resources/images/ProfileIdle.png')} style={{height: 30, width: 30}} />
                    }
                </View>
            </Link>
        </View>
    )
}

export default withRouter(ToolbarLMS);