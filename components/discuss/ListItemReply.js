import React, {Component, Fragment} from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import {Link} from 'react-router-native';
import HTML from 'react-native-render-html';
import { CNToolbar, getInitialObject, CNRichTextView, getDefaultStyles, convertToHtmlString } from "react-native-cn-richtext-editor";
import Moment from 'react-moment';

import * as StyleConstants from '../../styles/constants';
import BaseStyles from '../../styles/base';
import ListStyles from '../../styles/lists';
import {Avatar} from "react-native-elements";

const defaultStyles = getDefaultStyles();

const ListItem = props => {

    return(
        <Link component={TouchableOpacity}>
            <View style={{flexDirection: 'row', flex: 1}}>
                <View style={{flex: 0.1}}>
                    {
                        props.item.user.data.avatar === ''
                            ? <Avatar
                                rounded
                                source={require('../../resources/images/ProfileActive.png')}/>
                            : <Avatar
                                rounded
                                source={{uri: props.item.user.data.avatar}}/>
                    }
                </View>
                <View style={{flexDirection: 'column', flex: 0.9, paddingLeft: 10}}>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: "space-between"}}>
                        <Text style={{...BaseStyles.h5, textAlign: 'left', flex: 1, paddingTop: 2, paddingBottom: 0}}>{props.item.user.data.name}</Text>
                        <Moment element={Text} format="D MMM YYYY">{props.item.createdAt}</Moment>
                    </View>
                    <View style={{flex: 1}}>
                        <HTML
                            html={props.item.message}
                            styleList={defaultStyles}/>
                    </View>
                </View>
            </View>
        </Link>
    )
}

export default ListItem;