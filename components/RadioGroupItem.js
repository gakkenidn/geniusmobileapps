import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import BaseStyles from '../styles/base';
import * as StyleConstants from '../styles/constants';

const RadioGroupItem = props => (
  <TouchableOpacity onPress={ () => props.onSelect() }>
    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginRight: StyleConstants.SPACING.X3, paddingVertical: StyleConstants.SPACING.X2 }}>
      {
        props.chosen
          ? <View style={{ height: 26, width: 26, backgroundColor: StyleConstants.COLOR.PRIMARY, borderRadius: 13 }} />
          : <View style={{ height: 26, width: 26, borderColor: StyleConstants.COLOR_GRAY.LIGHT, borderWidth: 3, borderRadius: 13 }} />
      }
      <Text style={{
        fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF,
        fontSize: StyleConstants.FONT_SIZE.H5,
        color: StyleConstants.COLOR_GRAY.DARKEST,
        marginLeft: StyleConstants.SPACING.X2,
      }}>
        { props.label }
      </Text>
    </View>
  </TouchableOpacity>
);

export default RadioGroupItem;