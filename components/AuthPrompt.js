import React, { Fragment } from 'react';
import { Text, View } from 'react-native';
import { Link } from 'react-router-native';
import Touchable from 'react-native-platform-touchable';
import * as StyleConstants from '../styles/constants';
import BaseStyles from '../styles/base';

const AuthPrompt = props => (
  <View style={ BaseStyles.promptContainer }>
      <View style={ BaseStyles.content }>
      {
        props.authenticated ?
        (
          <Link to={ props.action }>
            <View style={ BaseStyles.promptButton }>
              <Text style={ BaseStyles.promptButtonText }>
                { props.actionText }
              </Text>
            </View>
          </Link>
        ) :
        (
          <Fragment>
            <View style={{ marginBottom: StyleConstants.SPACING.X2 }}>
              <Text style={ BaseStyles.mutedSmall }>Anda perlu masuk atau daftar terlebih dahulu untuk dapat mengakses { props.content }.</Text>
            </View>
            <Link to="/profile" component={ Touchable }>
              <View style={ BaseStyles.promptButton }>
                <Text style={ BaseStyles.promptButtonText }>
                  Masuk ke Akun Gakken
                </Text>
              </View>
            </Link>
          </Fragment>
        )
      }
    </View>
  </View>
);

export default AuthPrompt;