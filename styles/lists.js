import * as CONSTANTS from './constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({

  itemShadowed: {
    elevation:          2,
    shadowColor:        CONSTANTS.COLOR.BLACK,
    marginBottom:       CONSTANTS.SPACING.X2,
    shadowOffset:       { height: 2 },
    shadowOpacity:      0.2,
    backgroundColor:    CONSTANTS.COLOR.WHITE,
    paddingVertical:    CONSTANTS.SPACING.X3,
    paddingHorizontal:  CONSTANTS.SPACING.X4,
    opacity: 1
  },
  itemShadoweddis: {
    elevation:          2,
    shadowColor:        CONSTANTS.COLOR.BLACK,
    marginBottom:       CONSTANTS.SPACING.X2,
    shadowOffset:       { height: 2 },
    shadowOpacity:      0.2,
    backgroundColor:    CONSTANTS.COLOR.WHITE,
    paddingVertical:    CONSTANTS.SPACING.X3,
    paddingHorizontal:  CONSTANTS.SPACING.X4,
    opacity: 0.7
  }

});