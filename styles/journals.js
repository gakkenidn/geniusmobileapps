import * as CONSTANTS from './constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({

  categoryList: {
    elevation:        2,
    shadowColor:      CONSTANTS.COLOR.BLACK,
    shadowOffset:     { height: 2 },
    shadowOpacity:    0.2,
    borderRadius:     CONSTANTS.SPACING.X2,
    backgroundColor:  CONSTANTS.COLOR.WHITE,
  },
  categoryListItem: {
    paddingHorizontal: CONSTANTS.SPACING.X3,
    paddingVertical: CONSTANTS.SPACING.X2,
    borderBottomWidth: 1,
    borderBottomColor: CONSTANTS.COLOR_GRAY.LIGHTEST,
  },
  categoryListItemEnd: {
    paddingHorizontal: CONSTANTS.SPACING.X3,
    paddingVertical: CONSTANTS.SPACING.X2,
  },
  categoryListItemText: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
    fontSize: CONSTANTS.FONT_SIZE.H5
  },

});

export const description = StyleSheet.create({
  h1: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
    fontSize: CONSTANTS.FONT_SIZE.H1,
    lineHeight: CONSTANTS.FONT_SIZE.H1 * CONSTANTS.LINE_HEIGHT_BASE,
  },
  h2: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
    fontSize: CONSTANTS.FONT_SIZE.H2,
    lineHeight: CONSTANTS.FONT_SIZE.H2 * CONSTANTS.LINE_HEIGHT_BASE,
  },
  h3: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
    fontSize: CONSTANTS.FONT_SIZE.H3,
    lineHeight: CONSTANTS.FONT_SIZE.H3 * CONSTANTS.LINE_HEIGHT_BASE,
  },
  h4: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
    fontSize: CONSTANTS.FONT_SIZE.H4,
    lineHeight: CONSTANTS.FONT_SIZE.H4 * CONSTANTS.LINE_HEIGHT_BASE,
  },
  h5: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
    fontSize: CONSTANTS.FONT_SIZE.H5,
    lineHeight: CONSTANTS.FONT_SIZE.H5 * CONSTANTS.LINE_HEIGHT_BASE,
  },
  h6: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
    fontSize: CONSTANTS.FONT_SIZE.H6,
    lineHeight: CONSTANTS.FONT_SIZE.H6 * CONSTANTS.LINE_HEIGHT_BASE,
  },
  p: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
    fontSize: CONSTANTS.FONT_SIZE_BASE,
    lineHeight: CONSTANTS.FONT_SIZE_BASE * CONSTANTS.LINE_HEIGHT_BASE,
  },
  li: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
    fontSize: CONSTANTS.FONT_SIZE_BASE,
    lineHeight: CONSTANTS.FONT_SIZE_BASE * CONSTANTS.LINE_HEIGHT_BASE,
  },
  em: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF_ITALIC,
    fontSize: CONSTANTS.FONT_SIZE.H4,
    lineHeight: CONSTANTS.FONT_SIZE.H4 * CONSTANTS.LINE_HEIGHT_BASE,
  },
  a: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
    fontSize: CONSTANTS.FONT_SIZE.H4,
    color: CONSTANTS.COLOR.PRIMARY,
    lineHeight: CONSTANTS.FONT_SIZE.H4 * CONSTANTS.LINE_HEIGHT_BASE,
  },
  strong: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF_STRONG,
    fontSize: CONSTANTS.FONT_SIZE.H4,
    lineHeight: CONSTANTS.FONT_SIZE.H4 * CONSTANTS.LINE_HEIGHT_BASE,
  },
});