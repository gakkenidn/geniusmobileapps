import {Platform, StyleSheet} from 'react-native';
import * as CONSTANTS from './constants';

export default StyleSheet.create({

  h1: {
    color:        CONSTANTS.COLOR_GRAY.DARK,
    fontSize:     CONSTANTS.FONT_SIZE.H1,
    fontFamily:   CONSTANTS.FONT_FAMILY_HEADINGS,
    lineHeight:   CONSTANTS.FONT_SIZE.H1 * CONSTANTS.LINE_HEIGHT_BASE,
    marginBottom: CONSTANTS.SPACING.X2,
  },
  h2: {
    color:        CONSTANTS.COLOR_GRAY.DARK,
    fontSize:     CONSTANTS.FONT_SIZE.H2,
    fontFamily:   CONSTANTS.FONT_FAMILY_HEADINGS,
    lineHeight:   CONSTANTS.FONT_SIZE.H2 * CONSTANTS.LINE_HEIGHT_BASE,
    marginBottom: CONSTANTS.SPACING.X2,
  },
  h2Inline: {
    color:        CONSTANTS.COLOR_GRAY.DARK,
    fontSize:     CONSTANTS.FONT_SIZE.H2,
    fontFamily:   CONSTANTS.FONT_FAMILY_HEADINGS,
    lineHeight:   CONSTANTS.FONT_SIZE.H2 * CONSTANTS.LINE_HEIGHT_BASE,
  },
  h3: {
    color:        CONSTANTS.COLOR_GRAY.DARK,
    fontSize:     CONSTANTS.FONT_SIZE.H3,
    fontFamily:   CONSTANTS.FONT_FAMILY_HEADINGS,
    lineHeight:   CONSTANTS.FONT_SIZE.H3 * CONSTANTS.LINE_HEIGHT_BASE,
    marginBottom: CONSTANTS.SPACING.X2,
  },
  h4: {
    color:        CONSTANTS.COLOR_GRAY.DARK,
    fontSize:     CONSTANTS.FONT_SIZE.H4,
    fontFamily:   CONSTANTS.FONT_FAMILY_HEADINGS,
    lineHeight:   CONSTANTS.FONT_SIZE.H4 * CONSTANTS.LINE_HEIGHT_BASE,
    marginBottom: CONSTANTS.SPACING.X2,
  },
  h5: {
    color:        CONSTANTS.COLOR_GRAY.DARK,
    fontSize:     CONSTANTS.FONT_SIZE.H5,
    fontFamily:   CONSTANTS.FONT_FAMILY_HEADINGS,
    lineHeight:   CONSTANTS.FONT_SIZE.H5 * CONSTANTS.LINE_HEIGHT_BASE,
    marginBottom: CONSTANTS.SPACING.X2,
  },
  h5Inline: {
    color:        CONSTANTS.COLOR_GRAY.DARK,
    fontSize:     CONSTANTS.FONT_SIZE.H5,
    fontFamily:   CONSTANTS.FONT_FAMILY_HEADINGS,
    lineHeight:   CONSTANTS.FONT_SIZE.H5 * CONSTANTS.LINE_HEIGHT_BASE,
  },
  h6: {
    color:        CONSTANTS.COLOR_GRAY.DARK,
    fontSize:     CONSTANTS.FONT_SIZE.H6,
    fontFamily:   CONSTANTS.FONT_FAMILY_HEADINGS,
    lineHeight:   CONSTANTS.FONT_SIZE.H6 * CONSTANTS.LINE_HEIGHT_BASE,
    marginBottom: CONSTANTS.SPACING.X2,
  },
  topic: {
    color:        CONSTANTS.COLOR.BLUE,
    fontSize:     CONSTANTS.FONT_SIZE.H5,
    fontFamily:   CONSTANTS.FONT_FAMILY_HEADINGS,
    lineHeight:   CONSTANTS.FONT_SIZE.H3 * CONSTANTS.LINE_HEIGHT_BASE,
  },
  p: {
    color:        CONSTANTS.COLOR_GRAY.DARK,
    fontSize:     CONSTANTS.FONT_SIZE_BASE,
    fontFamily:   CONSTANTS.FONT_FAMILY_SANS_SERIF,
    lineHeight:   CONSTANTS.FONT_SIZE_BASE * CONSTANTS.LINE_HEIGHT_BASE,
    marginBottom: CONSTANTS.SPACING.X2,
  },
  text: {
    color:        CONSTANTS.COLOR_GRAY.DARK,
    fontSize:     CONSTANTS.FONT_SIZE_BASE,
    fontFamily:   CONSTANTS.FONT_FAMILY_SANS_SERIF,
    lineHeight:   CONSTANTS.FONT_SIZE_BASE * CONSTANTS.LINE_HEIGHT_BASE,
  },
  textInline: {
    color:        CONSTANTS.COLOR_GRAY.DARK,
    fontSize:     CONSTANTS.FONT_SIZE_BASE,
    fontFamily:   CONSTANTS.FONT_FAMILY_SANS_SERIF,
  },
  textInlineWhite: {
    color:        CONSTANTS.COLOR.WHITE,
    fontSize:     CONSTANTS.FONT_SIZE_BASE,
    fontFamily:   CONSTANTS.FONT_FAMILY_SANS_SERIF,
  },
  textLink: {
    color:        CONSTANTS.COLOR.PRIMARY,
    fontSize:     CONSTANTS.FONT_SIZE_BASE,
    fontFamily:   CONSTANTS.FONT_FAMILY_SANS_SERIF,
  },
  textInput: {
    color:        CONSTANTS.COLOR_GRAY.DARKEST,
    padding:      0,
    fontSize:     CONSTANTS.FONT_SIZE.H4,
    fontFamily:   CONSTANTS.FONT_FAMILY_SANS_SERIF,
  },
  muted: {
    color:        CONSTANTS.COLOR_GRAY.NORMAL,
    fontSize:     CONSTANTS.FONT_SIZE_BASE,
    fontFamily:   CONSTANTS.FONT_FAMILY_SANS_SERIF,
    lineHeight:   CONSTANTS.FONT_SIZE_BASE * CONSTANTS.LINE_HEIGHT_BASE,
  },
  mutedSmall: {
    color:        CONSTANTS.COLOR_GRAY.NORMAL,
    fontSize:     CONSTANTS.FONT_SIZE_BASE - 1,
    fontFamily:   CONSTANTS.FONT_FAMILY_SANS_SERIF,
    lineHeight:   CONSTANTS.FONT_SIZE_BASE * CONSTANTS.LINE_HEIGHT_BASE,
  },

  container: {
    flex:             1,
    alignItems:       'stretch',
    justifyContent:   'flex-start',
    backgroundColor:  CONSTANTS.COLOR_GRAY.LIGHTEST,
  },
  overlayContainer: {
    top:              0,
    left:             0,
    width:            '100%',
    bottom:           40,
    position:         'absolute',
    justifyContent:   'flex-start',
    backgroundColor:  CONSTANTS.COLOR.WHITE,
  },
  overlayContainerLMS: {
    left:             0,
    top:              0,
    bottom:           0,
    width:            '100%',
    position:         'absolute',
    justifyContent:   'flex-start',
    backgroundColor:  CONSTANTS.COLOR.WHITE,
  },

  content: {
    paddingHorizontal: CONSTANTS.SPACING.X4,
  },

  toolbar: {
    zIndex:         995,
    alignItems:     'center',
    flexDirection:  'row',
    borderTopColor: CONSTANTS.COLOR_GRAY.LIGHTEST,
    borderTopWidth: 1,
    justifyContent: 'space-around',
    backgroundColor: CONSTANTS.COLOR.WHITE,
  },

  toolbarTouchable: {
    width:          '25%',
  },

  toolbarItem: {
    alignItems:     'center',
    justifyContent: 'center',
    paddingVertical: 5,
  },

  toolbarItemText: {
    paddingTop: 2,
    color: CONSTANTS.COLOR_GRAY.DARK,
    fontSize: CONSTANTS.FONT_SIZE_BASE - 2,
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF
  },

  toolbarItemTextActive: {
    color:      CONSTANTS.COLOR.PRIMARY,
    fontSize:   CONSTANTS.FONT_SIZE_BASE - 2,
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
    paddingTop: 2,
  },

  topNavbar: {
    height:           45,
    zIndex:           990,
    elevation:        2,
    shadowColor:      CONSTANTS.COLOR.BLACK,
    shadowOpacity:    0.2,
    shadowOffset:     { height: 2 },
    flexDirection:    'row',
    backgroundColor:  CONSTANTS.COLOR.WHITE,
  },

  topNavbarItem: {
    flex:           1,
    alignItems:     'center',
    paddingTop:     4,
    paddingBottom:  4,
    justifyContent: 'center',
  },

  topNavbarItemActive: {
    flex:               1,
    alignItems:         'center',
    paddingTop:         4,
    paddingBottom:      0,
    justifyContent:     'center',
    borderBottomColor:  CONSTANTS.COLOR.PRIMARY,
    borderBottomWidth:  4,
  },

  topNavbarItemText: {
    color:      CONSTANTS.COLOR_GRAY.NORMAL,
    fontSize:   CONSTANTS.FONT_SIZE_BASE,
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
  },

  promptContainer: {
    zIndex:           980,
    elevation:        1,
    shadowColor:      CONSTANTS.COLOR.BLACK,
    shadowOffset:     { height: 1 },
    shadowOpacity:    0.2,
    borderTopColor:   CONSTANTS.COLOR_GRAY.LIGHTEST,
    borderTopWidth:   1,
    backgroundColor:  CONSTANTS.COLOR.WHITE,
    paddingVertical:  CONSTANTS.SPACING.X3,
  },

  promptButton: {
    alignItems:         'center',
    borderRadius:       CONSTANTS.SPACING_BASE,
    backgroundColor:    CONSTANTS.COLOR.PRIMARY,
    paddingVertical:    CONSTANTS.SPACING.X2,
    paddingHorizontal:  CONSTANTS.SPACING.X3,
  },

  promptButtonImage: {
    borderRadius: CONSTANTS.SPACING.X2,
    width: 150,
    height: 100,
    elevation: 2,
    backgroundColor: CONSTANTS.COLOR.WHITE,
    borderWidth: Platform.OS === 'ios' ? 1 : 0,
    marginBottom: CONSTANTS.SPACING.X2,
    alignItems: 'center',
    justifyContent: 'center',
  },

  promptButtonText: {
    color:      CONSTANTS.COLOR.WHITE,
    fontSize:   CONSTANTS.FONT_SIZE_BASE,
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
  },

  badgeSuccess: {
    borderRadius:       CONSTANTS.SPACING_BASE,
    backgroundColor:    CONSTANTS.COLOR.GREEN,
    paddingVertical:    CONSTANTS.SPACING_BASE,
    paddingHorizontal:  CONSTANTS.SPACING.X2,
  },

  badgeWarning: {
    borderRadius:       CONSTANTS.SPACING_BASE,
    backgroundColor:    CONSTANTS.COLOR.AMBER,
    paddingVertical:    CONSTANTS.SPACING_BASE,
    paddingHorizontal:  CONSTANTS.SPACING.X2,
  },
  badgeTopic: {
    flex: 0.25,
    textAlign: 'center',
    borderRadius:3,
    color: CONSTANTS.COLOR.WHITE
  }

});