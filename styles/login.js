import { Platform, StyleSheet } from 'react-native';
import * as CONSTANTS from './constants';

export default StyleSheet.create({

  container: {
    width:          '100%',
    height:         '100%',
    alignItems:     'stretch',
    justifyContent: 'flex-start',
  },

  headerContainer: {
    width:              '100%',
    alignItems:         'stretch',
    flexDirection:      'row',
    justifyContent:     'flex-end',
    paddingVertical:    CONSTANTS.SPACING.X4,
    paddingHorizontal:  CONSTANTS.SPACING.X4,
  },

  activeHeading: {
    color:        CONSTANTS.COLOR_GRAY.LIGHT,
    fontSize:     CONSTANTS.FONT_SIZE.H1,
    maxWidth:     200,
    fontFamily:   CONSTANTS.FONT_FAMILY_HEADINGS,
    lineHeight:   CONSTANTS.LINE_HEIGHT_BASE * CONSTANTS.FONT_SIZE.H2,
    marginRight:  CONSTANTS.SPACING.X3,
  },

  formContainer: {
    width:              '100%',
    alignItems:         'center',
    paddingTop:         CONSTANTS.SPACING.X3,
    paddingBottom:      CONSTANTS.SPACING.X5,
    overflow:         'visible',
    paddingHorizontal:  CONSTANTS.SPACING.X4,
  },

  headingContainer: {
    width:              '100%',
    marginBottom:       CONSTANTS.SPACING.X4,
    flexDirection:      'row',
    justifyContent:     'flex-start',
    paddingHorizontal:  CONSTANTS.SPACING.X4,
  },

  inactiveHeading: {
    color:        CONSTANTS.COLOR_GRAY.LIGHTEST,
    fontSize:     CONSTANTS.FONT_SIZE.H1,
    maxWidth:     200,
    fontFamily:   CONSTANTS.FONT_FAMILY_HEADINGS,
    lineHeight:   CONSTANTS.LINE_HEIGHT_BASE * CONSTANTS.FONT_SIZE.H2,
    marginRight:  CONSTANTS.SPACING.X3,
  },

  errorContainer: {
    marginBottom: CONSTANTS.SPACING.X3,
    backgroundColor: CONSTANTS.COLOR_GRAY.DARK,
    paddingVertical: CONSTANTS.SPACING.X2,
    paddingHorizontal: CONSTANTS.SPACING.X3,
  },

  errorText: {
    color:      CONSTANTS.COLOR_GRAY.LIGHTEST,
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
  },

  inputContainer: {
    width:            '100%',
    position:         'relative',
    elevation:        2,
    shadowColor:      CONSTANTS.COLOR.BLACK,
    borderRadius:     CONSTANTS.SPACING.X2,
    shadowOffset:     { height: 2 },
    marginBottom:     CONSTANTS.SPACING.X3,
    shadowOpacity:    0.2,
    backgroundColor:  CONSTANTS.COLOR.WHITE,
  },

  input: {
    width:            '100%',
    fontSize:         CONSTANTS.FONT_SIZE.H6,
    fontFamily:       CONSTANTS.FONT_FAMILY_SANS_SERIF,
    paddingLeft:      65,
    paddingRight:     CONSTANTS.SPACING.X2,
    paddingVertical:  Platform.OS === 'ios' ? CONSTANTS.SPACING.X2 + 3 : CONSTANTS.SPACING.X2,
  },

  inputErrorContainer: {
    top:                0,
    right:              0,
    position:           'absolute',
    backgroundColor:    CONSTANTS.COLOR_GRAY.DARKEST,
    paddingVertical:    CONSTANTS.SPACING.X2,
    paddingHorizontal:  CONSTANTS.SPACING.X3,
  },

  inputErrorText: {
    color:      CONSTANTS.COLOR_GRAY.LIGHTEST,
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
  },

  submitBtn: {
    width:            200,
    elevation:        2,
    shadowColor:      CONSTANTS.COLOR.BLACK,
    borderRadius:     CONSTANTS.SPACING.X2,
    shadowOffset:     { height: 2 },
    shadowOpacity:    0.2,
    backgroundColor:  CONSTANTS.COLOR.PRIMARY,
    paddingVertical:  CONSTANTS.SPACING.X2,
  },

  submitBtnDisabled: {
    width:            200,
    elevation:        2,
    shadowColor:      CONSTANTS.COLOR.BLACK,
    borderRadius:     CONSTANTS.SPACING.X2,
    shadowOffset:     { height: 2 },
    shadowOpacity:    0.2,
    backgroundColor:  CONSTANTS.COLOR_GRAY.LIGHT,
    paddingVertical:  CONSTANTS.SPACING.X2,
  },

  submitBtnText: {
    color:      CONSTANTS.COLOR.WHITE,
    fontSize:   CONSTANTS.FONT_SIZE.H6,
    textAlign:  'center',
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
  }

});