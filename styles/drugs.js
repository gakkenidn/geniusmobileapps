import * as CONSTANTS from './constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({

  categoryList: {
    elevation:        2,
    shadowColor:      CONSTANTS.COLOR.BLACK,
    shadowOffset:     { height: 2 },
    shadowOpacity:    0.2,
    borderRadius:     CONSTANTS.SPACING.X2,
    backgroundColor:  CONSTANTS.COLOR.WHITE,
  },
  categoryListItem: {
    paddingHorizontal: CONSTANTS.SPACING.X3,
    paddingVertical: CONSTANTS.SPACING.X2,
    borderBottomWidth: 1,
    borderBottomColor: CONSTANTS.COLOR_GRAY.LIGHTEST,
  },
  categoryListItemEnd: {
    paddingHorizontal: CONSTANTS.SPACING.X3,
    paddingVertical: CONSTANTS.SPACING.X2,
  },
  categoryListItemText: {
    fontFamily: CONSTANTS.FONT_FAMILY_SANS_SERIF,
    fontSize: CONSTANTS.FONT_SIZE.H5
  },

});