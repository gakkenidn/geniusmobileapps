import React, { Component } from 'react';
import { Image, View } from 'react-native-animatable';
import * as StyleConstants from '../styles/constants';

class Splash extends Component {
  state = {  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.ready && (nextProps !== this.props.ready)) {
      setTimeout(() => {
        this.refs.baseView.fadeOut(300);
        this.refs.logo.transitionTo({ height: 1610, width: 1500 }, 300, 'ease-in-out-sine');
        setTimeout(() => {
          this.props.onDone();
        }, 300);
      }, 1000);
    }
  }

  render() {
    return (
      <View style={{ height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: StyleConstants.COLOR.PRIMARY }} ref="baseView">
        <Image source={ require('../resources/images/LogoG.png') } style={{ height: 161, width: 150 }} ref="logo" />
      </View>
    );
  }
}

export default Splash;