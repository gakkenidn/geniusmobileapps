import React, { Component } from 'react';
import { View as AnimatableView, Text as AnimatableText } from 'react-native-animatable';
import { Image, Modal, Text, TouchableWithoutFeedback, View } from 'react-native';
import Touchable from 'react-native-platform-touchable';

import LoginStyles from '../styles/login';
import * as StyleConstants from '../styles/constants';
import LoginForm from '../components/login/LoginForm';
import RegisterForm from '../components/login/RegisterForm';

class Login extends Component {
  state = {
    page: 'login',
    busy: false
  }

  switchPage({ page }) {
    if (this.state.busy)
      return;

    this.setState({ page });
  }

  render() {
    return (
      <Modal animationType="fade" onRequestClose={ () => { if (!this.state.authenticating) this.props.onCancel(); }}>
        <View style={ LoginStyles.container }>
          <View style={ LoginStyles.headerContainer }>
            <Touchable onPress={ () => { if (!this.state.authenticating) this.props.onCancel(); } }>
              <Image source={ require('../resources/images/Close-Dark.png') } style={{ width: 20, height: 20 }} />
            </Touchable>
          </View>
          <View>
            <View style={ LoginStyles.headingContainer }>
              <TouchableWithoutFeedback onPress={ () => this.switchPage({ page: 'login' }) }>
                <AnimatableText transition="color" style={ this.state.page === 'login' ? LoginStyles.activeHeading : LoginStyles.inactiveHeading }>Masuk</AnimatableText>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={ () => this.switchPage({ page: 'register' }) }>
                <AnimatableText transition="color" style={ this.state.page === 'register' ? LoginStyles.activeHeading : LoginStyles.inactiveHeading }>Daftar</AnimatableText>
              </TouchableWithoutFeedback>
            </View>

            {
              this.state.page === 'login' ?
                <LoginForm
                  onSuccess={ ({ access, refresh }) => this.props.onAuthenticated({ access, refresh }) }
                  onSubmit={ () => this.setState({ busy: true }) }
                  onFinish={ () => this.setState({ busy: false }) } /> :
                <RegisterForm
                  onSuccess={ ({ access, refresh }) => this.props.onAuthenticated({ access, refresh }) }
                  onSubmit={ () => this.setState({ busy: true }) }
                  onFinish={ () => this.setState({ busy: false }) }/>
            }
          </View>
        </View>
      </Modal>
    );
  }
}

export default Login;