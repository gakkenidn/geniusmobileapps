import React, {Component, Fragment} from 'react';
import {FlatList, Text, View} from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import BaseStyles from "../../styles/base";
import Header from "../../components/HeaderLMS";
import ListItem from "../../components/notification/ListItem";

class Notification extends Component{

    constructor(props){
        super(props);
        this.state = {
            dataNotif: {
                'data': [
                    {
                        'title': 'Notif Satu',
                        'desc' : 'bla bla bla',
                        'key': 'satu'
                    },
                    {
                        'title': 'Notif Dua',
                        'desc' : 'bla bla bla',
                        'key': 'dua'
                    },
                    {
                        'title': 'Notif Satu',
                        'desc' : 'bla bla bla',
                        'key': 'tiga'
                    },
                    {
                        'title': 'Notif Satu',
                        'desc' : 'bla bla bla',
                        'key': 'empat'
                    },
                ]
            }
        }
    }

    state = {
        exit: false,
        fetching: false,
        error: false,
        loaded: false,
        topic: {},
        content: [],
    }

    componentDidMount(){

    }

    render(){
        return (
            <Fragment>
                <AnimatableView style={ BaseStyles.container }>
                    <Header title="Notifikasi" logo={true}/>
                    <FlatList data={this.state.dataNotif.data}
                              renderItem={ ({item}) => <ListItem item={item} withFeatured={true}/>}
                              keyExtractor={item => item.key}
                    />
                </AnimatableView>
            </Fragment>
        );
    }
}

export default Notification;