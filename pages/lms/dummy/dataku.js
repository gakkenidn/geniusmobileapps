export const dataTopik = {
    "data": [
    {
        "id": 24,
        "type": "assignment",
        "order": 1,
        "availableAt": null,
        "expiresAt": "2019-10-19T17:02:18+00:00",
        "expiresAtForHumans": "3 bulan dari sekarang",
        "finalGradeWeight": 1,
        "maxAttempts": 0,
        "createdAt": "2019-05-08T10:03:33+00:00",
        "createdAtForHumans": "1 bulan yang lalu",
        "isOpen": true,
        "createdBy": {
            "data": {
                "name": "Lavinia Wintheiser",
                "username": "oleta.hackett",
                "initials": "LW",
                "avatar": "http://lms.local/lms.local/storage/653/avatar-05.png",
                "email": null,
                "detail": null
            }
        },
        "prerequisites": {
            "data": []
        },
        "teachableItem": {
            "data": {
                "id": 8,
                "title": "Rerum Et",
                "description": "Alias doloribus voluptas fugit ut ut tempora. Eos id distinctio vero veniam. Quas reprehenderit culpa cum enim tenetur tempora sunt aperiam."
            }
        },
        "teachableSelf": {
            "data": {
                "id": 362,
                "completedAt": null,
                "completedAtForHumans": ""
            }
        }
    },
    {
        "id": 25,
        "type": "assignment",
        "order": 2,
        "availableAt": null,
        "expiresAt": "2019-10-30T00:06:18+00:00",
        "expiresAtForHumans": "4 bulan dari sekarang",
        "finalGradeWeight": 1,
        "maxAttempts": 0,
        "createdAt": "2019-05-08T20:38:54+00:00",
        "createdAtForHumans": "1 bulan yang lalu",
        "isOpen": true,
        "createdBy": {
            "data": {
                "name": "Lavinia Wintheiser",
                "username": "oleta.hackett",
                "initials": "LW",
                "avatar": "http://lms.local/lms.local/storage/653/avatar-05.png",
                "email": null,
                "detail": null
            }
        },
        "prerequisites": {
            "data": []
        },
        "teachableItem": {
            "data": {
                "id": 9,
                "title": "Culpa Omnis Corporis",
                "description": "Fugit pariatur eveniet vitae. Enim aut maiores ex dolor voluptatem. Non velit ut doloribus est at explicabo. Illum rem omnis quis quaerat saepe molestiae repudiandae."
            }
        },
        "teachableSelf": {
            "data": {
                "id": 379,
                "completedAt": "2019-06-11 18:48:59",
                "completedAtForHumans": "2 minggu yang lalu"
            }
        }
    },
    {
        "id": 26,
        "type": "resource",
        "order": 3,
        "availableAt": null,
        "expiresAt": null,
        "expiresAtForHumans": "1 detik yang lalu",
        "finalGradeWeight": 1,
        "maxAttempts": 0,
        "createdAt": "2019-06-02T21:11:35+00:00",
        "createdAtForHumans": "3 minggu yang lalu",
        "isOpen": false,
        "createdBy": {
            "data": {
                "name": "Lavinia Wintheiser",
                "username": "oleta.hackett",
                "initials": "LW",
                "avatar": "http://lms.local/lms.local/storage/653/avatar-05.png",
                "email": null,
                "detail": null
            }
        },
        "prerequisites": {
            "data": []
        },
        "teachableItem": {
            "data": {
                "id": 10,
                "type": "audio",
                "title": "Vel Maxime Dolore Aut Quo Id Sunt",
                "description": "Et dolores cupiditate numquam enim eveniet quis. Id distinctio mollitia culpa odio. Sit consequatur iure repellat numquam a optio. Ex est culpa aliquam omnis aut. Veritatis consequatur est qui earum pariatur quisquam.",
                "data": "[]"
            }
        },
        "teachableSelf": null
    },
    {
        "id": 27,
        "type": "quiz",
        "order": 4,
        "availableAt": null,
        "expiresAt": "2019-07-19T17:37:26+00:00",
        "expiresAtForHumans": "3 minggu dari sekarang",
        "finalGradeWeight": 1,
        "maxAttempts": 5,
        "createdAt": "2019-04-22T22:55:18+00:00",
        "createdAtForHumans": "2 bulan yang lalu",
        "isOpen": true,
        "createdBy": {
            "data": {
                "name": "Lavinia Wintheiser",
                "username": "oleta.hackett",
                "initials": "LW",
                "avatar": "http://lms.local/lms.local/storage/653/avatar-05.png",
                "email": null,
                "detail": null
            }
        },
        "prerequisites": {
            "data": [
                {
                    "id": 26,
                    "requirable": {
                        "data": {
                            "id": 25,
                            "type": "assignment",
                            "order": 2,
                            "availableAt": null,
                            "expiresAt": "2019-10-30T00:06:18+00:00",
                            "expiresAtForHumans": "4 bulan dari sekarang",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-05-08T20:38:54+00:00",
                            "createdAtForHumans": "1 bulan yang lalu",
                            "isOpen": true,
                            "teachableItem": {
                                "data": {
                                    "id": 9,
                                    "title": "Culpa Omnis Corporis",
                                    "description": "Fugit pariatur eveniet vitae. Enim aut maiores ex dolor voluptatem. Non velit ut doloribus est at explicabo. Illum rem omnis quis quaerat saepe molestiae repudiandae."
                                }
                            }
                        }
                    }
                },
                {
                    "id": 27,
                    "requirable": {
                        "data": {
                            "id": 26,
                            "type": "resource",
                            "order": 3,
                            "availableAt": null,
                            "expiresAt": null,
                            "expiresAtForHumans": "1 detik yang lalu",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-06-02T21:11:35+00:00",
                            "createdAtForHumans": "3 minggu yang lalu",
                            "isOpen": false,
                            "teachableItem": {
                                "data": {
                                    "id": 10,
                                    "type": "audio",
                                    "title": "Vel Maxime Dolore Aut Quo Id Sunt",
                                    "description": "Et dolores cupiditate numquam enim eveniet quis. Id distinctio mollitia culpa odio. Sit consequatur iure repellat numquam a optio. Ex est culpa aliquam omnis aut. Veritatis consequatur est qui earum pariatur quisquam.",
                                    "data": "[]"
                                }
                            }
                        }
                    }
                },
                {
                    "id": 28,
                    "requirable": {
                        "data": {
                            "id": 28,
                            "type": "resource",
                            "order": 5,
                            "availableAt": null,
                            "expiresAt": null,
                            "expiresAtForHumans": "1 detik yang lalu",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-05-26T08:07:38+00:00",
                            "createdAtForHumans": "1 bulan yang lalu",
                            "isOpen": false,
                            "teachableItem": {
                                "data": {
                                    "id": 11,
                                    "type": "youtubevideo",
                                    "title": "Awesome Laptops and Tech from CES 2018!",
                                    "description": "Ut debitis quibusdam sunt sint ut et. Aut totam voluptatum voluptatem asperiores ipsa est. Dolor eius nesciunt omnis magnam vitae.",
                                    "data": "{\"videoId\":\"egaGs_mFQns\"}"
                                }
                            }
                        }
                    }
                },
                {
                    "id": 29,
                    "requirable": {
                        "data": {
                            "id": 30,
                            "type": "resource",
                            "order": 7,
                            "availableAt": null,
                            "expiresAt": null,
                            "expiresAtForHumans": "1 detik yang lalu",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-06-09T03:19:06+00:00",
                            "createdAtForHumans": "2 minggu yang lalu",
                            "isOpen": false,
                            "teachableItem": {
                                "data": {
                                    "id": 12,
                                    "type": "url",
                                    "title": "Eaque Esse Dignissimos Accusamus Ut Nemo Reiciendis",
                                    "description": "Aut placeat nisi est soluta recusandae ratione qui voluptas. Magnam quod aut architecto rerum sit consequatur alias. Voluptas neque libero suscipit sed tempora consequatur.",
                                    "data": "{\"url\":\"http:\\/\\/www.botsford.com\\/assumenda-excepturi-aut-est-non-iusto-quia-modi\"}"
                                }
                            }
                        }
                    }
                },
                {
                    "id": 30,
                    "requirable": {
                        "data": {
                            "id": 31,
                            "type": "resource",
                            "order": 8,
                            "availableAt": null,
                            "expiresAt": null,
                            "expiresAtForHumans": "1 detik yang lalu",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-05-31T20:41:17+00:00",
                            "createdAtForHumans": "3 minggu yang lalu",
                            "isOpen": false,
                            "teachableItem": {
                                "data": {
                                    "id": 13,
                                    "type": "youtubevideo",
                                    "title": "This Is the OnePlus 6... I Think.",
                                    "description": "Voluptatum unde in id iste. Veritatis libero sapiente qui reiciendis dolorem sint ducimus et. Nostrum dignissimos qui sed consectetur quaerat. Rerum et distinctio incidunt corrupti.",
                                    "data": "{\"videoId\":\"ujws3_SLMPk\"}"
                                }
                            }
                        }
                    }
                },
                {
                    "id": 31,
                    "requirable": {
                        "data": {
                            "id": 33,
                            "type": "assignment",
                            "order": 10,
                            "availableAt": null,
                            "expiresAt": "2019-10-09T17:35:35+00:00",
                            "expiresAtForHumans": "3 bulan dari sekarang",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-06-11T18:49:50+00:00",
                            "createdAtForHumans": "2 minggu yang lalu",
                            "isOpen": true,
                            "teachableItem": {
                                "data": {
                                    "id": 10,
                                    "title": "Omnis Expedita Velit Quod",
                                    "description": "Inventore expedita soluta deleniti occaecati. Similique vel quibusdam amet iusto accusantium neque et. Vitae tempora nisi nihil qui repudiandae et."
                                }
                            }
                        }
                    }
                },
                {
                    "id": 32,
                    "requirable": {
                        "data": {
                            "id": 35,
                            "type": "assignment",
                            "order": 12,
                            "availableAt": null,
                            "expiresAt": "2019-06-18T13:40:58+00:00",
                            "expiresAtForHumans": "1 minggu yang lalu",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-05-19T07:35:54+00:00",
                            "createdAtForHumans": "1 bulan yang lalu",
                            "isOpen": false,
                            "teachableItem": {
                                "data": {
                                    "id": 11,
                                    "title": "Et Nemo Ut",
                                    "description": "Pariatur occaecati dignissimos et sapiente aperiam neque id. Velit voluptate natus non soluta aut quasi quod."
                                }
                            }
                        }
                    }
                }
            ]
        },
        "teachableItem": {
            "data": {
                "grading_method": "Standard",
                "title": "Fuga",
                "description": "Dicta impedit itaque ut sed. Quis molestias numquam animi eos corporis vel. Similique facilis asperiores in nihil et et. Explicabo id molestias sapiente. Et ipsam sunt exercitationem voluptatem qui id est."
            }
        },
        "teachableSelf": {
            "data": {
                "id": 3990,
                "completedAt": null,
                "completedAtForHumans": ""
            }
        }
    },
    {
        "id": 28,
        "type": "resource",
        "order": 5,
        "availableAt": null,
        "expiresAt": null,
        "expiresAtForHumans": "1 detik yang lalu",
        "finalGradeWeight": 1,
        "maxAttempts": 0,
        "createdAt": "2019-05-26T08:07:38+00:00",
        "createdAtForHumans": "1 bulan yang lalu",
        "isOpen": false,
        "createdBy": {
            "data": {
                "name": "Lavinia Wintheiser",
                "username": "oleta.hackett",
                "initials": "LW",
                "avatar": "http://lms.local/lms.local/storage/653/avatar-05.png",
                "email": null,
                "detail": null
            }
        },
        "prerequisites": {
            "data": []
        },
        "teachableItem": {
            "data": {
                "id": 11,
                "type": "video",
                "title": "Awesome Laptops and Tech from CES 2018!",
                "description": "Ut debitis quibusdam sunt sint ut et. Aut totam voluptatum voluptatem asperiores ipsa est. Dolor eius nesciunt omnis magnam vitae.",
                "data": "{\"url\":\"X0IBZGbM\"}"
            }
        },
        "teachableSelf": null
    },
    {
        "id": 29,
        "type": "quiz",
        "order": 6,
        "availableAt": null,
        "expiresAt": "2019-11-08T14:02:10+00:00",
        "expiresAtForHumans": "4 bulan dari sekarang",
        "finalGradeWeight": 1,
        "maxAttempts": 4,
        "createdAt": "2019-05-01T15:19:24+00:00",
        "createdAtForHumans": "1 bulan yang lalu",
        "isOpen": true,
        "createdBy": {
            "data": {
                "name": "Lavinia Wintheiser",
                "username": "oleta.hackett",
                "initials": "LW",
                "avatar": "http://lms.local/lms.local/storage/653/avatar-05.png",
                "email": null,
                "detail": null
            }
        },
        "prerequisites": {
            "data": [
                {
                    "id": 33,
                    "requirable": {
                        "data": {
                            "id": 24,
                            "type": "assignment",
                            "order": 1,
                            "availableAt": null,
                            "expiresAt": "2019-10-19T17:02:18+00:00",
                            "expiresAtForHumans": "3 bulan dari sekarang",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-05-08T10:03:33+00:00",
                            "createdAtForHumans": "1 bulan yang lalu",
                            "isOpen": true,
                            "teachableItem": {
                                "data": {
                                    "id": 8,
                                    "title": "Rerum Et",
                                    "description": "Alias doloribus voluptas fugit ut ut tempora. Eos id distinctio vero veniam. Quas reprehenderit culpa cum enim tenetur tempora sunt aperiam."
                                }
                            }
                        }
                    }
                },
                {
                    "id": 34,
                    "requirable": {
                        "data": {
                            "id": 26,
                            "type": "resource",
                            "order": 3,
                            "availableAt": null,
                            "expiresAt": null,
                            "expiresAtForHumans": "1 detik yang lalu",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-06-02T21:11:35+00:00",
                            "createdAtForHumans": "3 minggu yang lalu",
                            "isOpen": false,
                            "teachableItem": {
                                "data": {
                                    "id": 10,
                                    "type": "audio",
                                    "title": "Vel Maxime Dolore Aut Quo Id Sunt",
                                    "description": "Et dolores cupiditate numquam enim eveniet quis. Id distinctio mollitia culpa odio. Sit consequatur iure repellat numquam a optio. Ex est culpa aliquam omnis aut. Veritatis consequatur est qui earum pariatur quisquam.",
                                    "data": "[]"
                                }
                            }
                        }
                    }
                },
                {
                    "id": 35,
                    "requirable": {
                        "data": {
                            "id": 28,
                            "type": "resource",
                            "order": 5,
                            "availableAt": null,
                            "expiresAt": null,
                            "expiresAtForHumans": "1 detik yang lalu",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-05-26T08:07:38+00:00",
                            "createdAtForHumans": "1 bulan yang lalu",
                            "isOpen": false,
                            "teachableItem": {
                                "data": {
                                    "id": 11,
                                    "type": "youtubevideo",
                                    "title": "Awesome Laptops and Tech from CES 2018!",
                                    "description": "Ut debitis quibusdam sunt sint ut et. Aut totam voluptatum voluptatem asperiores ipsa est. Dolor eius nesciunt omnis magnam vitae.",
                                    "data": "{\"videoId\":\"egaGs_mFQns\"}"
                                }
                            }
                        }
                    }
                },
                {
                    "id": 36,
                    "requirable": {
                        "data": {
                            "id": 30,
                            "type": "resource",
                            "order": 7,
                            "availableAt": null,
                            "expiresAt": null,
                            "expiresAtForHumans": "1 detik yang lalu",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-06-09T03:19:06+00:00",
                            "createdAtForHumans": "2 minggu yang lalu",
                            "isOpen": false,
                            "teachableItem": {
                                "data": {
                                    "id": 12,
                                    "type": "url",
                                    "title": "Eaque Esse Dignissimos Accusamus Ut Nemo Reiciendis",
                                    "description": "Aut placeat nisi est soluta recusandae ratione qui voluptas. Magnam quod aut architecto rerum sit consequatur alias. Voluptas neque libero suscipit sed tempora consequatur.",
                                    "data": "{\"url\":\"http:\\/\\/www.botsford.com\\/assumenda-excepturi-aut-est-non-iusto-quia-modi\"}"
                                }
                            }
                        }
                    }
                },
                {
                    "id": 37,
                    "requirable": {
                        "data": {
                            "id": 31,
                            "type": "resource",
                            "order": 8,
                            "availableAt": null,
                            "expiresAt": null,
                            "expiresAtForHumans": "1 detik yang lalu",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-05-31T20:41:17+00:00",
                            "createdAtForHumans": "3 minggu yang lalu",
                            "isOpen": false,
                            "teachableItem": {
                                "data": {
                                    "id": 13,
                                    "type": "youtubevideo",
                                    "title": "This Is the OnePlus 6... I Think.",
                                    "description": "Voluptatum unde in id iste. Veritatis libero sapiente qui reiciendis dolorem sint ducimus et. Nostrum dignissimos qui sed consectetur quaerat. Rerum et distinctio incidunt corrupti.",
                                    "data": "{\"videoId\":\"ujws3_SLMPk\"}"
                                }
                            }
                        }
                    }
                },
                {
                    "id": 38,
                    "requirable": {
                        "data": {
                            "id": 33,
                            "type": "assignment",
                            "order": 10,
                            "availableAt": null,
                            "expiresAt": "2019-10-09T17:35:35+00:00",
                            "expiresAtForHumans": "3 bulan dari sekarang",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-06-11T18:49:50+00:00",
                            "createdAtForHumans": "2 minggu yang lalu",
                            "isOpen": true,
                            "teachableItem": {
                                "data": {
                                    "id": 10,
                                    "title": "Omnis Expedita Velit Quod",
                                    "description": "Inventore expedita soluta deleniti occaecati. Similique vel quibusdam amet iusto accusantium neque et. Vitae tempora nisi nihil qui repudiandae et."
                                }
                            }
                        }
                    }
                },
                {
                    "id": 39,
                    "requirable": {
                        "data": {
                            "id": 34,
                            "type": "resource",
                            "order": 11,
                            "availableAt": null,
                            "expiresAt": null,
                            "expiresAtForHumans": "1 detik yang lalu",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-04-15T13:03:50+00:00",
                            "createdAtForHumans": "2 bulan yang lalu",
                            "isOpen": false,
                            "teachableItem": {
                                "data": {
                                    "id": 14,
                                    "type": "youtubevideo",
                                    "title": "This Laptop Has EVERYTHING You Want For Gaming!",
                                    "description": "Sed nostrum voluptatem animi tempora quo. Temporibus et qui dolorem nam ut iusto. Pariatur suscipit consequuntur a dolorum at laudantium.",
                                    "data": "{\"videoId\":\"U0BjO3Z-CXM\"}"
                                }
                            }
                        }
                    }
                },
                {
                    "id": 40,
                    "requirable": {
                        "data": {
                            "id": 35,
                            "type": "assignment",
                            "order": 12,
                            "availableAt": null,
                            "expiresAt": "2019-06-18T13:40:58+00:00",
                            "expiresAtForHumans": "1 minggu yang lalu",
                            "finalGradeWeight": 1,
                            "maxAttempts": 0,
                            "createdAt": "2019-05-19T07:35:54+00:00",
                            "createdAtForHumans": "1 bulan yang lalu",
                            "isOpen": false,
                            "teachableItem": {
                                "data": {
                                    "id": 11,
                                    "title": "Et Nemo Ut",
                                    "description": "Pariatur occaecati dignissimos et sapiente aperiam neque id. Velit voluptate natus non soluta aut quasi quod."
                                }
                            }
                        }
                    }
                }
            ]
        },
        "teachableItem": {
            "data": {
                "grading_method": "Weighted",
                "title": "Unde Qui Saepe",
                "description": "Ullam hic voluptatem quis et. Ipsum ut soluta illo ut amet beatae voluptatem. Non voluptas omnis alias amet ab."
            }
        },
        "teachableSelf": null
    },
    {
        "id": 30,
        "type": "resource",
        "order": 7,
        "availableAt": null,
        "expiresAt": null,
        "expiresAtForHumans": "1 detik yang lalu",
        "finalGradeWeight": 1,
        "maxAttempts": 0,
        "createdAt": "2019-06-09T03:19:06+00:00",
        "createdAtForHumans": "2 minggu yang lalu",
        "isOpen": false,
        "createdBy": {
            "data": {
                "name": "Lavinia Wintheiser",
                "username": "oleta.hackett",
                "initials": "LW",
                "avatar": "http://lms.local/lms.local/storage/653/avatar-05.png",
                "email": null,
                "detail": null
            }
        },
        "prerequisites": {
            "data": []
        },
        "teachableItem": {
            "data": {
                "id": 12,
                "type": "url",
                "title": "Eaque Esse Dignissimos Accusamus Ut Nemo Reiciendis",
                "description": "Aut placeat nisi est soluta recusandae ratione qui voluptas. Magnam quod aut architecto rerum sit consequatur alias. Voluptas neque libero suscipit sed tempora consequatur.",
                "data": "{\"url\":\"http:\\/\\/www.botsford.com\\/assumenda-excepturi-aut-est-non-iusto-quia-modi\"}"
            }
        },
        "teachableSelf": {
            "data": {
                "id": 444,
                "completedAt": "2019-06-11 10:07:38",
                "completedAtForHumans": "2 minggu yang lalu"
            }
        }
    },
    {
        "id": 31,
        "type": "resource",
        "order": 8,
        "availableAt": null,
        "expiresAt": null,
        "expiresAtForHumans": "1 detik yang lalu",
        "finalGradeWeight": 1,
        "maxAttempts": 0,
        "createdAt": "2019-05-31T20:41:17+00:00",
        "createdAtForHumans": "3 minggu yang lalu",
        "isOpen": false,
        "createdBy": {
            "data": {
                "name": "Lavinia Wintheiser",
                "username": "oleta.hackett",
                "initials": "LW",
                "avatar": "http://lms.local/lms.local/storage/653/avatar-05.png",
                "email": null,
                "detail": null
            }
        },
        "prerequisites": {
            "data": []
        },
        "teachableItem": {
            "data": {
                "id": 13,
                "type": "youtubevideo",
                "title": "This Is the OnePlus 6... I Think.",
                "description": "Voluptatum unde in id iste. Veritatis libero sapiente qui reiciendis dolorem sint ducimus et. Nostrum dignissimos qui sed consectetur quaerat. Rerum et distinctio incidunt corrupti.",
                "data": "{\"videoId\":\"ujws3_SLMPk\"}"
            }
        },
        "teachableSelf": {
            "data": {
                "id": 477,
                "completedAt": "2019-06-04 21:53:58",
                "completedAtForHumans": "3 minggu yang lalu"
            }
        }
    },
    {
        "id": 32,
        "type": "quiz",
        "order": 9,
        "availableAt": null,
        "expiresAt": "2019-09-18T20:30:08+00:00",
        "expiresAtForHumans": "2 bulan dari sekarang",
        "finalGradeWeight": 1,
        "maxAttempts": 0,
        "createdAt": "2019-06-04T09:25:40+00:00",
        "createdAtForHumans": "3 minggu yang lalu",
        "isOpen": true,
        "createdBy": {
            "data": {
                "name": "Lavinia Wintheiser",
                "username": "oleta.hackett",
                "initials": "LW",
                "avatar": "http://lms.local/lms.local/storage/653/avatar-05.png",
                "email": null,
                "detail": null
            }
        },
        "prerequisites": {
            "data": []
        },
        "teachableItem": {
            "data": {
                "grading_method": "Standard",
                "title": "Voluptatem Sapiente Magnam",
                "description": "Voluptate natus et eum mollitia velit. Ut cumque a cupiditate necessitatibus officiis tenetur repudiandae."
            }
        },
        "teachableSelf": null
    },
    {
        "id": 33,
        "type": "assignment",
        "order": 10,
        "availableAt": null,
        "expiresAt": "2019-10-09T17:35:35+00:00",
        "expiresAtForHumans": "3 bulan dari sekarang",
        "finalGradeWeight": 1,
        "maxAttempts": 0,
        "createdAt": "2019-06-11T18:49:50+00:00",
        "createdAtForHumans": "2 minggu yang lalu",
        "isOpen": true,
        "createdBy": {
            "data": {
                "name": "Lavinia Wintheiser",
                "username": "oleta.hackett",
                "initials": "LW",
                "avatar": "http://lms.local/lms.local/storage/653/avatar-05.png",
                "email": null,
                "detail": null
            }
        },
        "prerequisites": {
            "data": []
        },
        "teachableItem": {
            "data": {
                "id": 10,
                "title": "Omnis Expedita Velit Quod",
                "description": "Inventore expedita soluta deleniti occaecati. Similique vel quibusdam amet iusto accusantium neque et. Vitae tempora nisi nihil qui repudiandae et."
            }
        },
        "teachableSelf": null
    },
    {
        "id": 34,
        "type": "resource",
        "order": 11,
        "availableAt": null,
        "expiresAt": null,
        "expiresAtForHumans": "1 detik yang lalu",
        "finalGradeWeight": 1,
        "maxAttempts": 0,
        "createdAt": "2019-04-15T13:03:50+00:00",
        "createdAtForHumans": "2 bulan yang lalu",
        "isOpen": false,
        "createdBy": {
            "data": {
                "name": "Lavinia Wintheiser",
                "username": "oleta.hackett",
                "initials": "LW",
                "avatar": "http://lms.local/lms.local/storage/653/avatar-05.png",
                "email": null,
                "detail": null
            }
        },
        "prerequisites": {
            "data": []
        },
        "teachableItem": {
            "data": {
                "id": 14,
                "type": "youtubevideo",
                "title": "This Laptop Has EVERYTHING You Want For Gaming!",
                "description": "Sed nostrum voluptatem animi tempora quo. Temporibus et qui dolorem nam ut iusto. Pariatur suscipit consequuntur a dolorum at laudantium.",
                "data": "{\"videoId\":\"U0BjO3Z-CXM\"}"
            }
        },
        "teachableSelf": null
    },
    {
        "id": 35,
        "type": "assignment",
        "order": 12,
        "availableAt": null,
        "expiresAt": "2019-06-18T13:40:58+00:00",
        "expiresAtForHumans": "1 minggu yang lalu",
        "finalGradeWeight": 1,
        "maxAttempts": 0,
        "createdAt": "2019-05-19T07:35:54+00:00",
        "createdAtForHumans": "1 bulan yang lalu",
        "isOpen": false,
        "createdBy": {
            "data": {
                "name": "Lavinia Wintheiser",
                "username": "oleta.hackett",
                "initials": "LW",
                "avatar": "http://lms.local/lms.local/storage/653/avatar-05.png",
                "email": null,
                "detail": null
            }
        },
        "prerequisites": {
            "data": []
        },
        "teachableItem": {
            "data": {
                "id": 11,
                "title": "Et Nemo Ut",
                "description": "Pariatur occaecati dignissimos et sapiente aperiam neque id. Velit voluptate natus non soluta aut quasi quod."
            }
        },
        "teachableSelf": {
            "data": {
                "id": 531,
                "completedAt": "2019-06-07 00:01:49",
                "completedAtForHumans": "3 minggu yang lalu"
            }
        }
    }
]
};

export const dataKelas = {
    "data":[
        {
            "slug":"kdi101-berpikir-kritis-keterampilan-belajar-dan-evidence-based-learning-ebl-2019-genap",
            "code":"KDI101",
            "subject_id":3,
            "title":"Berpikir Kritis, Keterampilan Belajar dan Evidence Based Learning (EBL)",
            "teaching_period_id":1,
            "description":"Adipisci doloremque accusantium corporis quo aut culpa. Voluptatem harum consequatur consequuntur voluptas et sapiente sed. Aut est nisi laudantium harum.",
            "studentsCount":35,
            "major":3,
            "faculty":1,
            "createdAt":"2019-03-08T00:58:17+00:00",
            "createdAtForHumans":"3 months ago",
            "start_at":null,
            "end_at":null,
            "self":{
                "data":{
                    "id":69,
                    "role":"student",
                    "lastAccessedAt":"2019-06-25T02:14:44+00:00",
                    "lastAccessedAtForHumans":"1 day ago",
                    "joinedAt":"2019-06-12T08:22:58+00:00",
                    "joinedAtForHumans":"1 week ago"
                }
            },
            "teachers":{
                "data":[
                    {
                        "id":68,
                        "role":"teacher",
                        "lastAccessedAt":null,
                        "lastAccessedAtForHumans":null,
                        "joinedAt":"2019-06-12T08:22:58+00:00",
                        "joinedAtForHumans":"1 week ago",
                        "user":{
                            "data":{
                                "name":"Lavinia Wintheiser",
                                "username":"oleta.hackett",
                                "initials":"LW",
                                "avatar":"http:\/\/lms.local\/lms.local\/storage\/653\/avatar-05.png",
                                "email":null,
                                "detail":null
                            }
                        }
                    }
                ]
            },
            "categories":{
                "data":[
                    {
                        "slug":"obstetri-dan-ginekologi",
                        "name":"Obstetri dan Ginekologi",
                        "id":3,
                        "parent":{
                            "data":{
                                "id":1,
                                "slug":"fakultas-kedokteran",
                                "name":"Fakultas Kedokteran",
                                "parent_id":null,
                                "created_at":"2019-06-12 08:23:57",
                                "updated_at":"2019-06-12 08:23:57",
                                "deleted_at":null
                            }
                        }
                    }
                ]
            }
        },
        {
            "slug":"kdk206-patologi-anatomi-2019-genap",
            "code":"KDK206",
            "subject_id":11,
            "title":"Patologi Anatomi",
            "teaching_period_id":1,
            "description":"Officia sit ut blanditiis rerum iste accusamus. Tempora possimus nihil cupiditate non sed neque nam. Quaerat non aut provident voluptas.",
            "studentsCount":34,
            "major":23,
            "faculty":1,
            "createdAt":"2019-01-27T01:43:57+00:00",
            "createdAtForHumans":"4 months ago",
            "start_at":null,
            "end_at":null,
            "self":{
                "data":{
                    "id":348,
                    "role":"student",
                    "lastAccessedAt":"2019-06-18T02:56:59+00:00",
                    "lastAccessedAtForHumans":"1 week ago",
                    "joinedAt":"2019-06-12T08:23:02+00:00",
                    "joinedAtForHumans":"1 week ago"
                }
            },
            "teachers":{
                "data":[
                    {
                        "id":347,
                        "role":"teacher",
                        "lastAccessedAt":null,
                        "lastAccessedAtForHumans":null,
                        "joinedAt":"2019-06-12T08:23:02+00:00",
                        "joinedAtForHumans":"1 week ago",
                        "user":{
                            "data":{
                                "name":"Prof. Dino Schaefer",
                                "username":"nschuster",
                                "initials":"PD",
                                "avatar":"",
                                "email":"wilbert.reynolds@example.com",
                                "detail":null
                            }
                        }
                    }
                ]
            },
            "categories":{
                "data":[
                    {
                        "slug":"patologi-klinik",
                        "name":"Patologi Klinik",
                        "id":23,
                        "parent":{
                            "data":{
                                "id":1,
                                "slug":"fakultas-kedokteran",
                                "name":"Fakultas Kedokteran",
                                "parent_id":null,
                                "created_at":"2019-06-12 08:23:57",
                                "updated_at":"2019-06-12 08:23:57",
                                "deleted_at":null
                            }
                        }
                    }
                ]
            }
        },
        {
            "slug":"phk101-filsafat-ilmu-2019-genap",
            "code":"PHK101",
            "subject_id":14,
            "title":"Filsafat Ilmu",
            "teaching_period_id":1,
            "description":"Laboriosam est nulla est natus ab cum. Laboriosam velit eum et doloremque modi doloremque. Voluptas perferendis numquam velit.",
            "studentsCount":31,
            "major":4,
            "faculty":1,
            "createdAt":"2019-05-04T07:31:11+00:00",
            "createdAtForHumans":"1 month ago",
            "start_at":null,
            "end_at":null,
            "self":{
                "data":{
                    "id":450,
                    "role":"student",
                    "lastAccessedAt":"2019-05-26T18:43:11+00:00",
                    "lastAccessedAtForHumans":"4 weeks ago",
                    "joinedAt":"2019-06-12T08:23:03+00:00",
                    "joinedAtForHumans":"1 week ago"
                }
            },
            "teachers":{
                "data":[
                    {
                        "id":449,
                        "role":"teacher",
                        "lastAccessedAt":null,
                        "lastAccessedAtForHumans":null,
                        "joinedAt":"2019-06-12T08:23:03+00:00",
                        "joinedAtForHumans":"1 week ago",
                        "user":{
                            "data":{
                                "name":"Roselyn Gerhold",
                                "username":"kian03",
                                "initials":"RG",
                                "avatar":"http:\/\/lms.local\/lms.local\/storage\/795\/avatar-08.png",
                                "email":"ellie.ziemann@example.com",
                                "detail":null
                            }
                        }
                    }
                ]
            },
            "categories":{
                "data":[
                    {
                        "slug":"kulit-dan-kelamin",
                        "name":"Kulit dan Kelamin",
                        "id":4,
                        "parent":{
                            "data":{
                                "id":1,
                                "slug":"fakultas-kedokteran",
                                "name":"Fakultas Kedokteran",
                                "parent_id":null,
                                "created_at":"2019-06-12 08:23:57",
                                "updated_at":"2019-06-12 08:23:57",
                                "deleted_at":null
                            }
                        }
                    }
                ]
            }
        },
        {
            "slug":"gls001-general-emergency-life-support-gels-1-2019-genap",
            "code":"GLS001",
            "subject_id":23,
            "title":"General Emergency Life Support (GELS) 1",
            "teaching_period_id":1,
            "description":"Ut distinctio amet blanditiis. Ullam rem distinctio amet ut exercitationem. Illo quia atque quam nesciunt.",
            "studentsCount":37,
            "major":19,
            "faculty":1,
            "createdAt":"2019-03-19T00:14:24+00:00",
            "createdAtForHumans":"3 months ago",
            "start_at":null,
            "end_at":null,
            "self":{
                "data":{
                    "id":789,
                    "role":"student",
                    "lastAccessedAt":"2019-06-06T20:34:18+00:00",
                    "lastAccessedAtForHumans":"2 weeks ago",
                    "joinedAt":"2019-06-12T08:23:07+00:00",
                    "joinedAtForHumans":"1 week ago"
                }
            },
            "teachers":{
                "data":[
                    {
                        "id":788,
                        "role":"teacher",
                        "lastAccessedAt":null,
                        "lastAccessedAtForHumans":null,
                        "joinedAt":"2019-06-12T08:23:07+00:00",
                        "joinedAtForHumans":"1 week ago",
                        "user":{
                            "data":{
                                "name":"Roselyn Gerhold",
                                "username":"kian03",
                                "initials":"RG",
                                "avatar":"http:\/\/lms.local\/lms.local\/storage\/795\/avatar-08.png",
                                "email":"ellie.ziemann@example.com",
                                "detail":null
                            }
                        }
                    }
                ]
            },
            "categories":{
                "data":[
                    {
                        "slug":"kardiologi-dan-vaskuler",
                        "name":"Kardiologi dan Vaskuler",
                        "id":19,
                        "parent":{
                            "data":{
                                "id":1,
                                "slug":"fakultas-kedokteran",
                                "name":"Fakultas Kedokteran",
                                "parent_id":null,
                                "created_at":"2019-06-12 08:23:57",
                                "updated_at":"2019-06-12 08:23:57",
                                "deleted_at":null
                            }
                        }
                    }
                ]
            }
        }
    ]
};

export const dataSemester = {
    'data': [
        {
            'label': 'Lihat Semua',
            'value': 'all'
        },
        {
            'label': 'Semester 1',
            'value': '1'
        },
        {
            'label': 'Semester 2',
            'value': '2'
        }
    ]
};

export const dataBagian = {
    'data': [
        {
            'label': 'Lihat Semua',
            'value': 'all'
        },
        {
            'label': 'Bahan Ajar',
            'value': 'resource'
        },
        {
            'label': 'Tugas',
            'value': 'assignment'
        },
        {
            'label': 'Quiz',
            'value': 'quiz'
        },
    ]
};

export const dataFile = {
    'data': [
        {
            'nama': 'File Satu',
            'type': 'pdf',
            'ukuran' : '32',
            'key': 'satu'
        },
        {
            'nama': 'File Dua',
            'type': 'presentation',
            'ukuran' : '32',
            'key': 'dua'
        },
        {
            'nama': 'File Tiga',
            'type': 'pdf',
            'ukuran' : '32',
            'key': 'tiga'
        },
        {
            'nama': 'File Empat',
            'type': 'pdf',
            'ukuran' : '32',
            'key': 'empat',
        },
        {
            'nama': 'File Satu',
            'type': 'presentation',
            'ukuran' : '32',
            'key': 'satu1'
        },
        {
            'nama': 'File Dua',
            'type': 'pdf',
            'ukuran' : '32',
            'key': 'dua1'
        },
        {
            'nama': 'File Tiga',
            'type': 'presentation',
            'ukuran' : '32',
            'key': 'tiga1'
        },
        {
            'nama': 'File Empat',
            'type': 'pdf',
            'ukuran' : '32',
            'key': 'empat1',
        },
        {
            'nama': 'File Dia',
            'type': 'word',
            'ukuran' : '32',
            'key': 'empat4',
        }
    ]
};

export const dataDiskusi = {
    "data": [
        {
            "id": 1148,
            "isOwn": true,
            "message": "<p><strong>dia </strong><strong><em>mereka</em></strong></p>",
            "createdAt": "2019-06-28T06:21:13+00:00",
            "createdAtForHumans": "3 days ago",
            "updatedAt": "2019-06-28T06:21:13+00:00",
            "updatedAtForHumans": "3 days ago",
            "user": {
                "data": {
                    "name": "Carter Stokes",
                    "username": "judd.toy",
                    "initials": "CS",
                    "avatar": "https://cdn.pixabay.com/photo/2017/02/23/13/05/profile-2092113_960_720.png",
                    "email": "mueller.claudia@example.org",
                    "detail": null
                }
            },
            "replies": {
                "data": [
                    {
                        "id": 1190,
                        "isOwn": true,
                        "message": "<p>Coba</p>",
                        "createdAt": "2019-06-19T05:22:54+00:00",
                        "createdAtForHumans": "1 week ago",
                        "updatedAt": "2019-06-19T05:22:54+00:00",
                        "updatedAtForHumans": "1 week ago",
                        "user": {
                            "data": {
                                "name": "Carter Stokes",
                                "username": "judd.toy",
                                "initials": "CS",
                                "avatar": "https://cdn.pixabay.com/photo/2017/02/23/13/05/profile-2092113_960_720.png",
                                "email": "mueller.claudia@example.org",
                                "detail": null
                            }
                        }
                    },
                ]
            }
        },
        {
            "id": 1147,
            "isOwn": false,
            "message": "<p>asfasfasasfas</p>",
            "createdAt": "2019-06-28T06:14:49+00:00",
            "createdAtForHumans": "3 days ago",
            "updatedAt": "2019-06-28T06:14:49+00:00",
            "updatedAtForHumans": "3 days ago",
            "user": {
                "data": {
                    "name": "Carter Stokes",
                    "username": "judd.toy",
                    "initials": "CS",
                    "avatar": "https://cdn.pixabay.com/photo/2017/02/23/13/05/profile-2092113_960_720.png",
                    "email": "mueller.claudia@example.org",
                    "detail": null
                }
            },
            "replies": {
                "data": []
            }
        },
        {
            "id": 954,
            "isOwn": false,
            "message": "<p>Ut harum quia beatae rerum qui. Nam repellendus facilis sint qui magnam incidunt consectetur.</p>",
            "createdAt": "2019-05-02T15:01:33+00:00",
            "createdAtForHumans": "1 month ago",
            "updatedAt": "2019-05-25T10:20:14+00:00",
            "updatedAtForHumans": "1 month ago",
            "user": {
                "data": {
                    "name": "Mr. Jacques Wolff PhD",
                    "username": "bayer.ricky",
                    "initials": "MJ",
                    "avatar": "https://cdn.pixabay.com/photo/2017/02/23/13/05/profile-2092113_960_720.png",
                    "email": "meredith41@example.org",
                    "detail": null
                }
            },
            "replies": {
                "data": []
            }
        },
        {
            "id": 363,
            "isOwn": false,
            "message": "<p>Sapiente optio enim officia et quos nisi. Vel veritatis dolorum quia.</p>",
            "createdAt": "2019-03-18T15:25:45+00:00",
            "createdAtForHumans": "3 months ago",
            "updatedAt": "2019-03-18T15:25:45+00:00",
            "updatedAtForHumans": "3 months ago",
            "user": {
                "data": {
                    "name": "Delilah McCullough",
                    "username": "wanda78",
                    "initials": "DM",
                    "avatar": "",
                    "email": "earlene.armstrong@example.net",
                    "detail": null
                }
            },
            "replies": {
                "data": [
                    {
                        "id": 1146,
                        "isOwn": true,
                        "message": "<p>Coba</p>",
                        "createdAt": "2019-06-19T05:22:54+00:00",
                        "createdAtForHumans": "1 week ago",
                        "updatedAt": "2019-06-19T05:22:54+00:00",
                        "updatedAtForHumans": "1 week ago",
                        "user": {
                            "data": {
                                "name": "Carter Stokes",
                                "username": "judd.toy",
                                "initials": "CS",
                                "avatar": "https://cdn.pixabay.com/photo/2017/02/23/13/05/profile-2092113_960_720.png",
                                "email": "mueller.claudia@example.org",
                                "detail": null
                            }
                        }
                    },
                    {
                        "id": 1149,
                        "isOwn": false,
                        "message": "<p>baru ini</p>",
                        "createdAt": "2019-07-02T01:46:39+00:00",
                        "createdAtForHumans": "1 minute ago",
                        "updatedAt": "2019-07-02T01:46:39+00:00",
                        "updatedAtForHumans": "1 minute ago",
                        "user": {
                            "data": {
                                "name": "Carter Stokes",
                                "username": "judd.toy",
                                "initials": "CS",
                                "avatar": "",
                                "email": "mueller.claudia@example.org",
                                "detail": null
                            }
                        }
                    }
                ]
            }
        }
    ]
};

export const dataQuiz = {
    "data": [
        {
            "id": "qo0fNH93",
            "type": "Boolean",
            "typeLabel": "Boolean",
            "scoringMethod": "Default",
            "content": "Voluptatem sit et dolores architecto sapiente voluptatem a. Dolores quia omnis amet enim ut occaecati. Nostrum autem pariatur corrupti neque. Error aspernatur nihil numquam sint.",
            "choiceItems": {
                "data": [
                    {
                        "id": "tnfDnrNZ",
                        "choiceText": "False",
                        "isCorrect": true
                    },
                    {
                        "id": "swMm2LSU",
                        "choiceText": "True",
                        "isCorrect": false
                    }
                ]
            }
        },
        {
            "id": "KhV1UVM5",
            "type": "MultipleChoice",
            "typeLabel": "Multiple Choice",
            "scoringMethod": "Default",
            "content": "Odit voluptate voluptatem sequi delectus. Sequi doloribus laudantium nam illum sed. Eveniet dicta et sed. Aut recusandae ut nobis dolor molestias nam sunt.",
            "choiceItems": {
                "data": [
                    {
                        "id": "DlyuZAdV",
                        "choiceText": "Dolorem quasi commodi autem illum consequuntur ad sapiente animi.",
                        "isCorrect": false
                    },
                    {
                        "id": "YIkrqXWk",
                        "choiceText": "Aliquam accusantium saepe qui sed repellendus deserunt.",
                        "isCorrect": true
                    },
                    {
                        "id": "iBhsVZXr",
                        "choiceText": "Et et excepturi consequatur voluptate et et.",
                        "isCorrect": false
                    },
                    {
                        "id": "FfUTyjOZ",
                        "choiceText": "Repellendus dolore molestias nobis perspiciatis quaerat.",
                        "isCorrect": false
                    }
                ]
            }
        },
        {
            "id": "ExWdSNGA",
            "type": "MultipleChoice",
            "typeLabel": "Multiple Choice",
            "scoringMethod": "Default",
            "content": "Ipsa aut in aliquam nam harum. Delectus sint dolores sunt in libero explicabo. Ratione iure quisquam error officia praesentium aliquam.",
            "choiceItems": {
                "data": [
                    {
                        "id": "WvkxMav4",
                        "choiceText": "Accusantium voluptatem quia voluptatum.",
                        "isCorrect": false
                    },
                    {
                        "id": "shZfEf2w",
                        "choiceText": "Atque et expedita culpa ratione ipsum eum laborum id.",
                        "isCorrect": false
                    },
                    {
                        "id": "tCoMCYlZ",
                        "choiceText": "Voluptates aperiam ullam quis.",
                        "isCorrect": false
                    },
                    {
                        "id": "EtIMifPU",
                        "choiceText": "Aut aut doloremque sapiente et vel.",
                        "isCorrect": false
                    },
                    {
                        "id": "kKACDB0F",
                        "choiceText": "Explicabo ut in et dignissimos veniam nihil libero corporis.",
                        "isCorrect": true
                    }
                ]
            }
        },
        {
            "id": "YsRKrOf6",
            "type": "Boolean",
            "typeLabel": "Boolean",
            "scoringMethod": "Default",
            "content": "Deserunt hic asperiores et ab. Itaque eius suscipit quod magni et sint. Porro animi qui soluta a vitae quas eligendi. Blanditiis consequatur vero esse id.",
            "choiceItems": {
                "data": [
                    {
                        "id": "k1rZGYXA",
                        "choiceText": "True",
                        "isCorrect": false
                    },
                    {
                        "id": "rySa2ZpZ",
                        "choiceText": "False",
                        "isCorrect": true
                    }
                ]
            }
        },
        {
            "id": "lencx9P2",
            "type": "MultipleRespon",
            "typeLabel": "Multiple Respon",
            "scoringMethod": "Default",
            "content": "In dolorum laborum iste suscipit repellat tenetur. Rerum quibusdam architecto quas quaerat dicta quidem repudiandae. Mollitia optio ab nobis est ipsam corporis accusantium. Labore laboriosam voluptatum tempore recusandae fugiat id aperiam. Suscipit natus mollitia vitae odio deleniti.",
            "choiceItems": {
                "data": [
                    {
                        "id": "PBDNO9AC",
                        "choiceText": "Rerum nisi et similique illo dignissimos temporibus soluta ex.",
                        "isCorrect": true
                    },
                    {
                        "id": "aZjyi9b2",
                        "choiceText": "Eaque voluptatem distinctio consequuntur sequi quam aspernatur suscipit.",
                        "isCorrect": false
                    },
                    {
                        "id": "pqh0bWju",
                        "choiceText": "Saepe libero aut dignissimos tempore beatae.",
                        "isCorrect": false
                    },
                    {
                        "id": "ZVfGKZxI",
                        "choiceText": "Dignissimos veniam beatae reiciendis hic rem iusto.",
                        "isCorrect": false
                    },
                    {
                        "id": "QSpj1bB7",
                        "choiceText": "Animi totam non aliquid sed explicabo.",
                        "isCorrect": false
                    }
                ]
            }
        },
        {
            "id": "vWyyBUzY",
            "type": "Boolean",
            "typeLabel": "Boolean",
            "scoringMethod": "Default",
            "content": "Ut illo sed rerum in qui vel voluptatum. Perferendis vero velit voluptas qui. Aliquid corporis ipsum odio a.",
            "choiceItems": {
                "data": [
                    {
                        "id": "44hfn2aO",
                        "choiceText": "True",
                        "isCorrect": true
                    },
                    {
                        "id": "11zk2QEG",
                        "choiceText": "False",
                        "isCorrect": false
                    }
                ]
            }
        },
        {
            "id": "bYZoUo34",
            "type": "MultipleChoice",
            "typeLabel": "Multiple Choice",
            "scoringMethod": "Default",
            "content": "Tenetur rerum eligendi repellat incidunt. Occaecati quis ut qui impedit eaque enim eius voluptas. Quia consectetur perferendis error et ut voluptates. Eveniet explicabo doloremque et natus dolor.",
            "choiceItems": {
                "data": [
                    {
                        "id": "QKaaWwOy",
                        "choiceText": "Molestiae dolor reprehenderit et qui.",
                        "isCorrect": false
                    },
                    {
                        "id": "N21YtbGj",
                        "choiceText": "Dicta et velit porro voluptatem occaecati quisquam corporis velit.",
                        "isCorrect": false
                    },
                    {
                        "id": "anKenKhN",
                        "choiceText": "Ut velit tenetur possimus doloribus cum nemo quo.",
                        "isCorrect": false
                    },
                    {
                        "id": "d87s1YVr",
                        "choiceText": "Odit est ut fugit non delectus.",
                        "isCorrect": false
                    },
                    {
                        "id": "wCy1244Q",
                        "choiceText": "Non blanditiis et eum deleniti harum corrupti doloribus exercitationem.",
                        "isCorrect": true
                    }
                ]
            }
        },
        {
            "id": "qhzoK7DP",
            "type": "Boolean",
            "typeLabel": "Boolean",
            "scoringMethod": "Default",
            "content": "Vel et debitis voluptatum et voluptates suscipit magni in. Voluptatem voluptas qui pariatur dolorem. Voluptatibus et possimus eos maiores tenetur praesentium. Velit voluptatem commodi in quasi.",
            "choiceItems": {
                "data": [
                    {
                        "id": "87IY19OL",
                        "choiceText": "False",
                        "isCorrect": true
                    },
                    {
                        "id": "XXAfIV9f",
                        "choiceText": "True",
                        "isCorrect": false
                    }
                ]
            }
        },
        {
            "id": "Hn3R8n27",
            "type": "MultipleChoice",
            "typeLabel": "Multiple Choice",
            "scoringMethod": "Default",
            "content": "Et doloremque dolore quia cupiditate impedit veritatis excepturi. Cumque nemo similique velit voluptas temporibus inventore. Occaecati ab dolor voluptas non doloremque.",
            "choiceItems": {
                "data": [
                    {
                        "id": "849hEupu",
                        "choiceText": "Et molestiae impedit soluta perferendis quisquam eum.",
                        "isCorrect": true
                    },
                    {
                        "id": "eOpXQACX",
                        "choiceText": "Earum corporis veniam quia fuga nihil voluptas eligendi.",
                        "isCorrect": false
                    },
                    {
                        "id": "UBFjIDSH",
                        "choiceText": "Laborum maiores eum non nemo dolor eius a.",
                        "isCorrect": false
                    },
                    {
                        "id": "pqQj2xtP",
                        "choiceText": "Dicta sed voluptatem laboriosam animi.",
                        "isCorrect": false
                    }
                ]
            }
        },
        {
            "id": "k0RMaU5p",
            "type": "FillIn",
            "typeLabel": "Fill In",
            "scoringMethod": "Default",
            "content": "Nostrum a eaque nam doloribus. Quaerat voluptatibus iure sed quos enim. Voluptatum quisquam beatae expedita dolores et voluptates harum.",
            "choiceItems": {
                "data": [
                    {
                        "id": "Xcdt7Fqd",
                        "choiceText": "",
                        "isCorrect": false
                    },
                ]
            }
        },
        {
            "id": "9m09KlBV",
            "type": "Essay",
            "typeLabel": "Essay",
            "scoringMethod": "Default",
            "content": "Facere distinctio eum sequi quia saepe et. Dolorem vero nihil ab amet consequatur. Eius quia eos quia tenetur voluptas velit.",
            "choiceItems": {
                "data": [
                    {
                        "id": "6uqsbk5k",
                        "choiceText": "",
                        "isCorrect": false
                    }
                ]
            }
        },
        {
            "id": "DFGpGvlu",
            "type": "Boolean",
            "typeLabel": "Boolean",
            "scoringMethod": "Default",
            "content": "Exercitationem velit voluptatem deserunt harum. Enim fugiat cum ipsum aut numquam voluptatem suscipit. Et voluptatem repudiandae et. Quam qui voluptatibus omnis recusandae eius aut itaque minima.",
            "choiceItems": {
                "data": [
                    {
                        "id": "iAcOoQWt",
                        "choiceText": "False",
                        "isCorrect": false
                    },
                    {
                        "id": "ZWaS60Vg",
                        "choiceText": "True",
                        "isCorrect": true
                    }
                ]
            }
        },
        {
            "id": "FiPIBxCB",
            "type": "MultipleChoice",
            "typeLabel": "Multiple Choice",
            "scoringMethod": "Default",
            "content": "Dolorum magnam distinctio voluptatem repellendus perspiciatis totam quasi ut. Ipsam quidem quibusdam libero itaque veritatis ipsa. Assumenda vero enim quisquam.",
            "choiceItems": {
                "data": [
                    {
                        "id": "uaSJGBe8",
                        "choiceText": "Aspernatur molestiae perferendis nemo eum consequatur nesciunt perspiciatis.",
                        "isCorrect": false
                    },
                    {
                        "id": "RgmjMYYz",
                        "choiceText": "Necessitatibus voluptas et voluptatem odit culpa quos.",
                        "isCorrect": true
                    },
                    {
                        "id": "gi2QJA9J",
                        "choiceText": "Magni rerum ipsum voluptatum sed.",
                        "isCorrect": false
                    },
                    {
                        "id": "Q9yG240t",
                        "choiceText": "Eaque molestias sequi praesentium deserunt maxime.",
                        "isCorrect": false
                    }
                ]
            }
        },
        {
            "id": "KL83z0rI",
            "type": "Boolean",
            "typeLabel": "Boolean",
            "scoringMethod": "Default",
            "content": "Dolorem vel molestiae cum sequi. Ex labore provident nihil laboriosam ut quia accusamus veritatis. Officia voluptatum recusandae deserunt voluptate ut qui reprehenderit. Magni voluptatum eaque et quos dolores.",
            "choiceItems": {
                "data": [
                    {
                        "id": "SLn4DLBz",
                        "choiceText": "False",
                        "isCorrect": false
                    },
                    {
                        "id": "iBCb4Kv2",
                        "choiceText": "True",
                        "isCorrect": true
                    }
                ]
            }
        }
    ]
}