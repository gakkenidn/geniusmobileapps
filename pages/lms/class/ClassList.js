import React, { Component, Fragment } from 'react';
import { FlatList, Text, View } from 'react-native';
import SwitchSelector from 'react-native-switch-selector';
import {dataKelas, dataSemester} from '../dummy/dataku';

import Spinner from '../../../components/Spinner';
import ListItem from '../../../components/classes/ListItemClass';
import BaseStyles from '../../../styles/base';
import Error from '../../../components/Error';
import Header from "../../../components/HeaderLMS";

class ClassList extends Component{

    state = {
    };
    
    componentDidMount() {

    }

    render() {
        return (
            <View style={BaseStyles.container}>
                <Header title="Gakken LMS" logo={true}/>
                <View style={{paddingTop: 10, paddingBottom: 10, marginRight:4, marginLeft:4}}>
                    <SwitchSelector buttonColor={'#4f92ca'} options={dataSemester.data} initial={0} onPress={value => console.log(`Call onPress with value: ${value}`)} />
                </View>
                <FlatList data={dataKelas.data}
                          renderItem={ ({item}) => <ListItem item={item} withFeatured={true}/>}
                          keyExtractor={item => item.slug}
                />
            </View>
        )
    }

}

export default ClassList;
