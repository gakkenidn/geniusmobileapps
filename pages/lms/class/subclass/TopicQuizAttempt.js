import React from 'react';
import {View, Text, StyleSheet, PixelRatio, TouchableOpacity, Button, Alert, BackHandler} from 'react-native';
import {Redirect} from 'react-router-native';

import {dataQuiz} from "../../dummy/dataku";
import * as StyleConstants from "../../../../styles/constants";
import Icon from "react-native-vector-icons/FontAwesome";
import ContentQuiz from '../../../../components/classes/quiz/ContentQuiz';
import {View as AnimatableView} from "react-native-animatable";
import BaseStyles from "../../../../styles/base";
import Header from "../../../../components/HeaderLMS";
import Spinner from "../../../../components/Spinner";
import Error from "../../../../components/Error";

class TopicQuizAttempt extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            quePos: 1,                  //urutan pertanyaan
            currAns: '',                //jawaban sementara sebelum submit
            answer: [],                  //array jawaban keseluruhan setelah submit
            exit: false,
            fetching: false,
            error: false
        };
        // this.initAnswer();
    }

    initAnswer = () => {
        // this.setState({answer: []});
    };

    //Next Button
    _next = () => {
        const pos = this.state.quePos;
        this.setState({quePos: pos + 1, currAns: ''})
    };

    //Previous Button
    _prev = () => {
        const pos = this.state.quePos;
        this.setState({quePos: pos - 1});
        this.state.answer[pos - 2] === ''
            ? this.setState({currAns: ''})
            : this.setState({currAns: this.state.answer[pos-2]});
    };

    exit = this._exit.bind(this);
    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.exit);
        this.loadData();
    };

    loadData = () => {
        // this.setState({fetching: true, error: false});
    };

    _exit() {
        this.refs.baseView.fadeOutRight(300);
        this.cancelToken && this.cancelToken();
        BackHandler.removeEventListener('hardwareBackPress', this.exit);
        setTimeout(() => {
            this.setState({ exit: true });
        }, 300);
        return true;
    }

    pilihJawaban = () => {
        let ans = this.state.answer.slice();
        // let key = dataQuiz.data[this.state.quePos-1].id;
        ans[this.state.quePos-1] = this.state.currAns;
        // ans[key] = this.state.currAns;
        this.setState({answer: [ans]});
        this._next()
    };

    selesai = () => {
        Alert.alert(
            'Peringatan!',
            'Anda akan mengakhiri ujian?',
            [
                {text: 'Tidak', onPress: () => console.warn('Tidak'), style: 'cancel'},
                {text: 'Ya', onPress: () => console.warn('Ya')}
            ]
        );
    };

    render(){
        console.log("Jawaban: ", this.state.answer);
        if (this.state.exit){
            return <Redirect to={{pathname: '/class/' + this.props.route.match.params.slug + '/detail/' + this.props.route.match.params.subslug, state: {...this.props.route.location.state}}}/>
        }
        return(
            <AnimatableView style={ BaseStyles.overlayContainerLMS }
                            animation="fadeInRight"
                            easing="ease-in-out-sine"
                            duration={ 300 }
                            ref="baseView">
                <Header title={"Quiz"} back={ () => this._exit()}/>
                {this.state.fetching && <Spinner/>}
                {this.state.error && <Error/>}
                <View style={{flex: 0.1, flexDirection: 'row', alignItems: 'center', justifyContent: "space-between", paddingLeft: 10, paddingRight: 10}}>
                    <Text style={{textAlign: 'right', flex:1, paddingRight: 10}}>{this.state.quePos} / {dataQuiz.data.length}</Text>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity onPress={this._prev} disabled={this.state.quePos > 1 ? false : true}>
                            <Icon name="angle-left" size={25} color={this.state.quePos > 1 ? StyleConstants.COLOR_GRAY.LIGHT : StyleConstants.COLOR_GRAY.LIGHTEST} style={{...styleku.nextPrev, borderBottomLeftRadius: 5, borderTopLeftRadius: 5}}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this._next} disabled={this.state.quePos < dataQuiz.data.length ? false : true}>
                            <Icon name="angle-right" size={25} color={this.state.quePos < dataQuiz.data.length ? StyleConstants.COLOR_GRAY.LIGHT : StyleConstants.COLOR_GRAY.LIGHTEST} style={{...styleku.nextPrev, borderBottomRightRadius: 5, borderTopRightRadius: 5}}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <ContentQuiz
                    data={dataQuiz.data[this.state.quePos-1]}
                    answer={this.state.answer[this.state.quePos-1]}
                    currAns={this.state.currAns}
                    onChange={jawaban => this.setState({currAns: jawaban})}/>
                <View style={{flex: 0.1, flexDirection: 'row', paddingLeft: 10, alignItems: 'center', paddingRight: 10, justifyContent: 'flex-end'}}>
                    <Button title={"Submit Jawaban"} onPress={this.pilihJawaban} color={'green'}/>
                    <View style={{margin:5}}/>
                    <Button title={"Selesai"} onPress={this.selesai}/>
                </View>
            </AnimatableView>
        )
    }

}

const styleku = StyleSheet.create({
   nextPrev: {
       paddingTop: 5,
       paddingBottom: 5,
       paddingLeft: 10,
       paddingRight: 10,
       borderColor: '#9B9B9B',
       borderWidth: 1 / PixelRatio.get(),
   }
});

export default TopicQuizAttempt;