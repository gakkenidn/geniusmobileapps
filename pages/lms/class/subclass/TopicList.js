import React, {Component, Fragment} from 'react';
import {BackHandler, FlatList, Text, View, ScrollView} from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import BaseStyles from "../../../../styles/base";
import * as Constans from "../../../../styles/constants";
import Header from "../../../../components/HeaderLMS";
import SwitchSelector from "react-native-switch-selector";
import ListItem from "../../../../components/classes/ListItemTopic";
import {dataBagian, dataTopik} from '../../dummy/dataku';

class TopicList extends Component{

    state = {
        exit: false,
        fetching: false,
        error: false,
        loaded: false,
        topic: {},
        content: [],
        data: dataTopik.data,
        slug: this.props.route.match.params.slug,
        type: ''
    };


    componentDidMount(){
    }

    filterFunction = type => {
        this.setState({
            type: type
        });

        const newData = dataTopik.data.filter(item => {
            const itemData = `${item.type}`

            if (type === 'all'){
                return dataTopik.data;
            }
            else{
                return itemData.indexOf(type) > -1;
            }
        });

        this.setState({data: newData});
    }

    render(){
        return (
            <View style={[BaseStyles.container, {backgroundColor: Constans.COLOR_GRAY.LIGHTEST}]}>
                <View style={{paddingTop: 10, paddingBottom: 10, marginBottom: 10, backgroundColor: Constans.COLOR.WHITE}}>
                    <SwitchSelector buttonColor={'#4f92ca'} hasPadding={true} options={dataBagian.data} initial={0} onPress={value => this.filterFunction(value)} />
                </View>
                <FlatList data={this.state.data}
                          renderItem={ ({item}) => <ListItem item={item} withFeatured={true} slug={this.state.slug}/>}
                          keyExtractor={item => item.id.toString()}
                />
            </View>
        );
    }
}

export default TopicList;