import React, {Component, Fragment} from 'react';
// import ClassRoomTabs from "./ClassRoomTabs";
import {BackHandler, Text, View} from 'react-native';
import Query from "query-string";
import {View as AnimatableView} from "react-native-animatable";
import BaseStyles from "../../../../styles/base";
import Touchable from "react-native-platform-touchable";
import TopicList from "./TopicList";
import Discuss from "./Discuss";
import File from "./File";
import Header from "../../../../components/HeaderLMS";
import {Redirect} from 'react-router-native';

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activePage: Query.parse(this.props.route.location.search).section || 'topic',
            origin: false,
            exit: false
        };
    }

    exit = this._exit.bind(this);

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.exit);
    }

    _exit() {
        this.refs.baseView.fadeOutRight(300);
        this.cancelToken && this.cancelToken();
        BackHandler.removeEventListener('hardwareBackPress', this.exit);
        setTimeout(() => {
            this.setState({ exit: true });
        }, 300);
        return true;
    }

    render(){
        if (this.state.exit){
            return <Redirect to={'/class/' + this.props.route.match.params.slug + '/detail'}/>
        }

        return(
            <AnimatableView style={ BaseStyles.overlayContainerLMS }
                            animation="fadeInRight"
                            easing="ease-in-out-sine"
                            duration={ 300 }
                            ref="baseView">
                <Header title="Ruang Kelas" back={ () => this._exit()}/>
                <View style={{flex: 1, width: '100%'}}>
                    <View style={BaseStyles.topNavbar}>
                        <Touchable onPress={() => this.setState({activePage: 'topic'})}
                                   style={this.state.activePage === 'topic' ? BaseStyles.topNavbarItemActive : BaseStyles.topNavbarItem}>
                            <Text style={BaseStyles.topNavbarItemText}>Kelas</Text>
                        </Touchable>
                        <Touchable onPress={() => this.setState({activePage: 'discuss'})}
                                   style={this.state.activePage === 'discuss' ? BaseStyles.topNavbarItemActive : BaseStyles.topNavbarItem}>
                            <Text style={BaseStyles.topNavbarItemText}>Diskusi</Text>
                        </Touchable>
                        <Touchable onPress={() => this.setState({activePage: 'file'})}
                                   style={this.state.activePage ==='file' ? BaseStyles.topNavbarItemActive : BaseStyles.topNavbarItem}>
                            <Text style={BaseStyles.topNavbarItemText}>File</Text>
                        </Touchable>
                    </View>
                    {this.state.activePage === 'topic' && <TopicList {...this.props}/>}
                    {this.state.activePage === 'discuss'&& <Discuss {...this.props}/>}
                    {this.state.activePage === 'file'&& <File {...this.props}/>}
                </View>
            </AnimatableView>
        );
    }

};

export default Home;