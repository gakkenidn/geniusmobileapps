import React from 'react';
import {View, Text} from 'react-native';
import ResPDF from "../../../../components/classes/resource/ResPDF";
import ResVideoJWT from "../../../../components/classes/resource/ResVideoJWT";
import ResAudio from "../../../../components/classes/resource/ResAudio";
import ResURL from "../../../../components/classes/resource/ResURL";
import ResVideoYoutube from "../../../../components/classes/resource/ResVideoYoutube";

class TopicResource extends React.Component {

    constructor(props){
        super(props);
        this.state = {
        };
    }

    componentDidMount(){
    }

    render(){

        let content = null;
        switch (this.props.teachableItem.data.type) {
            case 'text' : content = <ResPDF/>; break;
            case 'youtubevideo': content = <ResVideoYoutube/>; break;
            case 'video': content = <ResVideoJWT {...this.props} url={this.props.teachableItem.data.data.url}/>; break;
            case 'audio': content = <ResAudio/>; break;
            case 'url': content = <ResURL/>; break;
        }

        return(
            <View style={{flex: 1}}>
                {content}
            </View>
        )
    }

}

export default TopicResource;