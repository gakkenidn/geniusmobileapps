import React from 'react';
import {View, Text, TouchableOpacity, Button, StyleSheet, PixelRatio} from 'react-native';
import Axios from "axios";
import {View as AnimatableView} from "react-native-animatable";
import DocumentPicker from 'react-native-document-picker';
import DropdownAlert from 'react-native-dropdownalert';

import BaseStyles from "../../../../styles/base";
import TopicBox from '../../../../components/TopicBox';

class TopicAssignment extends React.Component{

    state={
        size : '',
        fileUpload: {
            name: ''
        }
    };

    componentDidMount(){
    }

    static bytesToSize(bytes) {
        let sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    };

    selectFile = () =>{
        const res = DocumentPicker.pick({
            type: [DocumentPicker.types.allFiles]
        })
            .then(res => {
                console.log(res)
                this.setState({
                    fileUpload: res
                });
            })
            .catch(err => console.log(err));
    };

    uploadFile(state){
        this.dropDownAlertRef.alertWithType('warn', 'Process', 'Uploading');
        const data = new FormData();
        data.append("fileToUpload", {
            uri: state.uri,
            type: state.type,
            name: state.name
        });
        console.log(data);

        const config = {
            headers: {
                Accept: "application/json",
                'Content-type': "multipart/form-data",
            }
        };

        Axios.post("http://192.168.1.16:80/cobaupload/upload2.php", data, config)
            .then(resp => {
                console.log("RES", resp.data);
                this.dropDownAlertRef.closeAction();
                this.dropDownAlertRef.alertWithType('success', 'Sukses', 'File berhasil di unggah.');
            })
            .catch(err => {
                console.log("ERR", err);
                this.dropDownAlertRef.closeAction();
                this.dropDownAlertRef.alertWithType('error', 'Error', err);
            });
    }

    openDiscuss = () => {
        console.log("SHOW DISCUSS");
    };

    content(){
        return(
            <View style={{height: 150, padding: 10}}>
                <View style={{flex: 1, width: '100%'}}>
                    <TouchableOpacity style={styles.button} onPress={this.selectFile}>
                        <Text>Pilih file...</Text>
                    </TouchableOpacity>
                    <Text style={styles.fileInfo}>{this.state.fileUpload.name}</Text>
                    <Button title={"KIRIM"} onPress={this.uploadFile.bind(this, this.state.fileUpload)}/>
                </View>
            </View>
        )
    }

    render(){
        return (
            <AnimatableView style={ BaseStyles.overlayContainerLMS }
                            animation="fadeInRight"
                            easing="ease-in-out-sine"
                            duration={ 300 }
                            ref="baseView">
                <TopicBox component={this.content()}/>
                <View>
                    <TouchableOpacity onPress={this.openDiscuss}>
                        <Text>Tampilkan Diskusi</Text>
                    </TouchableOpacity>
                </View>
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
            </AnimatableView>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        borderColor: '#9B9B9B',
        borderWidth: 1 / PixelRatio.get(),
        margin: 5,
        padding: 5
    },
    fileInfo: {
        borderColor: '#9B9B9B',
        borderWidth: 1 / PixelRatio.get(),
        margin: 5,
        padding: 5
    }
});

export default TopicAssignment;