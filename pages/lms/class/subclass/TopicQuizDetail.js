import React from 'react';
import {View, Text, TouchableOpacity, Button} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import BaseStyles from '../../../../styles/base';
import * as Constants from '../../../../styles/constants';
import {View as AnimatableView} from "react-native-animatable";
import TopicBox from "../../../../components/TopicBox";
import Link from "react-router-native/Link";


class TopicQuizDetail extends React.Component{

    openDiscuss = () => {
        console.log("SHOW DISCUSS");
    };

    content() {
        return (
            <View style={{height: 200, padding: 10}}>
                <View style={{flex: 1, width: '100%', flexDirection: 'column', alignItems: 'center'}}>
                    <Icon name='edit' size={50} color='gray'/>
                    <Text style={{fontSize: 18}}>Mulai Quiz</Text>
                    <Text numberOfLines={3} style={{textAlign: 'center'}}>Mulai quiz Anda dan selesaikan sebelum batas akhir waktu.</Text>
                    <Link to={{pathname: "/class/attemptquiz", state: {...this.props.route.location.state}}} component={TouchableOpacity}>
                        <View style={{backgroundColor: Constants.COLOR.BLUE, margin: 10, padding: 10, borderRadius: 5}}><Text style={{color: 'white'}}>Mulai Quiz</Text></View>
                    </Link>
                </View>
            </View>
        )
    }

    render(){
        console.log(this.props);
        return(
            <AnimatableView style={ BaseStyles.overlayContainerLMS }
                            animation="fadeInRight"
                            easing="ease-in-out-sine"
                            duration={ 300 }
                            ref="baseView">
                <TopicBox component={this.content()}/>
                <View style={{padding: 10}}>
                    <TouchableOpacity onPress={this.openDiscuss}>
                        <Text>Tampilkan Diskusi</Text>
                    </TouchableOpacity>
                </View>
            </AnimatableView>
        )
    }

}

export default TopicQuizDetail;