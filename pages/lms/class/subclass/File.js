import React, {Component, Fragment} from 'react';
import {FlatList, Text, View} from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import BaseStyles from "../../../../styles/base";
import SwitchSelector from "react-native-switch-selector";
import {dataFile} from "../../dummy/dataku";
import ListItem from "../../../../components/backpack/ListItem";

class File extends Component{

    state = {
        exit: false,
        fetching: false,
        error: false,
        loaded: false,
        topic: {},
        content: [],
    }

    componentDidMount(){

    }

    render(){
        return (
            <View style={BaseStyles.container}>
                <FlatList data={dataFile.data}
                          renderItem={ ({item}) => <ListItem item={item} withFeatured={true}/>}
                          keyExtractor={item => item.key}
                />
            </View>
        );
    }
}

export default File;