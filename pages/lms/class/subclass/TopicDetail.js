import React, {Component} from 'react';
import {BackHandler, Text, View, TouchableOpacity, StyleSheet, PixelRatio, Button, Image} from 'react-native';
import {Redirect} from 'react-router-native';

import BaseStyles from '../../../../styles/base';
import Header from "../../../../components/HeaderLMS";
import {View as AnimatableView} from "react-native-animatable";
import TopicAssignment from "./TopicAssignment";
import TopicQuiz from "./TopicQuizDetail";
import TopicResource from "./TopicResource";
import Spinner from "../../../../components/Spinner";
import Error from "../../../../components/Error";


class TopicDetail extends Component{

    state ={
        exit: false,
        fetching: false,
        error: false
    }

    exit = this._exit.bind(this);

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.exit);
        this.loadData();
    }

    _exit() {
        this.refs.baseView.fadeOutRight(300);
        this.cancelToken && this.cancelToken();
        BackHandler.removeEventListener('hardwareBackPress', this.exit);
        setTimeout(() => {
            this.setState({ exit: true });
        }, 300);
        return true;
    }

    judulType(type){
        if (type === 'assignment') {
            return "Tugas"
        }
        else if (type === 'quiz') {
            return "Quiz"
        }
        else{
            return "Materi"
        }
    }

    loadData = () => {
        // this.setState({fetching: true, error: false});
    };

    render(){
        let content = null;
        switch (this.props.route.location.state.typeTopic) {
            case 'assignment': content = <TopicAssignment {...this.props}/>; break;
            case 'quiz': content = <TopicQuiz {...this.props}/>; break;
            case 'resource': content = <TopicResource {...this.props} teachableItem={this.props.route.location.state.teachableItem}/>; break;
        }

        if (this.state.exit){
            return <Redirect to={'/class/' + this.props.route.match.params.slug + '/classroom?section=topic'}/>
        }

        return (
            <AnimatableView style={ BaseStyles.overlayContainerLMS }
                  animation="fadeInRight"
                  easing="ease-in-out-sine"
                  duration={ 300 }
                  ref="baseView">
                <Header title={this.judulType(this.props.route.location.state.typeTopic)} back={ () => this._exit()}/>
                {this.state.fetching && <Spinner/>}
                {this.state.error && <Error/>}
                {
                    !this.state.fetching && !this.state.error && (
                        <View style={{flex: 1, width: '100%'}}>
                            {content}
                        </View>
                    )
                }
            </AnimatableView>
        )
    }
}

export default TopicDetail;