import React, {Component, Fragment} from 'react';
import {
    View,
    TouchableWithoutFeedback,
    Keyboard,
    FlatList
} from 'react-native';

import ComposeText from '../../../../components/discuss/ComposeText';
import {dataDiskusi} from "../../dummy/dataku";
import ListItem from "../../../../components/discuss/ListItem";
import BaseStyles from '../../../../styles/base';
import * as Cons from '../../../../styles/constants';


class Discuss extends Component{

    render(){
        return(
          <View style={[BaseStyles.overlayContainerLMS, {backgroundColor: Cons.COLOR_GRAY.LIGHTEST}]}>
              <TouchableWithoutFeedback onPress={Keyboard.dismiss} >
                  <View style={{flex: 1, marginTop: 50, marginBottom: 20}}>
                      <FlatList data={dataDiskusi.data}
                                renderItem={ ({item}) => <ListItem item={item} withFeatured={true}/>}
                                keyExtractor={item => item.id.toString()}
                      />
                  </View>
              </TouchableWithoutFeedback>
              <ComposeText/>
          </View>
        );
    }
}




export default Discuss;