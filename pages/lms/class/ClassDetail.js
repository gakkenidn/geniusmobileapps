import React, {Component} from 'react';
import {BackHandler, Image, ScrollView, Text, View, StyleSheet} from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import BaseStyles from "../../../styles/base";
import * as StyleConstants from "../../../styles/constants";
import {Link, Redirect} from "react-router-native";
import Touchable from "react-native-platform-touchable";
import Header from "../../../components/HeaderLMS";
import Query from "query-string";

class ClassDetail extends Component{

    state = {
        exit: false,
        fetching: false,
        error: false,
        loaded: false,
        topic: {},
        content: [],
        slug: this.props.route.match.params.slug,
        tf: false,
        tfTemp: false,
    }
    exit = this._exit.bind(this);

    componentDidMount(){
        this.props.isFull(true);
        BackHandler.addEventListener('hardwareBackPress', this.exit);
    }

    _exit() {
        this.refs.baseView.fadeOutRight(300);
        this.cancelToken && this.cancelToken();
        BackHandler.removeEventListener('hardwareBackPress', this.exit);
        setTimeout(() => {
            this.setState({ exit: true });
        }, 300);
        this.props.isFull(false);
        return true;
    }

    render(){
        if (this.state.exit) {
            return <Redirect
                to={Query.parse(this.props.route.location.search).ref ? Query.parse(this.props.location.search).ref : '/class'}/>;
        }

        return (
            <AnimatableView style={ BaseStyles.overlayContainerLMS }
                            animation="fadeInRight"
                            easing="ease-in-out-sine"
                            duration={ 300 }
                            ref="baseView">
                <Header title="Detail Kelas" back={ () => this._exit()} />
                <ScrollView>
                    <View style={{flex: 1, backgroundColor: StyleConstants.COLOR_GRAY.LIGHTEST}}>
                        <View style={{
                            paddingHorizontal: StyleConstants.SPACING.X4,
                            paddingVertical: StyleConstants.SPACING.X4,
                            backgroundColor: StyleConstants.COLOR.WHITE,
                            elevation: 1,
                            shadowColor: StyleConstants.COLOR.BLACK,
                            shadowOpacity: 0.2,
                            shadowOffset: { height: 1, width: 1 }
                        }}>
                            <Text style={[BaseStyles.topic]} numberOfLines={1}>Tema</Text>
                            <Text style={BaseStyles.h4} numberOfLines={1}>Judul</Text>
                            <Text style={BaseStyles.p} numberOfLines={1}>Deskripsi</Text>
                            <View style={{marginLeft: 20, marginRight: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                                <View style={styles.promptNilai}>
                                    <Text style={BaseStyles.h4}>100</Text>
                                    <Text>Pelajaran</Text>
                                </View>
                                <View style={styles.promptNilai}>
                                    <Text style={BaseStyles.h4}>9/10</Text>
                                    <Text>Tugas</Text>
                                </View>
                                <View style={styles.promptNilai}>
                                    <Text style={BaseStyles.h4}>0/100</Text>
                                    <Text>Total Progress</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{
                            alignItems:     'center',
                            justifyContent: 'space-around',
                            flexDirection: 'column',
                            marginTop: 10,
                            marginRight: 10,
                            marginLeft: 10,
                            marginBottom: 10
                        }}>
                            <View style={{flex: 2, flexDirection:"row", justifyContent: 'space-around', width: '100%'}}>
                                <Link to={'/class/' + this.state.slug + '/classroom?section=topic'} component={Touchable} >
                                    <View style={BaseStyles.promptButtonImage}>
                                        <Image source={require('../../../resources/images/Class.png')} style={{width: 50, height: 50}}/>
                                        <Text style={{paddingVertical: StyleConstants.SPACING.X2}}>Topik</Text>
                                    </View>
                                </Link>
                                <Link to={'/class/' + this.state.slug + '/classroom?section=discuss'} component={Touchable} hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}>
                                    <View style={BaseStyles.promptButtonImage}>
                                        <Image source={require('../../../resources/images/Discuss.png')} style={{width: 50, height: 50}}/>
                                        <Text style={{paddingVertical: StyleConstants.SPACING.X2}}>Diskusi</Text>
                                    </View>
                                </Link>
                            </View>
                            <View style={{flex: 2, flexDirection:"row", justifyContent: 'space-around', marginTop: 10}}>
                                <Link to={'/class/' + this.state.slug + '/classroom?section=file'} component={Touchable} hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}>
                                    <View style={BaseStyles.promptButtonImage}>
                                        <Image source={require('../../../resources/images/BackpackIdle.png')} style={{width: 50, height: 50}}/>
                                        <Text style={{paddingVertical: StyleConstants.SPACING.X2}}>File</Text>
                                    </View>
                                </Link>
                                <View style={{flex: 0.1}}/>
                                <Touchable>
                                    <View style={{flex: 2, padding: 10}}/>
                                </Touchable>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </AnimatableView>
        );
    }
}

const styles = StyleSheet.create({
   promptNilai: {
       flexDirection: 'column',
       justifyContent: 'center',
       alignItems: 'center',
       padding: 20
   },
});

export default ClassDetail;