import React, {Fragment} from 'react';
import {Route, Switch, AndroidBackButton} from 'react-router-native';

import ClassList from './ClassList';
import ClassDetail from './ClassDetail';
import SubClass from './subclass/Index';
import TopicDetail from './subclass/TopicDetail';
import TopicQuizAttempt from "./subclass/TopicQuizAttempt";

class Classes extends React.Component {
    render() {
        return(
            <Fragment>
                <Route path="/class" render={route => <ClassList {...this.props} route={route}/>}/>
                <Route path="/class/:slug/detail" render={route => <ClassDetail {...this.props} route={route}/>}/>
                <Route path="/class/:slug/classroom" render={route => <SubClass {...this.props} route={route}/>}/>
                <Route path="/class/:slug/detail/:subslug" render={(route) => <TopicDetail {...this.props} route={route}/>}/>
                <Route path="/class/attemptquiz" render={(route) => <TopicQuizAttempt {...this.props} route={route}/>}/>
             </Fragment>
        );
    }
}

export default Classes;