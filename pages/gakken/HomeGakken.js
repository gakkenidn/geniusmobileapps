import React, { Component } from 'react';
import { ImageBackground, ScrollView, Text, View } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import Touchable from 'react-native-platform-touchable';
import { Link } from 'react-router-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

import BaseStyles from '../../styles/base';
import * as StyleConstants from '../../styles/constants';
import ArticlesSlider from '../../components/articles/HomepageSlider';
import CatalogSlider from '../../components/catalog/HomepageSlider';

const HomeGakken = props => (
  <AnimatableView animation="fadeInRight" duration={ 300 } easing="ease-in-out-sine" style={ BaseStyles.container }>
    <View style={{
      zIndex: 980,
      elevation: 4,
      shadowColor: StyleConstants.COLOR.BLACK,
      shadowOffset: { height: 3 },
      shadowOpacity: 0.2,
      backgroundColor: StyleConstants.COLOR_GRAY.LIGHTEST,
      paddingTop: getStatusBarHeight(true)
      }}>
      <View style={{ height: StyleConstants.SPACING.X4 }} />
      <View style={ BaseStyles.content }>
        <Text style={{
          fontFamily: StyleConstants.FONT_FAMILY_HEADINGS,
          fontSize: 20,
          color: StyleConstants.COLOR.PRIMARY
        }}>Selamat Datang</Text>
        <Text style={ BaseStyles.p }>di Gakken Indonesia!</Text>

        <Text style={ BaseStyles.p }>Temukan semua kebutuhan pembelajaran dan keprofesian kesehatan di Gakken Indonesia.</Text>
        <View style={{ height: StyleConstants.SPACING.X4 }} />
      </View>
    </View>
    <ScrollView style={{ backgroundColor: StyleConstants.COLOR.WHITE }}>

      <View style={{ height: StyleConstants.SPACING.X3 }} />
      <ArticlesSlider { ...props } />

      <CatalogSlider { ...props } />

      <View style={ BaseStyles.content }>
        <View style={{ height: StyleConstants.SPACING.X4 }} />

        <Text style={ BaseStyles.h4 }>..temukan referensi kesehatan</Text>
        <Link to="/learn?section=journals" component={ Touchable } style={{ borderRadius: StyleConstants.SPACING.X2, overflow: 'hidden', marginBottom: StyleConstants.SPACING.X3 }}>
          <ImageBackground
            source={ require('../../resources/images/JournalsHomeFeatureImage.png') }
            style={{ width: '100%', height: 80, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_HEADINGS, fontSize: StyleConstants.FONT_SIZE.H3, color: StyleConstants.COLOR.WHITE }}>
              Jurnal Premium
            </Text>
          </ImageBackground>
        </Link>
        <Link to="/learn?section=drugs" component={ Touchable } style={{ borderRadius: StyleConstants.SPACING.X2, overflow: 'hidden' }}>
          <ImageBackground
            source={ require('../../resources/images/DrugsHomeFeatureImage.png') }
            style={{ width: '100%', height: 80, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_HEADINGS, fontSize: StyleConstants.FONT_SIZE.H3, color: StyleConstants.COLOR.WHITE }}>
              Indeks Obat
            </Text>
          </ImageBackground>
        </Link>
      </View>

      <View style={{ height: StyleConstants.SPACING.X5 }} />
    </ScrollView>
  </AnimatableView>
);

export default HomeGakken;