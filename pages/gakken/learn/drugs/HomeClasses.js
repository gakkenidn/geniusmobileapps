import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Link } from 'react-router-native';
import Touchable from 'react-native-platform-touchable';

import BaseStyles from '../../../../styles/base';
import DrugStyles from '../../../../styles/drugs';
import * as StyleConstants from '../../../../styles/constants';
import Spinner from '../../../../components/Spinner';

class HomeClasses extends Component {
  state = {  }

  componentDidMount() {
    this.props.getClasses();
  }

  render() {
    return (
      <View style={ BaseStyles.content }>
        <View style={{ height: StyleConstants.SPACING.X4 }} />
        <Text style={ BaseStyles.h2Inline }>Cari berdasarkan kelas obat</Text>
        <Text style={ BaseStyles.mutedSmall }>Temukan informasi obat yang Anda cari dengan lebih mudah melalui kelas obat.</Text>
        <View style={{ height: StyleConstants.SPACING.X3 }} />

        { this.props.fetching && <Spinner /> }
        { this.props.error && <Error onRetry={ () => this.props.getClasses() } /> }
        {
          this.props.data.length ?
            (
              <View style={ DrugStyles.categoryList }>
                {
                  this.props.data.map((drugClass, index) =>
                    (
                      <Link
                        key={ drugClass.code }
                        to={ "/learn/drugs/class/" + drugClass.code }
                        component={ Touchable }
                        style={
                          index === (this.props.data.length - 1) ?
                            DrugStyles.categoryListItemEnd : DrugStyles.categoryListItem
                        }>
                        <Text style={ DrugStyles.categoryListItemText } numberOfLines={ 1 }>
                          { drugClass.name }
                        </Text>
                      </Link>
                    )
                  )
                }
              </View>
            ) : null
        }
      </View>
    );
  }
}

export default HomeClasses;