import React, { Component, Fragment } from 'react';
import { Text, View } from 'react-native';
import { Link } from 'react-router-native';
import Touchable from 'react-native-platform-touchable';
import { CancelToken } from 'axios';

import DrugsApi from '../../../../api/drugs';
import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';
import Spinner from '../../../../components/Spinner';

class SingleRelated extends Component {
  state = {
    byManufacturer: [],
    byClass: [],
    fetching: false,
    error: false,
  }
  cancelToken = false;

  componentDidMount() {
    this.load();
  }

  componentWillUnmount() {
    this.cancelToken && this.cancelToken();
  }

  load() {
    if (this.state.fetching)
      return;
    this.setState({ fetching: true, error: false });
    DrugsApi.index({
      params: { relatedTo: this.props.slug },
      cb: data => {
        this.setState({ byManufacturer: data.byManufacturer.data, byClass: data.byClass.data, fetching: false });
        this.cancelToken = false;
      },
      err: () => {
        this.setState({ fetching: false, error: true });
        this.cancelToken = false;
      },
      cancelToken: new CancelToken(c => { this.cancelToken = c; })
    });
  }

  render() {
    return (
      <Fragment>

        <View style={ BaseStyles.content }>
          <Text style={ BaseStyles.h3 }>Produsen sama</Text>
        </View>
        { this.state.fetching && <Spinner /> }
        { this.state.byManufacturer.map(drug => <DrugItem key={ drug.slug } { ...drug } displayField="drugSubclass" />) }

        <View style={{ height: StyleConstants.SPACING.X4 }} />

        <View style={ BaseStyles.content }>
          <Text style={ BaseStyles.h3 }>Indikasi serupa</Text>
        </View>
        { this.state.fetching && <Spinner /> }
        { this.state.byClass.map(drug => <DrugItem key={ drug.slug } { ...drug } displayField="manufacturer" />) }

      </Fragment>
    );
  }
}

const DrugItem = props => (
  <View
    style={{
      borderBottomColor: StyleConstants.COLOR_GRAY.LIGHTEST,
      borderBottomWidth: 1,
    }}>
    <Link
      to={ "/learn/drugs/" + props.slug }
      component={ Touchable }
      style={{
        paddingVertical: StyleConstants.SPACING.X3,
      }}>
      <View style={ BaseStyles.content }>
        <Text style={{
          color:      StyleConstants.COLOR_GRAY.DARK,
          fontSize:   StyleConstants.FONT_SIZE.H5,
          fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF,
        }}>{ props.name }</Text>
        <Text style={ BaseStyles.muted }>{ props[props.displayField] }</Text>
      </View>
    </Link>
  </View>
)

export default SingleRelated;