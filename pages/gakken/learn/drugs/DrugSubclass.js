import React, { Component, Fragment } from 'react';
import { BackHandler, ScrollView, Text, View } from 'react-native';
import { Redirect } from 'react-router-native'
import { View as AnimatableView } from 'react-native-animatable';

import Header from '../../../../components/Header';
import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';
import DrugListItem from './DrugListItem';

class DrugSubclass extends Component {
  state = {
    exit: false,
    loaded: false,
    drugClass: '',
    subclass: {},
  }
  exit = this._exit.bind(this);

  componentDidMount() {
    let drugClass = this.props.data.filter(drugClass => drugClass.code === this.props.match.params.slug)[0];
    this.setState({
      drugClass: drugClass.code,
      subclass: drugClass.subclasses.data.filter(subclass => subclass.code === this.props.match.params.subslug)[0],
      loaded: true
    });
    BackHandler.addEventListener('hardwareBackPress', this.exit);
  }

  _exit() {
    BackHandler.removeEventListener('hardwareBackPress', this.exit);
    this.refs.baseView.fadeOutRight(200);
    setTimeout(() => {
      this.setState({ exit: true });
    }, 200);
    return true;
  }

  render() {
    if (this.state.exit) {
      // console.warn(this.state.drugClass);
      return <Redirect to={ "/learn/drugs/" + this.state.drugClass } />;
    }

    return (
      <AnimatableView
        ref="baseView"
        animation="fadeInRight"
        duration={ 300 }
        easing="ease-in-out-sine"
        style={ BaseStyles.overlayContainer }>

        <Header back={ () => this._exit() } titleComponent={
          (
            <View style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
              <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_HEADINGS, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>Cari Obat</Text>
              <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>
                { this.state.loaded && this.state.subclass.name }
              </Text>
            </View>
          )
        } />

        {
          this.state.loaded &&
          (
            <ScrollView style={{ flex: 1 }}>
              <View style={{ height: StyleConstants.SPACING.X4 }} />
              {
                this.state.subclass.drugs.data.length ?
                (
                  <Fragment>
                    <View style={ BaseStyles.content }>
                      <Text style={ BaseStyles.h4 }>Obat</Text>
                    </View>
                    {
                      this.state.subclass.drugs.data.map(drug =>
                        <DrugListItem key={ drug.slug } { ...drug } referrer={ this.props.location.pathname } />
                      )
                    }
                    <View style={{ height: StyleConstants.SPACING.X2 }} />
                  </Fragment>
                ) : null
              }
              <View style={{ height: StyleConstants.SPACING.X5 }} />
            </ScrollView>
          )
        }
      </AnimatableView>
    );
  }
}

export default DrugSubclass;