import React from 'react';
import { Text, View } from 'react-native';
import { Link } from 'react-router-native';

import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';

const SubclassListItem = props => (
  <Link
    to={ '/learn/drugs/class/' + props.drugClass + '/' + props.code }
    style={{
      borderTopColor:   StyleConstants.COLOR_GRAY.LIGHTEST,
      borderTopWidth:   1,
      paddingVertical:  StyleConstants.SPACING.X3,
    }}>
    <View style={ BaseStyles.content}>
      <Text style={{
        color:      StyleConstants.COLOR_GRAY.NORMAL,
        fontSize:   StyleConstants.FONT_SIZE.H5,
        fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF,
      }}>{ props.name }</Text>
    </View>
  </Link>
);

export default SubclassListItem;