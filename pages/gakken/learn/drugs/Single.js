import React, { Component, Fragment } from 'react';
import { BackHandler, Image, ScrollView, Text, View } from 'react-native';
import { Redirect } from 'react-router-native';
import { View as AnimatableView } from 'react-native-animatable';
import Touchable from 'react-native-platform-touchable';
import { CancelToken } from 'axios';

import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';
import Header from '../../../../components/Header';
import DrugsApi from '../../../../api/drugs';
import Spinner from '../../../../components/Spinner';
import Error from '../../../../components/Error';
import SingleTabs from './SingleTabs';
import SingleOverview from './SingleOverview';
import SingleComposition from './SingleComposition';
import SingleDosages from './SingleDosages';
import SinglePackaging from './SinglePackaging';
import SingleRelated from './SingleRelated';

class Single extends Component {
  state = {
    drug: {},
    exit: false,
    fetching: false,
    loaded: false,
    error: false,
    activePage: 'overview',
  }
  cancelToken = false;
  exit = this._exit.bind(this);

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.exit);
    this.load(this.props.match.params.slug);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.slug !== this.props.match.params.slug)
      this.load(nextProps.match.params.slug);
  }

  _exit() {
    this.refs.baseView.fadeOutRight(200);
    this.cancelToken && this.cancelToken();
    BackHandler.removeEventListener('hardwareBackPress', this.exit);
    setTimeout(() => {
      this.setState({ exit: true });
    }, 200);
    return true;
  }

  load(slug) {
    if (this.state.fetching)
      return;

    this.setState({ fetching: true, error: false, loaded: false });
    DrugsApi.show({
      slug,
      cb: drug => {
        this.setState({ drug, fetching: false, loaded: true });
        this.cancelToken = false;
      },
      err: e => {
        this.setState({ fetching: false, error: true });
        this.cancelToken = false;
      },
      cancelToken: new CancelToken(c => { this.cancelToken = c; })
    });
  }

  render() {
    if (this.state.exit)
      return <Redirect to={ "/learn/drugs" } />

    return (
      <AnimatableView
        ref="baseView"
        animation="fadeInRight"
        duration={ 300 }
        style={ BaseStyles.overlayContainer }>

        <Header back={ () => this._exit() } titleComponent={
          (
            <View style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
              <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_HEADINGS, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>Indeks Obat</Text>
              <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>
                {
                  this.state.loaded ?
                    this.state.drug.name :
                    'Memuat..'
                }
              </Text>
            </View>
          )
        } />

        { this.state.fetching && <View style={{ flex: 1 }}><Spinner /></View> }
        {
          this.state.loaded &&
          (
            <Fragment>
              <View>
                <View style={{ backgroundColor: StyleConstants.COLOR_GRAY.LIGHTEST }}>
                  <View style={ BaseStyles.content }>
                    <View style={{ height: StyleConstants.SPACING.X4 }} />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start', marginBottom: 30 }}>
                      <View style={{ flex: 1 }}>
                        <Text style={ BaseStyles.h2Inline }>{ this.state.drug.name }</Text>
                        <Text style={ BaseStyles.muted }>
                          Diproduksi oleh { this.state.drug.manufacturer }
                        </Text>
                        {
                          this.state.drug.marketer ?
                          (
                            <Text style={ BaseStyles.muted }>
                              Dipasarkan oleh { this.state.drug.marketer }
                            </Text>
                          ) : null
                        }
                      </View>
                      <Image source={ require('../../../../resources/images/Bebas.png') } style={{ height: 60, width: this.state.drug.classification === 'b' ? 60 : 0 }} />
                      <Image source={ require('../../../../resources/images/BebasTerbatas.png') } style={{ height: 60, width: this.state.drug.classification === 'w' ? 60 : 0 }} />
                      <Image source={ require('../../../../resources/images/Keras.png') } style={{ height: 60, width: this.state.drug.classification === 'g' ? 60 : 0 }} />
                      <Image source={ require('../../../../resources/images/Narkotika.png') } style={{ height: 60, width: this.state.drug.classification === 'o' ? 60 : 0 }} />
                    </View>
                  </View>
                </View>

                <SingleTabs onChange={ ({activePage, origin = false }) => this.setState({ activePage })} active={ this.state.activePage } />
              </View>

              <ScrollView>
                <View style={{ height: StyleConstants.SPACING.X4 }} />

                { this.state.activePage === 'overview' && <SingleOverview { ...this.state.drug } /> }
                { this.state.activePage === 'composition' && <SingleComposition { ...this.state.drug } /> }
                { this.state.activePage === 'dosages' && <SingleDosages { ...this.state.drug } /> }
                { this.state.activePage === 'packaging' && <SinglePackaging { ...this.state.drug } /> }
                { this.state.activePage === 'related' && <SingleRelated { ...this.state.drug } /> }

                <View style={{ height: StyleConstants.SPACING.X4 }} />
              </ScrollView>
            </Fragment>
          )
        }
        { this.state.error && <Error onRetry={ () => this.load() } /> }

      </AnimatableView>
    );
  }
}

export default Single;