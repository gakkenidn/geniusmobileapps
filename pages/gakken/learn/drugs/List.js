import React, { Component } from 'react';
import { FlatList, TextInput } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';

import DrugsApi from '../../../../api/drugs';
import ListItem from './DrugListItem';
import Header from '../../../../components/Header';
import Error from '../../../../components/Error';
import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';
import Spinner from '../../../../components/Spinner';

class List extends Component {
  state = {
    drugs: [],
    fetching: false,
    error: false,
    term: ''
  }
  charThreshold = 3;

  componentDidMount() {
    this.refs.termInput.focus();
  }

  search({ term }) {
    this.setState({ term });
    if (term.length < this.charThreshold)
      return false;

    this.setState({ fetching: true, error: false });
    DrugsApi.index({
      cb: drugs => this.setState({ drugs, fetching: false }),
      err: () => this.setState({ error: true, fetching: false }),
      params: {
        term
      }
    });
  }

  render() {
    return (
      <AnimatableView style={ BaseStyles.overlayContainer } animation="fadeInRight" duration={ 300 }>
        <Header
          back="/learn"
          titleComponent={
            (
              <TextInput
                ref="termInput"
                placeholder="Cari obat.."
                underlineColorAndroid={ StyleConstants.COLOR_GRAY.LIGHTEST }
                onSubmitEditing={ e => this.search({ term: e.nativeEvent.text }) }
                style={{
                  fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF,
                  fontSize: StyleConstants.FONT_SIZE.H3,
                  marginTop: StyleConstants.SPACING_BASE
                }} />
            )
          }
          bgStyle="light"
          />

          { this.state.fetching && <Spinner /> }
          {
            this.state.error ?
              <Error onRetry={ () => this.search({ term: this.state.term }) } /> :
              (
                <FlatList
                  data={ this.state.drugs }
                  keyExtractor={ item => item.slug }
                  renderItem={ ({ item, index }) => <ListItem { ...item } index={ index } linkQuery="ref=/learn/drugs/all" /> } />
              )
          }
      </AnimatableView>
    );
  }
}

export default List;