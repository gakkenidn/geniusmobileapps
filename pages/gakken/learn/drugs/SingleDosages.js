import React from 'react';
import { Text, View } from 'react-native';
import _ from 'lodash';

import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';

const SingleDosages = props => (
  <View style={ BaseStyles.content }>
    {
      props.dosages.map((dosage, index) => {
        let mutedText = [];
        if (props.forms.length > 1)
          mutedText.push(dosage.form ? 'Untuk sediaan ' + dosage.form.name : 'Untuk semua jenis sediaan');
        mutedText.push(dosage.administration);

        return (
          <View key={ index } style={{ marginBottom: StyleConstants.SPACING.X3 }}>
            <Text style={ BaseStyles.mutedSmall }>{ mutedText.join(' ') }</Text>
            <Text style={ BaseStyles.text }>
              { dosage.name ? _.trim(dosage.name) + ': ' : '' }
              { _.trim(dosage.description) }
            </Text>
          </View>
        );
      })
    }
  </View>
);

export default SingleDosages;