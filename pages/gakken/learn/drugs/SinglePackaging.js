import React from 'react';
import { Text, View } from 'react-native';
import _ from 'lodash';

import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';

const SingleDosages = props => (
  <View style={ BaseStyles.content }>
    {
      props.packagings.map((pack, index) => {
        let mutedText = [];
        if (pack.quantity)
          mutedText.push(
            pack.quantity + ' x' +
            (pack.quantity2 ? ' ' + pack.quantity2 + ' x' : '') +
            (pack.quantity3 ? ' ' + pack.quantity3 + ' x' : '')
          );
        mutedText.push(pack.form.name);
        mutedText.push(pack.name);
        return (
          <View key={ index } style={{ marginBottom: StyleConstants.SPACING.X3 }}>
            <Text style={ BaseStyles.mutedSmall }>{ mutedText.join(' ') }</Text>
            <Text style={ BaseStyles.text }>
              { pack.price }
            </Text>
          </View>
        );
      })
    }
  </View>
);

export default SingleDosages;