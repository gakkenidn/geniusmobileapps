import React from 'react';
import { ScrollView } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import Catalog from './Catalog';

const Home = props => (
  <AnimatableView animation="fadeInLeft" duration={ 300 } easing="ease-in-out-sine" style={{ flex: 1, width: '100%' }}>
    <ScrollView>
      <Catalog topics={ props.catalog.data } getTopics={ props.getCatalog } fetching={ props.catalog.fetching } error={ props.catalog.error } />

    </ScrollView>
  </AnimatableView>
);

export default Home;