import React, { Component, Fragment } from 'react';
import { Image, ScrollView, Text, View } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import { Link } from 'react-router-native';
import Touchable from 'react-native-platform-touchable';

import Spinner from '../../../../components/Spinner';
import * as StyleConstants from '../../../../styles/constants';
import BaseStyles from '../../../../styles/base';

class Catalog extends Component {
  constructor(props) {
    super(props);
    this.state = {  };
  }

  componentDidMount() {
    if (!this.props.topics.length)
      this.props.getTopics();
  }

  render() {
    return (
      <Fragment>
        <View style={{ height: StyleConstants.SPACING.X4 }} />
        <View style={ BaseStyles.content }>
          <Text style={ BaseStyles.h2Inline }>Katalog</Text>
          <Text style={ BaseStyles.mutedSmall }>Koleksi topik GakkenP2KB bernilai SKP.</Text>
        </View>
        <View style={{ height: StyleConstants.SPACING.X3 }} />
        {
          this.props.fetching ?
            <Spinner /> :
            this.props.topics.map(topic => (
              <Link
                to={ '/learn/p2kb/' + topic.slug }
                key={ topic.slug }
                component={ Touchable }
                style={{
                  backgroundColor: StyleConstants.COLOR.WHITE,
                  paddingVertical: StyleConstants.SPACING.X3,
                  borderBottomColor: StyleConstants.COLOR_GRAY.LIGHTEST,
                  borderBottomWidth: 1,
                }}>
                <View style={ BaseStyles.content }>
                  <Text style={ BaseStyles.h5 }>{ topic.title }</Text>
                  <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={ BaseStyles.muted }>
                      2 SKP oleh { topic.category === 'doctor' ? 'IDI' : 'PDGI'}
                    </Text>
                    {
                      topic.alert &&
                      (
                        <View style={ topic.alert === 'Telah dilulusi' ? BaseStyles.badgeWarning : BaseStyles.badgeSuccess }>
                          <Text style={ BaseStyles.textInlineWhite }>
                            { topic.alert }
                          </Text>
                        </View>
                      )
                    }
                  </View>
                </View>
              </Link>
            ))
        }
        <View style={{ height: StyleConstants.SPACING.X5 }} />

      </Fragment>
    );
  }
}

export default Catalog;