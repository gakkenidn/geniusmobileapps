import React from 'react';
import PDF from 'react-native-pdf';

const PDFContentView = props => (
  <PDF source={{
    uri: props.data,
    headers: {
      'Authorization': 'Bearer ' + props.auth.access
    }
  }} style={{ width: '100%', height: '100%' }} />
);

export default PDFContentView;