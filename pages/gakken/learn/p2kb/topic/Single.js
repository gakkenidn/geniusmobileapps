import React, { Component, Fragment } from 'react';
import { BackHandler, FlatList, Image, ScrollView, Text, View } from 'react-native';
import Touchable from 'react-native-platform-touchable';
import { Redirect } from 'react-router-native';
import { View as AnimatableView } from 'react-native-animatable';
import Query from 'query-string';
import { CancelToken } from 'axios';
import { getStatusBarHeight } from 'react-native-status-bar-height';

import TopicsApi from '../../../../../api/topics';
import Header from '../../../../../components/Header';
import BaseStyles from '../../../../../styles/base';
import * as StyleConstants from '../../../../../styles/constants';
import Spinner from '../../../../../components/Spinner';
import ContentListItem from './ContentListItem';
import ContentView from './ContentView';

class Single extends Component {
  state = {
    exit: false,
    fetching: false,
    error: false,
    loaded: false,
    topic: {},
    passed: false,
    certified: false,
    remainingQuota: 0,
    content: [],
    selection: false,
  };
  exit = this._exit.bind(this);
  cancelToken = false;

  componentDidMount() {
    this.load();
    BackHandler.addEventListener('hardwareBackPress', this.exit);
  }

  load() {
    if (this.state.fetching)
      return;
    this.setState({ fetching: true, error: false, loaded: false });
    TopicsApi.show({
      slug: this.props.route.match.params.slug,
      token: this.props.auth.access,
      cancelToken: new CancelToken(c => { this.cancelToken = c; }),
      cb: response => {
        this.setState({
          topic: Object.assign({}, response.topic.data),
          passed: response.passed,
          certified: response.certified,
          remainingQuota: response.remainingQuota,
          content: response.content.data.slice(0),
          fetching: false,
          loaded: true
        });
        this.cancelToken = false;
      },
      err: e => {
        this.setState({ fetching: false, error: true });
        this.cancelToken = false;
      }
    });
  }

  _exit() {
    BackHandler.removeEventListener('hardwareBackPress', this.exit);
    this.cancelToken && this.cancelToken();
    this.refs.baseView.fadeOutRight(200);
    setTimeout(() => this.setState({ exit: true }), 200);
    return true;
  }

  render() {
    if (this.state.exit)
      return <Redirect to={ Query.parse(this.props.route.location.search).ref ? Query.parse(this.props.route.location.search).ref : '/learn/p2kb/' + this.props.route.match.params.slug } />;

    return (
      <Fragment>
        <AnimatableView ref="baseView" duration={ 300 } animation="fadeInRight" easing="ease-in-out-sine" style={ BaseStyles.overlayContainer }>
          <ScrollView>
            <View style={ BaseStyles.content }>
              <View style={{
                paddingTop: StyleConstants.SPACING.X2 + getStatusBarHeight(true),
                paddingBottom: StyleConstants.SPACING.X2,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                flexDirection: 'row',
                }}>
                <Touchable onPress={ () => this.exit() }>
                  <Image source={ require('../../../../../resources/images/ArrowLeft-Dark.png') } style={{ height: 20, width: 24, marginVertical: 16 }} />
                </Touchable>
                <View style={{ marginLeft: StyleConstants.SPACING.X3, paddingTop: StyleConstants.SPACING.X2, marginTop: StyleConstants.SPACING_BASE }}>
                  <Text style={ BaseStyles.h3 }>{ this.state.loaded ? this.state.topic.title : 'Memuat..' }</Text>
                </View>
              </View>

              { this.state.fetching && <Spinner /> }
            </View>
            { this.state.loaded && this.state.passed && (
              <View style={{ backgroundColor: StyleConstants.COLOR.GREEN, paddingVertical: StyleConstants.SPACING.X3 }}>
                <View style={ BaseStyles.content }>
                  <Text style={ BaseStyles.textInlineWhite }>
                    Anda telah melulusi topik ini.
                  </Text>
                </View>
              </View>
            ) }
            {
              this.state.loaded && !this.state.fetching &&
              <FlatList
                data={ this.state.content }
                renderItem={ item => {
                  // if (this.state.passed && item.item.type == 'quiz')
                  //   return null;
                  if (!this.state.passed && item.item.type == 'certificate')
                    return null;
                  return (
                    <ContentListItem { ...item }
                      onSelect={ selection => this.setState({ selection }) }
                      certified={ this.state.certified }
                      passed={ this.state.passed }
                      remainingQuota={ this.state.remainingQuota } />
                  );
                }}
                keyExtractor={ item => item.slug } />
            }
          </ScrollView>

        </AnimatableView>
        {
          this.state.selection &&
          <ContentView
            { ...this.state.selection }
            topic={ this.state.topic }
            auth={ this.props.auth }
            passed={ this.state.passed }
            certified={ this.state.certified }
            remainingQuota={ this.state.remainingQuota }
            onExit={ () => {
              this.setState({ selection: false });
              this.load();
            }} />
        }
      </Fragment>
    );
  }
}

export default Single;