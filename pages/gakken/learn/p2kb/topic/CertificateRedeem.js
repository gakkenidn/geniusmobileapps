import React, { Component } from 'react';
import { Text, View } from 'react-native';
import Touchable from 'react-native-platform-touchable';

import TopicsApi from '../../../../../api/topics';
import BaseStyles from '../../../../../styles/base';
import * as StyleConstants from '../../../../../styles/constants';

class CertificateRedeem extends Component {
  state = {
    redeeming: false,
    error: false,
  }
  cancelToken = false;

  redeem() {
    if (this.state.redeeming)
      return;

    this.setState({ redeeming: true })
    TopicsApi.redeem({
      topic: this.props.topic.slug,
      token: this.props.auth.access,
      cb: () => this.props.onRedeemed(),
      err: e => {
        console.warn(JSON.stringify(e));
        this.setState({ error: true });
      },
      cancelToken: this.cancelToken
    });
  }

  componentWillUnmount() {
    this.cancelToken && this.cancelToken();
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ height: StyleConstants.SPACING.X4 }} />
        <View style={ BaseStyles.content }>
          <Text style={ BaseStyles.h2 }>Selamat!</Text>
          <Text style={ BaseStyles.text }>
            Anda telah melulusi topik ini. Anda dapat menggunakan 1 dari { this.props.remainingQuota} kuota Anda untuk mendapatkan sertifikat bernilai 2 SKP.
          </Text>
        </View>
        <View style={{ height: StyleConstants.SPACING.X4 }} />

        <View style={ BaseStyles.content }>
          <Touchable style={ BaseStyles.promptButton } onPress={ () => this.redeem() }>
            <Text style={ BaseStyles.promptButtonText }>{ this.state.redeeming ? 'Harap tunggu..' : 'Dapatkan sertifikat' }</Text>
          </Touchable>
        </View>

      </View>
    );
  }
}

export default CertificateRedeem;