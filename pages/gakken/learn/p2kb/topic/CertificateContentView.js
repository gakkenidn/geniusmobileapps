import React from 'react';
import CertificateRedeem from './CertificateRedeem';
import PDFContentView from './PDFContentView';

const CertificateContentView = props => {
  if (props.certified)
    return <PDFContentView { ...props } />;

  if (props.remainingQuota > 0 && props.passed)
    return <CertificateRedeem
      { ...props }
      auth={ props.auth }
      remainingQuota={ props.remainingQuota }
      onRedeemed={ () => props.onRedeemed() } />

  return null;
};

export default CertificateContentView;