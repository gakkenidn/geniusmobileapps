import React from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';
import { WebView } from 'react-native-webview';
import Orientation from 'react-native-orientation';

import { API_BASE_URL } from '../../../../../env';

class VideoContentView extends React.Component{

    componentWillMount() {
        Orientation.lockToPortrait();
        if (!DeviceInfo.isTablet()) {
            Orientation.lockToPortrait();
        }
    }

    handleClick = () => {
        Orientation.unlockAllOrientations()
    }

    render()
    {
        return (
            <View style={{height: '100%', width: '100%', backgroundColor: 'black'}}>
                <TouchableWithoutFeedback onPress={this.handleClick}>
                    <WebView source={{
                        uri: API_BASE_URL + '/video/' + this.props.url_jw,
                        headers: {
                            'Authorization': 'Bearer ' + this.props.auth.access
                        }
                    }}/>
                </TouchableWithoutFeedback>
            </View>
        )
    }
};

export default VideoContentView;