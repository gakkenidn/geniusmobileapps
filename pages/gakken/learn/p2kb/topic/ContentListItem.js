import React from 'react';
import { Text, View } from 'react-native';
import Touchable from 'react-native-platform-touchable';
import _ from 'lodash';

import BaseStyles from '../../../../../styles/base';
import * as StyleConstants from '../../../../../styles/constants';

const ContentListItem = props => {

  let type = '';
  switch (props.item.type) {
    case 'text': type = 'Berkas PDF'; break;
    case 'certificate': type = 'Poin SKP'; break;
    default: type = props.item.type;
  }
  return (
    <Touchable onPress={ () => props.onSelect(props.item) }>
      <View style={{ paddingVertical: StyleConstants.SPACING.X3, borderTopColor: StyleConstants.COLOR_GRAY.LIGHTEST, borderTopWidth: 1 }}>
        <View style={ BaseStyles.content }>
          <Text style={ BaseStyles.muted }>
            { _.upperFirst(type) }
          </Text>
          <Text style={{
            fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF,
            fontSize: StyleConstants.FONT_SIZE.H5,
            color: StyleConstants.COLOR_GRAY.DARK
          }}>
            { props.item.label }
          </Text>
          {
            props.item.type === 'certificate'
            && props.passed
            && (
              <Text style={ BaseStyles.muted }>
                {
                 props.certified ?
                  'Lihat sertifikat Anda disini' :
                  (
                    props.remainingQuota > 0 ?
                      'Anda dapat menggunakan 1 dari ' + props.remainingQuota + ' kuota berlangganan Anda!' :
                      'Dapatkan sertifikat Anda melalui website Gakken!'
                  )
                }
              </Text>
            )
          }
        </View>
      </View>
    </Touchable>
  );
}

export default ContentListItem;