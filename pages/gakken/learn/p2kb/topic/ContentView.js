import React, { Component, Fragment } from 'react';
import { BackHandler } from 'react-native';
import { View } from 'react-native-animatable';

import BaseStyles from '../../../../../styles/base';
import Spinner from '../../../../../components/Spinner';
import Header from '../../../../../components/Header';
import PDFContentView from './PDFContentView';
import VideoContentView from './VideoContentView';
import QuizContentView from './QuizContentView';
import CertificateContentView from './CertificateContentView';

class ContentView extends Component {
  state = { }
  exit = this._exit.bind(this);

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.exit);
  }

  _exit() {
    BackHandler.removeEventListener('hardwareBackPress', this.exit);
    this.refs.baseView.fadeOutRight(300);
    setTimeout(() => {
      this.props.onExit()
    }, 300);
  }

  render() {
    let contentComponent = null;

    switch (this.props.type) {
      case 'text': contentComponent = <PDFContentView { ...this.props } />; break;
      case 'video': contentComponent = <VideoContentView { ...this.props } />; break;
      case 'quiz': contentComponent = <QuizContentView { ...this.props } />; break;
      case 'certificate': contentComponent = <CertificateContentView { ...this.props } onRedeemed={ () => this._exit() } />; break;
    }

    return (
      <View
        animation="fadeInRight"
        duration={ 300 }
        easing="ease-in-out-sine"
        style={ BaseStyles.overlayContainer }
        ref="baseView">

        <Header back={ () => this._exit() } />

        { contentComponent }

      </View>
    );
  }
}

export default ContentView;