import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import Touchable from 'react-native-platform-touchable';
import Query from 'query-string';

import Header from '../../../components/Header';
import BaseStyles from '../../../styles/base';
import P2KB from './p2kb/Home';
import Journals from './journals/Home';
import Drugs from './drugs/Home';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: Query.parse(this.props.location.search).section || 'p2kb',
      origin: false
    };
  }
  render() {
    return (
      <AnimatableView animation="fadeInRight" duration={ 300 } easing="ease-in-out-sine" style={ BaseStyles.container }>
        <Header title="Belajar" />
        <View style={{ flex: 1, width: '100%' }}>
          <View style={ BaseStyles.topNavbar }>
            <Touchable
              onPress={ () => this.setState({ activePage: 'p2kb'}) }
              style={ this.state.activePage === 'p2kb' ? BaseStyles.topNavbarItemActive : BaseStyles.topNavbarItem }>
              <Text style={ BaseStyles.topNavbarItemText }>Topik P2KB</Text>
            </Touchable>
            <Touchable
              onPress={ () => this.setState({ activePage: 'journals', origin: this.state.activePage}) }
              style={ this.state.activePage === 'journals' ? BaseStyles.topNavbarItemActive : BaseStyles.topNavbarItem }>
              <Text style={ BaseStyles.topNavbarItemText }>Jurnal</Text>
            </Touchable>
            <Touchable
              onPress={ () => this.setState({ activePage: 'drugs'}) }
              style={ this.state.activePage === 'drugs' ? BaseStyles.topNavbarItemActive : BaseStyles.topNavbarItem }>
              <Text style={ BaseStyles.topNavbarItemText }>Indeks Obat</Text>
            </Touchable>
          </View>

          { this.state.activePage === 'p2kb' && <P2KB { ...this.props } /> }
          { this.state.activePage === 'journals' && <Journals { ...this.props } origin={ this.state.origin } /> }
          { this.state.activePage === 'drugs' && <Drugs { ...this.props } /> }
        </View>
      </AnimatableView>
    );
  }
}

export default Home;