import React, { Fragment } from 'react';
import { Route, Switch } from 'react-router-native';

import Home from './Home';
import SingleCatalog from './p2kb/catalog/Single';
import SingleTopic from './p2kb/topic/Single';
import AllJournals from './journals/List';
import SingleJournal from './journals/Single';
import JournalCategory from './journals/Category';
import AllDrugs from './drugs/List';
import DrugClass from './drugs/DrugClass';
import DrugSubclass from './drugs/DrugSubclass';
import SingleDrug from './drugs/Single';


const Learn = props => (
  <Fragment>
    <Home { ...props } />

    <Route path="/learn/p2kb/:slug" render={ route => <SingleCatalog route={ route } { ...props } /> } />
    <Route path="/learn/p2kb/:slug/engage" render={ route => <SingleTopic route={ route } { ...props } /> } />
    <Switch>
      <Route exact path="/learn/journals/all" render={ () => <AllJournals /> } />
      <Route exact path="/learn/journals/category/:slug" render={ route => <JournalCategory { ...route } /> } />
      <Route exact path="/learn/journals/:slug" render={ route => <SingleJournal route={ route } { ...props } /> } />
    </Switch>
    <Switch>
      <Route path="/learn/drugs/class" render={ () => (
        <Fragment>
          <Route path="/learn/drugs/class/:slug" render={ route => <DrugClass { ...route } { ...props.drugClasses } /> } />
          <Route path="/learn/drugs/class/:slug/:subslug" render={ route => <DrugSubclass { ...route } { ...props.drugClasses } /> } />
        </Fragment>
      )} />
      <Route path="/learn/drugs/all" render={ route => <AllDrugs { ...route } /> } />
      <Route path="/learn/drugs/:slug" render={ route => <SingleDrug { ...route } /> } />
    </Switch>

  </Fragment>
);

export default Learn;