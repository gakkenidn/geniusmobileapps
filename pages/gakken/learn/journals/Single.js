import React, { Component, Fragment } from 'react';
import { BackHandler, Image, ScrollView, Text, View } from 'react-native';
import { Redirect } from 'react-router-native';
import { View as AnimatableView } from 'react-native-animatable';
import HTMLView from 'react-native-htmlview';
import Touchable from 'react-native-platform-touchable';
import Axios, { CancelToken } from 'axios';

import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';
import { description as DescriptionStyle } from '../../../../styles/journals';
import Header from '../../../../components/Header';
import JournalsApi from '../../../../api/journals';
import Spinner from '../../../../components/Spinner';
import Error from '../../../../components/Error';
import SingleRead from './SingleRead';

class Single extends Component {
  state = {
    journal: {},
    exit: false,
    fetching: false,
    loaded: false,
    error: false,
    readingURL: false,
    fetchingURL: false,
  }
  cancelToken = false;
  cancelReadToken = false;
  exit = this._exit.bind(this);

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.exit);
    this.load();
  }

  _exit() {
    this.refs.baseView.fadeOutRight(300);
    this.cancelToken && this.cancelToken();
    this.cancelReadToken && this.cancelReadToken();
    BackHandler.removeEventListener('hardwareBackPress', this.exit);
    setTimeout(() => {
      this.setState({ exit: true });
    }, 300);
    return true;
  }

  load() {
    if (this.state.fetching || this.state.loaded)
      return;

    this.setState({ fetching: true, error: false });
    JournalsApi.show({
      slug: this.props.route.match.params.slug,
      cb: journal => {
        this.setState({ journal, fetching: false, loaded: true });
        this.cancelToken = false;
      },
      err: e => {
        this.setState({ fetching: false, error: true });
        this.cancelToken = false;
      },
      cancelToken: new CancelToken(c => { this.cancelToken = c; })
    });
  }

  read() {
    if (this.state.fetchingURL)
      return;

    this.setState({ fetchingURL: true, readingURL: false });
    JournalsApi.getTicketedURL({
      slug: this.props.route.match.params.slug,
      token: this.props.auth.access,
      cb: readingURL => {
        this.setState({ readingURL, fetchingURL: false });
        this.cancelReadToken = false;
      },
      err: () => {},
      cancelToken: new CancelToken(c => { this.cancelReadToken = c; })
    });
  }

  retryRead() {
    let readingURL = this.state.readingURL;
    this.setState({ readingURL: false });
    this.setState({ readingURL });
  }

  render() {
    if (this.state.exit)
      return <Redirect to="/learn/journals" />

    return (
      <Fragment>
        <AnimatableView
          ref="baseView"
          animation="fadeInRight"
          duration={ 300 }
          style={ BaseStyles.overlayContainer }>

          <Header back={ () => this.exit() } titleComponent={
            (
              <View style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
                <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_HEADINGS, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>Jurnal Kesehatan</Text>
                <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>
                  {
                    this.state.loaded ?
                      this.state.journal.title :
                      'Memuat..'
                  }
                </Text>
              </View>
            )
          } />

          { this.state.fetching && <View style={{ flex: 1 }}><Spinner /></View> }
          {
            this.state.loaded &&
            (
              <ScrollView>
                <View style={ BaseStyles.content }>
                  <View style={{ height: StyleConstants.SPACING.X4 }} />
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'nowrap' }}>
                    { this.state.journal.imageUrl.length > 0 && <Image source={{ uri: this.state.journal.imageUrl }} style={{ height: 100, width: 80, marginRight: 18, borderRadius: 4 }} /> }
                    <View style={{ flex: 1 }}>
                      <Text style={ BaseStyles.h3 }>{ this.state.journal.title }</Text>
                      <Text style={ BaseStyles.muted }>
                        { this.state.journal.publisher }, ISSN: { this.state.journal.issn }
                      </Text>
                      {
                        this.state.journal.editor.length > 0 &&
                        (
                          <Text style={ BaseStyles.muted }>
                            Disunting oleh { this.state.journal.editor }
                          </Text>
                        )
                      }

                    </View>
                  </View>
                  <View style={{ height: StyleConstants.SPACING.X4 }} />
                  <HTMLView value={ this.state.journal.description } stylesheet={ DescriptionStyle } />

                </View>
              </ScrollView>
            )
          }
          { this.state.error && <Error onRetry={ () => this.load() } /> }

          <View style={ BaseStyles.promptContainer }>
            <View style={ BaseStyles.content }>
              <Touchable onPress={ () => this.read() }>
                <View style={ BaseStyles.promptButton }>
                  <Text style={ BaseStyles.promptButtonText }>
                    { this.state.fetchingURL ? 'Mengambil Jurnal..' : 'Baca Jurnal' }
                  </Text>
                </View>
              </Touchable>
            </View>
          </View>

        </AnimatableView>
        {
          this.state.readingURL &&
          (
            <SingleRead
              ticketedURL={ this.state.readingURL }
              onExit={ retry => retry ? this.retryRead() : this.setState({ readingURL: false }) } />
          )
        }
      </Fragment>
    );
  }
}

export default Single;