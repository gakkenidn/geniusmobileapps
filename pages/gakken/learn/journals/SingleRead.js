import React, { Component } from 'react';
import { BackHandler, View, WebView } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';

import BaseStyles from '../../../../styles/base';
import Spinner from '../../../../components/Spinner';
import Header from '../../../../components/Header';
import Error from '../../../../components/Error';

class SingleRead extends Component {
  state = {
    loading: false
  }
  exit = this._exit.bind(this);

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.exit);
  }

  _exit({ retry }) {
    BackHandler.removeEventListener('hardwareBackPress', this.exit);
    this.refs.baseView.fadeOutRight(300);
    setTimeout(() => {
      this.props.onExit(retry)
    }, 300);
  }

  render() {
    return (
      <AnimatableView
        animation="fadeInRight"
        duration={ 300 }
        easing="ease-in-out-sine"
        style={ BaseStyles.overlayContainer }
        ref="baseView">

        <Header back={ () => this._exit({ retry: false }) } title="Baca Jurnal" />
        <View style={{ flex: 1 }}>
          { this.state.loading && <Spinner /> }
          { this.state.error && <Error onRetry={ () => this._exit({ retry: true }) } /> }
          <WebView
            source={{
              uri: this.props.ticketedURL
            }}
            onLoadStart={ () => this.setState({ loading: true })}
            onLoadEnd={ () => this.setState({ loading: false })}
            onError={ () => this.setState({ error: true }) } />
        </View>

      </AnimatableView>
    );
  }
}

export default SingleRead;