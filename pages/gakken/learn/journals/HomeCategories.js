import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Link } from 'react-router-native';
import Query from 'query-string';
import Touchable from 'react-native-platform-touchable';

import BaseStyles from '../../../../styles/base';
import JournalStyles from '../../../../styles/journals';
import * as StyleConstants from '../../../../styles/constants';
import Spinner from '../../../../components/Spinner';
import Error from '../../../../components/Error';

class HomeCategories extends Component {
  constructor(props) {
    super(props);
    this.state = {  };
  }

  componentDidMount() {
    this.props.getCategories();
  }

  render() {
    return (
      <View style={ BaseStyles.content }>
        <View style={{ height: StyleConstants.SPACING.X4 }} />
        <Text style={ BaseStyles.h2Inline }>Cari berdasarkan kategori</Text>
        <Text style={ BaseStyles.mutedSmall }>Cari 400+ Jurnal Premium melalui puluhan kategori.</Text>
        <View style={{ height: StyleConstants.SPACING.X3 }} />

        { this.props.fetching && <Spinner /> }
        { this.props.error && <Error onRetry={ () => this.props.getCategories() } /> }
        {
          this.props.data.length ?
            (
              <View style={ JournalStyles.categoryList }>
                {
                  this.props.data.map((cat, index) =>
                    (
                      <Link key={ cat.slug } to={ "/learn/journals/category/" + cat.slug + '?' + Query.stringify({ categoryName: cat.name }) } component={ Touchable }  style={ index === (this.props.data.length - 1) ? JournalStyles.categoryListItemEnd : JournalStyles.categoryListItem }>
                        <Text style={ JournalStyles.categoryListItemText } numberOfLines={ 1 }>{ cat.name }</Text>
                      </Link>
                    )
                  )
                }
              </View>
            ) : null
        }
      </View>
    );
  }
}

export default HomeCategories;