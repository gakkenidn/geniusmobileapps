import React, { Fragment } from 'react';
import { Redirect, Route, Switch } from 'react-router-native';

import Home from './Home';
import Single from './Single';
import Search from './Search';
import Spinner from '../../../components/Spinner';

const Articles = props => (
  <Fragment>
    <Route path="/articles" render={ () => <Home articles={ props.articles.data } fetching={ props.articles.fetching } error={ props.articles.error } getArticles={ props.getArticles } /> } />
    <Switch>
      <Route exact path="/articles/search" render={ route => <Search articles={ props.articles.search } populate={ props.populateSearchArticles } /> } />
      <Route exact path="/articles/:slug" render={ route => <Single { ...route } /> } />
    </Switch>
  </Fragment>
);

export default Articles;