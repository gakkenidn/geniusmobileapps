import React, { Component, Fragment } from 'react';
import { BackHandler, Image, ScrollView, Text, View } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import HTMLView from 'react-native-htmlview';
import Query from 'query-string';
import { Redirect } from 'react-router-native';
import { CancelToken } from 'axios';

import BaseStyles from '../../../styles/base';
import * as StyleConstants from '../../../styles/constants';
import Header from '../../../components/Header';
import Articles from '../../../api/articles';
import ArticleStyles from '../../../styles/articles';
import SideScroller from '../../../components/SideScroller';
import Spinner from '../../../components/Spinner';

class Single extends Component {

  state = {
    article: {
      title: 'Memuat..',
    },
    fetching: false,
    exit: false,
  }
  exit = this._exit.bind(this);
  cancelToken = false;

  componentDidMount() {
    this.load(this.props.match.params.slug);
    BackHandler.addEventListener('hardwareBackPress', this.exit);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.slug !== this.props.match.params.slug)
      this.load(nextProps.match.params.slug);
  }

  load(slug) {
    if (this.state.fetching)
      return false;

    this.setState({ fetching: true });
    Articles.show({
      slug,
      cb: ({ article }) => {
        this.setState({ article, fetching: false });
        this.cancelToken = false;
      },
      err: () => {
        this.setState({ error: true });
        this.cancelToken = false;
      },
      cancelToken: new CancelToken(c => { this.cancelToken = c; })
    });
  }

  _exit() {
    this.refs.baseView.fadeOutRight(300);
    this.cancelToken && this.cancelToken();
    BackHandler.removeEventListener('hardwareBackPress', this.exit);
    setTimeout(() => {
      this.setState({ exit: true });
    }, 300);
    return true;
  }

  render() {
    if (this.state.exit)
      return <Redirect to={ Query.parse(this.props.location.search).ref ? Query.parse(this.props.location.search).ref : '/articles' } />;

    let featuredImageUri =
      this.state.article.featuredImage
      && (
        this.state.article.featuredImage.medium_large
        || this.state.article.featuredImage.medium
        || this.state.article.featuredImage.thumbnail
      );

    return (
      <AnimatableView
        style={ BaseStyles.overlayContainer }
        animation="fadeInRight"
        easing="ease-in-out-sine"
        duration={ 300 }
        ref="baseView">
        <Header
          titleComponent={
            (
              <View style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
                <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_HEADINGS, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>Artikel Kesehatan</Text>
                <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>
                  {
                    this.state.fetching ?
                      'Memuat..' :
                      this.state.article.author + ', ' + this.state.article.publishedAt
                  }
                </Text>
              </View>
            )
          }
          back={ () => this._exit() } />
        {
          this.state.fetching ?
            <Spinner /> :
            (
              <ScrollView>
                <View style={{ backgroundColor: StyleConstants.COLOR.WHITE, elevation: 2, shadowColor: StyleConstants.COLOR.BLACK, shadowOpacity: 0.2, shadowOffset: { height: 2 } }}>
                  <View style={{ paddingHorizontal: 24, paddingTop: 36, paddingBottom: 24 }}>
                    <Text style={ BaseStyles.h2 }>{ this.state.article.title }</Text>
                  </View>
                  { featuredImageUri && <Image source={{ uri: featuredImageUri }} style={{ height: 250, width: '100%' }} /> }
                  <View style={{ paddingHorizontal: 24, paddingVertical: 24 }}>
                    <HTMLView value={ this.state.article.content } stylesheet={ ArticleStyles } />
                  </View>
                </View>

                {
                  this.state.article.relatedArticles &&
                  (
                    <View style={{ paddingVertical: StyleConstants.SPACING.X4 }}>
                      <View style={{ paddingHorizontal: StyleConstants.SPACING.X4 }}>
                        <Text style={ BaseStyles.h5 }>Artikel terkait</Text>
                      </View>
                      <SideScroller
                        items={
                          this.state.article.relatedArticles.data.map((related, index) => {
                            return Object.assign(related, {
                              link: '/articles/' + related.slug,
                              image: related.featuredImage.medium || related.featuredImage.thumbnail
                            });
                          })
                        }
                        />
                    </View>
                  )
                }
              </ScrollView>
            )
        }
      </AnimatableView>
    );
  }
}

export default Single;