import React, { Component, Fragment } from 'react';
import { FlatList } from 'react-native';
import { View } from 'react-native-animatable';

import Header from '../../../components/Header';
import Spinner from '../../../components/Spinner';
import ListItem from '../../../components/articles/ListItem';
import BaseStyles from '../../../styles/base';
import Error from '../../../components/Error';

class Home extends Component {
  state = {  }

  componentDidMount() {
    setTimeout(() => {
      this.props.getArticles({ reload: true })
    }, 300);
  }

  render() {
    return (
      <View style={ BaseStyles.container } animation="fadeInRight" easing="ease-out" duration={ 300 }>
        <Header title="Artikel Kesehatan" searchRoute="/articles/search" />
        {
          this.props.articles.length || this.props.fetching ?
          <FlatList
            data={ this.props.articles }
            onRefresh={ () => this.props.getArticles({ reload: true }) }
            refreshing={ this.props.fetching }
            renderItem={ ({ item, index }) => <ListItem item={ item } index={ index } withFeatured={ true } /> }
            keyExtractor={ item => item.slug }
            onEndReached={ () => this.props.getArticles({ reload: false }) }
            ListFooterComponent={ () => (
              <Fragment>
                { !this.props.error && this.props.articles.length > 0 && <Spinner /> }
                { this.props.error && <Error onRetry={ () => this.props.getArticles({ reload: true }) } /> }
              </Fragment>
            )}
            /> : null
        }
      </View>
    );
  }
}

export default Home;