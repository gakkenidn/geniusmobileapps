import React, { Component, Fragment } from 'react';
import { Route, Redirect } from 'react-router-native';

import Home from './Home';
import Empty from './Empty';
import Login from '../Login';
import Edit from './Edit';

class Profile extends Component {
  state = { attemptingLogin: false, signingOut: false }
  render() {
    if (this.state.signingOut)
      return null;

    return (
      <Fragment>
        {
          !this.props.auth.access.length ?
            <Empty onAttemptLogin={ () => this.setState({ attemptingLogin: true })} /> :
            (
              <Fragment>
                <Route path="/profile" render={ route => <Home route={ route } getProfile={ () => this.props.getProfile() } { ...this.props.profile } onUnauthenticate={ () => this.props.unauthenticate({ token: this.props.auth.access }) } typeView={this.props.typeView} onGakken={this.props.onGakken} onLMS={this.props.onLMS}/> } />
                <Route path="/profile/edit" render={ () => <Edit getProfile={ () => this.props.getProfile() } { ...this.props.profile } onSuccess={ profile => this.props.populateProfile(profile) } accessToken={ this.props.auth.access } /> } />
              </Fragment>
            )
        }
        {
          this.state.attemptingLogin &&
          <Login
            onCancel={ () => this.setState({ attemptingLogin: false }) }
            onAuthenticated={ ({ access, refresh }) => {
              this.props.authenticate({ access, refresh });
              this.setState({ attemptingLogin: false })
            }} />
        }
      </Fragment>
    );
  }
}

export default Profile;