import React from 'react';
import { Text, View } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import Touchable from 'react-native-platform-touchable';

import BaseStyles from '../../styles/base'
import * as StyleConstants from '../../styles/constants'
import Header from '../../components/Header';

const Empty = props => (
  <AnimatableView animation="fadeInRight" duration={ 300 } style={ BaseStyles.container }>
    <Header title="Profil" />

    <View style={{ paddingHorizontal: StyleConstants.SPACING.X4, height: '100%', justifyContent: 'center', alignItems: 'center' }}>
      <Text style={{
        textAlign: 'center',
        fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF,
        fontSize: StyleConstants.FONT_SIZE_BASE,
        lineHeight: StyleConstants.FONT_SIZE_BASE * StyleConstants.LINE_HEIGHT_BASE,
        marginBottom: StyleConstants.SPACING.X4
      }}>
        Anda sedang menggunakan aplikasi Gakken Indonesia tanpa akun.
      </Text>

      <Touchable onPress={ props.onAttemptLogin }>
        <View style={{
          backgroundColor: StyleConstants.COLOR.PRIMARY,
          paddingVertical: StyleConstants.SPACING.X3,
          paddingHorizontal: StyleConstants.SPACING.X4,
          borderRadius: StyleConstants.SPACING_BASE
        }}>
          <Text style={{
            color: StyleConstants.COLOR.WHITE,
            fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF,
            fontSize: StyleConstants.FONT_SIZE_BASE,
          }}>
            Masuk ke Akun Gakken
          </Text>
        </View>
      </Touchable>
    </View>

  </AnimatableView>
);

export default Empty;