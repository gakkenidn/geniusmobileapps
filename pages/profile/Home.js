import React, { Component } from 'react';
import { Image, ScrollView, Text, View } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import {Redirect, Link} from 'react-router-native';
import Touchable from 'react-native-platform-touchable';

import Header from '../../components/Header';
import BaseStyles from '../../styles/base';
import * as StyleConstants from '../../styles/constants';
import Spinner from '../../components/Spinner';
import DetailItem from '../../components/profile/DetailItem';
import Error from '../../components/Error';

class Home extends Component {
  state = {
  }

  componentDidMount() {
    this.props.getProfile();
  }

  switchType(type) {
    (type.type === 1) ? this.props.onGakken() : this.props.onLMS();
  }

  render() {
    return (
      <AnimatableView style={ BaseStyles.container } animation="fadeInRight" duration={ 300 }>
        <Header title="Profil"
                route={this.props.route}
                gakkenIndo={this.props.onGakken}
                editRoute={ (!this.props.fetching && !this.props.error) && "/profile/edit" }
                signOut={(!this.props.fetching && !this.props.error) && (this.props.onUnauthenticate)} />

        { this.props.fetching && <Spinner /> }
        { this.props.error && <Error onRetry={ () => this.props.getProfile() } /> }
        {
          !this.props.fetching
          && !this.props.error
          &&
            (
              <ScrollView style={{ flex: 1 }}>
                <View style={{
                  height: 180,
                  backgroundColor: StyleConstants.COLOR_GRAY.LIGHTEST,
                  justifyContent: 'flex-end',
                  paddingHorizontal: StyleConstants.SPACING.X4,
                  paddingBottom: StyleConstants.SPACING.X3,
                  elevation: 2,
                  shadowColor: StyleConstants.COLOR.BLACK,
                  shadowOpacity: 0.2,
                  shadowOffset: { height: 1, width: 1 }
                  }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                    { this.props.data.avatar &&
                      <Image
                        source={{ uri: this.props.data.avatar }}
                        style={{
                          width: 80,
                          height: 80,
                          borderRadius: 40,
                          marginRight: StyleConstants.SPACING.X3,
                          shadowColor: StyleConstants.COLOR.BLACK,
                          shadowOpacity: 0.2,
                          shadowOffset: { height: 1, width: 1 }
                        }} />
                      }
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start' }}>
                      <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_HEADINGS, fontSize: StyleConstants.FONT_SIZE.H3, color: StyleConstants.COLOR_GRAY.DARKEST }} numberOfLines={ 1 }>{ this.props.data.name }</Text>
                      <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, fontSize: StyleConstants.FONT_SIZE_BASE, color: StyleConstants.COLOR_GRAY.NORMAL }} numberOfLines={ 1 }>Terdaftar sejak { this.props.data.joinedAt }</Text>
                    </View>
                  </View>
                </View>
                <View style={{
                  paddingHorizontal: StyleConstants.SPACING.X4,
                  paddingVertical: StyleConstants.SPACING.X4,
                  backgroundColor: StyleConstants.COLOR.WHITE,
                  elevation: 1,
                  shadowColor: StyleConstants.COLOR.BLACK,
                  shadowOpacity: 0.2,
                  shadowOffset: { height: 1, width: 1 }
                  }}>
                  <DetailItem title="Email terdaftar" detail={ this.props.data.email } />
                  <DetailItem title="Profesi" detail={ this.props.data.profession ? (this.props.data.profession === 'doctor' ? 'Dokter' : 'Dokter Gigi') : 'Belum diatur' } />
                  <DetailItem title="Nomor ID keprofesian" detail={ this.props.data.idNo ? this.props.data.idNo : 'Belum diatur' } />
                </View>
                <View>
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', paddingHorizontal: StyleConstants.SPACING.X4, paddingTop: StyleConstants.SPACING.X3, marginBottom: 10 }}>
                    <Text style={{color: StyleConstants.COLOR_GRAY.DARKEST}}>Layanan Gakken</Text>
                  </View>
                  <View style={{
                    alignItems:     'center',
                    justifyContent: 'space-around',
                    flexDirection: 'row',
                    marginRight: 10,
                    marginLeft: 10,
                  }}>
                    <Link to="/" component={Touchable} hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }} onPress={ () => this.switchType({type: 1}) }>
                      <View style={[BaseStyles.promptButtonImage]}>
                        <Image source={require('../../resources/images/GakkenIndonesia.png')} style={{width: 120, height: 40}}/>
                        <Text style={{paddingVertical: StyleConstants.SPACING.X2}}>Gakken Indonesia</Text>
                      </View>
                    </Link>
                    <Link to="/" component={Touchable} hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }} onPress={ () => this.switchType({type: 2}) }>
                      <View style={BaseStyles.promptButtonImage}>
                        <Image source={require('../../resources/images/GakkenGenius.png')} style={{width: 120, height: 40}}/>
                        <Text style={{paddingVertical: StyleConstants.SPACING.X2}}>LMS</Text>
                      </View>
                    </Link>
                  </View>
                </View>
              </ScrollView>
            )
        }
      </AnimatableView>
    );
  }
}

export default Home;