import ClassApi from '../api/classes';
import {invalidateArticles} from "./articles";

export const getClasses = ({reload}) =>
    (dispatch, getState) => {
        if (getState().classes.fetching)
            return;
        reload && dispatch(invalidateClasses());
        return ClassApi.index({
            params: {},
            cb: ({classes}) => {
                dispatch({type: 'CLASSES_POPULATE', classes});
            },
            err: () => dispatch(setErrorClasses())
        });
    };

export const invalidateClasses = () => {
    return {type: 'CLASSES_INVALIDATE'};
};

export const setErroClasses = () => {
    return {type: 'CLASSES_SET_ERROR'};
};