import ProfileApi from '../api/profile';
import AsyncStorage from '@react-native-community/async-storage';

export const getProfile = () =>
  (dispatch, getState) => {
    if (getState().profile.fetching)
      return;

    dispatch(invalidateProfile());
    return ProfileApi.show({
      accessToken: getState().auth.access,
      cb: profile => dispatch(populateProfile(profile)),
      err: () => dispatch(setErrorProfile()),
    });
  }

export const populateProfile = profile => {
  return { type: 'PROFILE_POPULATE', profile };
}
export const invalidateProfile = () => {
  return { type: 'PROFILE_INVALIDATE' };
}
export const setErrorProfile = () => {
  return { type: 'PROFILE_SET_ERROR' };
}

export const setAuthenticationTokens = ({ access, refresh }) => {
  return { type: 'AUTH_SET_TOKENS', access, refresh };
}

export const authenticate = ({ access, refresh }) => {
  return async (dispatch) => {
    await AsyncStorage.setItem('@AppStore:accessToken', access);
    await AsyncStorage.setItem('@AppStore:refreshToken', refresh);
    await AsyncStorage.setItem('@AppStore:type', 'gakken');
    return dispatch(setAuthenticationTokens({ access, refresh }));
  }
}

export const unauthenticate = ({ token }) => {
  return async (dispatch) => {
    await AsyncStorage.setItem('@AppStore:accessToken', '');
    await AsyncStorage.setItem('@AppStore:refreshToken', '');
    await AsyncStorage.setItem('@AppStore:type', 'gakken');
    return dispatch(setAuthenticationTokens({ access: '', refresh: '' }));
  }
}