import JournalsApi from '../api/journals';

export const getJournalCategories = () =>
  (dispatch, getState) => {
    if (
      getState().journalCategories.fetching
      || getState().journalCategories.data.length
    )
      return;

    dispatch(invalidateJournalCategories());
    JournalsApi.categoriesIndex({
      cb: categories => dispatch(populateJournalCategories(categories)),
      err: () => dispatch(setErrorJournalCategories())
    });
  }

export const populateJournalCategories = categories => {
  return { type: 'JOURNAL_CATEGORIES_POPULATE', categories };
}
export const invalidateJournalCategories = () => {
  return { type: 'JOURNAL_CATEGORIES_INVALIDATE' };
}
export const setErrorJournalCategories = () => {
  return { type: 'JOURNAL_CATEGORIES_SET_ERROR' };
}