import DrugsApi from '../api/drugs';
import AsyncStorage from '@react-native-community/async-storage';

export const getDrugClasses = () =>
  async (dispatch, getState) => {
    if (
      getState().drugClasses.fetching
      || getState().drugClasses.data.length
    )
      return;

    dispatch(invalidateDrugClasses());

    let classesFromStorage = await AsyncStorage.getItem('@AppStore:drugClasses');
    if (classesFromStorage != null)
      return dispatch(populateDrugClasses(JSON.parse(classesFromStorage)));

    DrugsApi.classesIndex({
      cb: classes => dispatch(populateDrugClassesToStorage(classes)),
      err: () => dispatch(setErrorDrugClasses())
    });
  }

export const populateDrugClassesToStorage = (classes) =>
  async (dispatch) => {
    await AsyncStorage.setItem('@AppStore:drugClasses', JSON.stringify(classes));
    return dispatch(populateDrugClasses(classes));
  }

export const populateDrugClasses = (classes) => {
  return { type: 'DRUG_CLASSES_POPULATE', classes };
}

export const invalidateDrugClasses = () => {
  return { type: 'DRUG_CLASSES_INVALIDATE' };
}
export const setErrorDrugClasses = () => {
  return { type: 'DRUG_CLASSES_SET_ERROR' };
}