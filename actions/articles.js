import ArticlesApi from '../api/articles';

export const getArticles = ({ reload }) =>
  (dispatch, getState) => {
    if (getState().articles.fetching)
      return;
    reload && dispatch(invalidateArticles());
    return ArticlesApi.index({
      params: { page: getState().articles.page },
      cb: ({ articles }) => {
        dispatch({ type: 'ARTICLES_POPULATE', articles });
      },
      err: () => dispatch(setErrorArticles())
    });
  }

export const invalidateArticles = () => {
  return { type: 'ARTICLES_INVALIDATE' };
}

export const populateSearchArticles = ({ articles }) => {
  return { type: 'ARTICLES_POPULATE_SEARCHED', articles };
}

export const setErrorArticles = () => {
  return { type: 'ARTICLES_SET_ERROR' };
}