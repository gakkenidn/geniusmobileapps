import TopicsApi from '../api/topics';

export const getCatalog = () =>
  (dispatch, getState) => {
    if (getState().catalog.fetching)
      return;
    dispatch(invalidateCatalog());
    return TopicsApi.catalogIndex({
      cb: (catalog) => dispatch(populateCatalog({ catalog })),
      err: () => dispatch(setErrorCatalog()),
      token: getState().auth.access || null
    });
  }

export const invalidateCatalog = () => {
  return { type: 'CATALOG_INVALIDATE' };
}

export const populateCatalog = ({ catalog }) => {
  return { type: 'CATALOG_POPULATE', catalog };
}

export const setErrorCatalog = () => {
  return { type: 'CATALOG_SET_ERROR' };
}