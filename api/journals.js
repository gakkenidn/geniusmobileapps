import Axios from "axios";
import { API_BASE_URL } from "../env";

export default class Journals {

  static index({ cb, err, params = null, cancelToken }) {
    return Axios.get(API_BASE_URL + '/journals', {
      params,
      cancelToken
    })
      .then(response => cb(response.data.data))
      .catch(e => err(e));
  }

  static show({ slug, cb, err, cancelToken }) {
    return Axios.get(API_BASE_URL + '/journals/' + slug, { cancelToken })
      .then(response => cb(response.data.data))
      .catch(e => err(e));
  }

  static categoriesIndex({ cb, err }) {
    return Axios.get(API_BASE_URL + '/journals/categories')
      .then(response => cb(response.data.data))
      .catch(e => err(e));
  }

  static categoriesShow({ slug, cb, err, cancelToken }) {
    return Axios.get(API_BASE_URL + '/journals/categories/' + slug, { cancelToken })
      .then(response => cb(response.data.data))
      .catch(e => err(e));
  }

  static getTicketedURL({ slug, cb, err, token, cancelToken }) {
    return Axios.get(API_BASE_URL + '/journals/read/' + slug, {
      headers: {
        'Authorization': 'Bearer ' + token
      },
      cancelToken
    })
      .then(response => cb(response.data.ticketedURL))
      .catch(e => err(e));
  }

}