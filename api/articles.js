import Axios from "axios";
import { API_BASE_URL } from "../env";

export default class Articles {

  static index({ params, cb, cancelToken = null, err }) {
    return Axios
      .get(API_BASE_URL + '/articles', {
        params,
        cancelToken: cancelToken ? new Axios.cancelToken(c => { cancelToken = c; }) : null
      })
      .then(response => {console.log(JSON.stringify(response)); cb({ articles: response.data.data })})
      .catch(e => {console.log(e);err(e);});
  }

  static show({ slug, cb, err = false, cancelToken }) {
    return Axios
      .get(API_BASE_URL + '/articles/' + slug, { cancelToken })
      .then(response => cb({ article: response.data.data }))
      .catch(e => handleError(e, err));
  }

}

const handleError = ({ err, cb = false }) => {
  if (cb) return cb(err);
  throw(err);
};