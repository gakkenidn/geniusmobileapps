import Axios from 'axios';
import { API_BASE_URL } from '../env';

export default class Drugs {

  static index({ params = {}, cb, err, cancelToken }) {
    Axios.get(API_BASE_URL + '/drugs', {
      params,
      cancelToken
    })
      .then(response => cb(response.data.data))
      .catch(e => err(e));
  }

  static show({ slug, cb, err, cancelToken }) {
    Axios.get(API_BASE_URL + '/drugs/' + slug, { cancelToken })
      .then(response => cb(response.data.data))
      .catch(e => err(e));
  }

  static classesIndex({ cb, err, cancelToken }) {
    Axios.get(API_BASE_URL + '/drugs/classes', {
      cancelToken: new Axios.CancelToken(c => { cancelToken = c; })
    })
      .then(response => cb(response.data.data))
      .catch(e => err(e));
  }

}