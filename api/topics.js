import Axios from 'axios';
import { API_BASE_URL } from "../env";

export default class Topics {

  static show({ slug, token, cb, err, cancelToken }) {
    return Axios.get(API_BASE_URL + '/topics/' + slug, {
      headers: {
        'Authorization': 'Bearer ' + token,
        'accept': 'application/json'
      },
      cancelToken: new Axios.CancelToken(c => { cancelToken = c; })
    })
      .then(response => cb(response.data.data))
      .catch(e => err(e));
  }

  static catalogIndex({ cb, err, token }) {
    return Axios.get(API_BASE_URL + '/topic-catalog' + (token ? '-user' : ''), {
      headers: {
        'Authorization': token ? 'Bearer ' + token : token,
        'accept': 'application/json'
      }
    })
      .then(response => cb(response.data.data))
      .catch(e => {
        // console.warn(JSON.stringify(e.response));
        err(e)
      });
  }

  static catalogShow({ slug, cb, err, cancelToken }) {
    return Axios.get(API_BASE_URL + '/topic-catalog/' + slug, {
      cancelToken
    })
      .then(response => cb(response.data.data))
      .catch(e => err(e));
  }

  static redeem({ topic, token, cb, err, cancelToken }) {
    return Axios.post(API_BASE_URL + '/topics/redeem', { topic }, {
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      cancelToken: new Axios.CancelToken(c => { cancelToken = c; })
    })
      .then(response => cb(response.data.data))
      .catch(e => err(e));
  }

}