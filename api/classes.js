import Axios from 'axios';
import {API_BASE_URL} from "../env";

export default class Classes {

    static index({params, cb, cancelToken = null, err}){
        return Axios
            .get(API_BASE_URL + '/classes', {
                params,
                cancelToken: cancelToken ? new Axios.cancelToken(c => {cancelToken = c; }) : null
            })
            .then(response => cb({classes: response.data.data}))
            .catch(e => err(e));
    }

}